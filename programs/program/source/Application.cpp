/**
 * @file programs/program/Application.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <Application.h>

#include <chrono>
#include <cmath>
#include <fstream>
#include <vector>

#include <core/Assert.h>
#include <core/Utility.h>
#include <graphics/CommandBuffer.h>
#include <gsl/span>

namespace chrono = std::chrono;

using namespace Core;
using namespace Graphics;

// Internal

struct Vertex
{
	float x;
	float y;
	float z;
	uint32_t colour;
	float u;
	float v;

	constexpr Vertex(const float x, const float y, const float z, const float u, const float v,
		const uint32_t colour) :
		x(x),
		y(y),
		z(z),
		u(u),
		v(v),
		colour(colour) { }
};

static constexpr std::array<uint16_t, 6> indexData
{
	0, 1, 2, 0, 2, 3
};

static constexpr std::array vertexData
{
	Vertex(-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0xFFFFFFFF),
	Vertex(-0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0xFFFFFFFF),
	Vertex( 0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 0xFFFFFFFF),
	Vertex( 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0xFFFFFFFF)
};

static constexpr uint32_t windowWidth  = 1440;
static constexpr uint32_t windowHeight = 810;

static std::vector<std::byte> loadImage(const std::string_view& path);

// Public

void Application::run()
{
	chrono::time_point startTime = chrono::high_resolution_clock::now();

	while(_graphics.isWindowOpen(_windowHandle))
	{
		_graphics.processWindowMessages();

		const chrono::time_point endTime = chrono::high_resolution_clock::now();
		const float deltaTime = chrono::duration<float>(endTime - startTime).count();
		startTime = endTime;
		update(deltaTime);
		draw(deltaTime);
	}
}

// Private

void Application::initialiseWindow()
{
	const GraphicsAdapter& adapter = _graphics.adapters()[0];
	_windowHandle = _graphics.createWindow(adapter.displays[0], windowWidth, windowHeight);
	_graphics.showWindow(_windowHandle);
}

void Application::initialiseDeviceAndObjects()
{
	_device = _graphics.createDevice(0);
	_stagingManager.initialise(_device, 1);
	_swapchain = _graphics.createSwapchain(_device, _windowHandle);
	_graphics.createContexts(_device, _contexts);
	createRenderPass();
	createLayouts();
	createPipeline();
	_stagingManager.beginStaging();
	createBuffers();
	createImage();
	_stagingManager.endStaging();
	_imageView = _device.createImageView(_image.object, Vk::Format::R8G8B8A8Srgb);
	createDescriptorSet();
	_gui.initialise(_graphics, _device, _renderPass, contextCount);
}

void Application::destroyDeviceAndObjects()
{
	_device.waitForIdle();

	_gui.deinitialise(_graphics);
	_device.destroyObject(_descriptorPool);
	_device.destroyObject(_imageView);
	_device.destroyObject(_image);
	_device.destroyObject(_indexBuffer);
	_device.destroyObject(_vertexBuffer);
	_device.destroyObject(_pipeline);
	_device.destroyObject(_pipelineLayout);
	_device.destroyObject(_descriptorSetLayout);
	_device.destroyObject(_sampler);
	_device.destroyObject(_renderPass);

	_graphics.destroyContexts(_device, _contexts);
	_graphics.destroySwapchain(_device, _swapchain);
	_stagingManager.deinitialise();
	_graphics.destroyDevice(_device);
}

void Application::update(const float deltaTime)
{
	_angle += 0.5f * 3.141593f * deltaTime;

	_transform =
	{
		std::cos(_angle), 0.0f, -std::sin(_angle), 0.0f,
		0.0f,             1.0f,  0.0f,             0.0f,
		std::sin(_angle), 0.0f,  std::cos(_angle), 0.0f,
		0.0f,             0.0f,  0.5f,             1.0f
	};

	_gui.begin(deltaTime, Vk::Extent2D(windowWidth, windowHeight));
	ImGui::ShowDemoWindow();
	_gui.end();
}

void Application::draw(const float deltaTime)
{
	static_cast<void>(deltaTime);

	_currentContext = &(_contexts[_contextIndex]);
	const uint32_t swapchainImageIndex = _currentContext->getNextSwapchainImage(_swapchain);
	_currentContext->begin();

	const CommandBuffer& commandBuffer = _currentContext->commandBuffer();
	const Swapchain::Framebuffer& framebuffer = _swapchain.framebuffer(swapchainImageIndex);
	const Vk::ClearValue clearValue(Vk::ClearColorValue(1.0f, 0.0f, 1.0f, 1.0f));
	commandBuffer.beginRenderPass(_renderPass, framebuffer.framebuffer, toSpan(clearValue));

	commandBuffer.bindPipeline(Vk::PipelineBindPoint::Graphics, _pipeline);
	commandBuffer.bindDescriptorSets(Vk::PipelineBindPoint::Graphics, _pipelineLayout, 0,
		toSpan(_descriptorSet), {});

	commandBuffer.bindVertexBuffer(_vertexBuffer.object);
	commandBuffer.bindIndexBuffer(_indexBuffer.object, Vk::IndexType::Uint16);
	commandBuffer.pushConstants<float>(_pipelineLayout, Vk::ShaderStageFlagBits::Vertex, 0, _transform);

	commandBuffer.drawIndexed(6);
	_gui.draw(commandBuffer);

	commandBuffer.endRenderPass();

	_currentContext->end(++_timelineSemaphoreValue);
	_currentContext->present(_swapchain, swapchainImageIndex);

	_contextIndex = (_contextIndex + 1) % contextCount;
}

void Application::createRenderPass()
{
	const Vk::AttachmentDescription attachment({}, _swapchain.colourImageFormat(),
		Vk::SampleCountFlagBits::e1, Vk::AttachmentLoadOp::Clear, Vk::AttachmentStoreOp::Store,
		Vk::AttachmentLoadOp::DontCare, Vk::AttachmentStoreOp::DontCare, Vk::ImageLayout::Undefined,
		Vk::ImageLayout::PresentSrcKHR);

	const Vk::AttachmentReference colourAttachment(0, Vk::ImageLayout::ColorAttachmentOptimal);
	const Vk::SubpassDescription subpass({}, Vk::PipelineBindPoint::Graphics, {}, toSpan(colourAttachment));
	const Vk::RenderPassCreateInfo createInfo({}, toSpan(attachment), toSpan(subpass));
	_renderPass = _device.createRenderPass(createInfo);
}

void Application::createLayouts()
{
	const Vk::SamplerCreateInfo samplerCreateInfo({}, Vk::Filter::Linear, Vk::Filter::Linear,
		Vk::SamplerMipmapMode::Linear, Vk::SamplerAddressMode::ClampToEdge,
		Vk::SamplerAddressMode::ClampToEdge, Vk::SamplerAddressMode::ClampToEdge, 0.0f, false, 0.0f, false,
		Vk::CompareOp::Never, 0.0f, 1000.0f);

	_sampler = _device.createSampler(samplerCreateInfo);

	const std::array descriptorSetLayoutBindings
	{
		Vk::DescriptorSetLayoutBinding(0, Vk::DescriptorType::CombinedImageSampler, 1,
			Vk::ShaderStageFlagBits::Fragment, toSpan(_sampler))
	};

	_descriptorSetLayout = _device.createDescriptorSetLayout(descriptorSetLayoutBindings);
	const Vk::PushConstantRange pushConstantRange(Vk::ShaderStageFlagBits::Vertex, 0, 16 * sizeof(float));
	_pipelineLayout = _device.createPipelineLayout(toSpan(_descriptorSetLayout), toSpan(pushConstantRange));
}

void Application::createPipeline()
{
	const Vk::ShaderModule vertexShaderModule = loadShader("assets/bin/vertex.spirv");
	const Vk::ShaderModule fragmentShaderModule = loadShader("assets/bin/fragment.spirv");

	const std::array shaderStageInfos
	{
		Vk::PipelineShaderStageCreateInfo({}, Vk::ShaderStageFlagBits::Vertex, vertexShaderModule, "main"),
		Vk::PipelineShaderStageCreateInfo({}, Vk::ShaderStageFlagBits::Fragment, fragmentShaderModule, "main")
	};

	const Vk::VertexInputBindingDescription vertexBindingInfo(0, sizeof(Vertex), Vk::VertexInputRate::Vertex);

	const std::array vertexAttributes
	{
		Vk::VertexInputAttributeDescription(0, 0, Vk::Format::R32G32B32Sfloat, 0),
		Vk::VertexInputAttributeDescription(1, 0, Vk::Format::R8G8B8A8Unorm, 3 * sizeof(float)),
		Vk::VertexInputAttributeDescription(2, 0, Vk::Format::R32G32Sfloat, 4 * sizeof(float)),
	};

	const Vk::PipelineVertexInputStateCreateInfo vertexInputInfo({}, toSpan(vertexBindingInfo),
		vertexAttributes);

	const Vk::PipelineInputAssemblyStateCreateInfo inputAssemblyInfo({}, Vk::PrimitiveTopology::TriangleList);

	const Vk::Viewport viewport(0.0f, 0.0f, static_cast<float>(windowWidth), static_cast<float>(windowHeight),
		0.0f, 1.0f);

	const Vk::Rect2D scissorRectangle(Vk::Offset2D(0, 0), Vk::Extent2D(windowWidth, windowHeight));
	const Vk::PipelineViewportStateCreateInfo viewportInfo({}, toSpan(viewport), toSpan(scissorRectangle));

	Vk::PipelineRasterizationStateCreateInfo rasterisationInfo({}, false, false, Vk::PolygonMode::Fill,
		Vk::CullModeFlagBits::None, Vk::FrontFace::CounterClockwise);

	rasterisationInfo.lineWidth = 1.0f;

	const Vk::PipelineMultisampleStateCreateInfo multisampleInfo;

	const Vk::PipelineDepthStencilStateCreateInfo depthInfo;

	Vk::PipelineColorBlendAttachmentState blendAttachmentState;
	blendAttachmentState.colorWriteMask = Vk::ColorComponentFlagBits::R | Vk::ColorComponentFlagBits::G |
		Vk::ColorComponentFlagBits::B | Vk::ColorComponentFlagBits::A;

	const Vk::PipelineColorBlendStateCreateInfo blendInfo({}, false, Vk::LogicOp::Clear,
		toSpan(blendAttachmentState));

	const Vk::GraphicsPipelineCreateInfo createInfo({}, shaderStageInfos, vertexInputInfo, inputAssemblyInfo,
		{}, viewportInfo, rasterisationInfo, multisampleInfo, depthInfo, blendInfo, {}, _pipelineLayout,
		_renderPass);

	_pipeline = _device.createPipeline(createInfo);

	_device.destroyObject(fragmentShaderModule);
	_device.destroyObject(vertexShaderModule);
}

void Application::createBuffers()
{
	const Vk::DeviceSize vertexDataSize = vertexData.size() * sizeof(Vertex);
	const Vk::DeviceSize indexDataSize = indexData.size() * sizeof(uint16_t);
	_vertexBuffer = _device.createBuffer(vertexData.size() * sizeof(Vertex),
		Vk::BufferUsageFlagBits::VertexBuffer | Vk::BufferUsageFlagBits::TransferDst, MemoryUsage::Device);

	_indexBuffer = _device.createBuffer(indexData.size() * sizeof(uint16_t),
		Vk::BufferUsageFlagBits::IndexBuffer | Vk::BufferUsageFlagBits::TransferDst, MemoryUsage::Device);

	gsl::span bufferData(reinterpret_cast<const std::byte*>(vertexData.data()), vertexDataSize);
	Vk::BufferCopy copyRegion(0, 0, vertexDataSize);
	_stagingManager.stageBuffer(bufferData, copyRegion, _vertexBuffer.object,
		Vk::PipelineStageFlagBits::VertexInput, Vk::AccessFlagBits::VertexAttributeRead);

	bufferData = gsl::span(reinterpret_cast<const std::byte*>(indexData.data()), indexDataSize);
	copyRegion.size = indexDataSize;
	_stagingManager.stageBuffer(bufferData, copyRegion, _indexBuffer.object,
		Vk::PipelineStageFlagBits::VertexInput, Vk::AccessFlagBits::IndexRead);
}

void Application::createImage()
{
	const Vk::Extent2D extent(256, 256);
	const std::vector<std::byte> imageData = loadImage("assets/image.raw");
	_image = _device.createImage(Vk::Format::R8G8B8A8Srgb, extent,
		Vk::ImageUsageFlagBits::Sampled | Vk::ImageUsageFlagBits::TransferDst, MemoryUsage::Device);

	const Vk::ImageSubresourceLayers subresourceLayers(Vk::ImageAspectFlagBits::Color, 0, 0, 1);
	const Vk::Extent3D copyExtent(extent.width, extent.height, 1);
	const Vk::BufferImageCopy copyRegion(0, extent.width, extent.height, subresourceLayers,
		Vk::Offset3D(), copyExtent);

	_stagingManager.stageImage(imageData, copyRegion, _image.object, Vk::PipelineStageFlagBits::TopOfPipe,
		Vk::PipelineStageFlagBits::FragmentShader, Vk::ImageLayout::Undefined,
		Vk::ImageLayout::ShaderReadOnlyOptimal);
}

void Application::createDescriptorSet()
{
	const Vk::DescriptorPoolSize poolSize(Vk::DescriptorType::CombinedImageSampler, 1);
	_descriptorPool = _device.createDescriptorPool(1, toSpan(poolSize));
	_descriptorSet = _device.allocateDescriptorSet(_descriptorPool, _descriptorSetLayout);

	const Vk::DescriptorImageInfo imageInfo(Vk::Sampler(), _imageView,
		Vk::ImageLayout::ShaderReadOnlyOptimal);

	const Vk::WriteDescriptorSet descriptorWrite(_descriptorSet, 0, 0,
		Vk::DescriptorType::CombinedImageSampler, toSpan(imageInfo));

	_device.updateDescriptorSets(toSpan(descriptorWrite));
}

Vk::ShaderModule Application::loadShader(const std::string_view& path) const
{
	std::basic_ifstream<std::byte> fileStream(path.data(), std::ios::binary | std::ios::in);
	RUNKO_ASSERT(fileStream.is_open());
	fileStream.seekg(0, std::ios::end);
	std::vector<std::byte> shaderCode(fileStream.tellg());
	fileStream.seekg(0).read(shaderCode.data(), shaderCode.size());

	return _device.createShaderModule(shaderCode);
}


// Internal

static std::vector<std::byte> loadImage(const std::string_view& path)
{
	std::basic_ifstream<std::byte> fileStream(path.data(), std::ios::binary | std::ios::in);
	RUNKO_ASSERT(fileStream.is_open());
	fileStream.seekg(0, std::ios::end);
	std::vector<std::byte> imageData(fileStream.tellg());
	fileStream.seekg(0).read(imageData.data(), imageData.size());

	return imageData;
}
