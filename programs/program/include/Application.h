/**
 * @file programs/program/Application.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <array>
#include <string_view>

#include <core/Types.h>
#include <graphics/Common.h>
#include <graphics/GraphicsContext.h>
#include <graphics/GraphicsDevice.h>
#include <graphics/GraphicsSystem.h>
#include <graphics/GUI.h>
#include <graphics/StagingManager.h>
#include <graphics/Swapchain.h>
#include <graphics/vulkan/Vulkan.h>

class Application
{
public:

	Application()
	{
		initialiseWindow();
		initialiseDeviceAndObjects();
	}

	Application(const Application& application) = delete;
	Application(Application&& application) = delete;

	~Application()
	{
		destroyDeviceAndObjects();
		_graphics.destroyWindow(_windowHandle);
	}

	void run();

	Application& operator =(const Application& application) = delete;
	Application& operator =(Application&& application) = delete;

private:

	using Matrix4 = std::array<float, 16>;

	static constexpr uint32_t contextCount = 3;

	std::array<Graphics::GraphicsContext, contextCount> _contexts;
	Graphics::GraphicsContext* _currentContext = nullptr;
	Graphics::GraphicsDevice _device;
	Graphics::GraphicsSystem _graphics = Graphics::GraphicsSystem(true);
	Graphics::GUI _gui;
	Graphics::StagingManager _stagingManager;
	Graphics::Swapchain _swapchain;
	uint64_t _timelineSemaphoreValue = 0;
	Graphics::WindowHandle _windowHandle = 0;
	float _angle = 0.0f;
	uint32_t _contextIndex = 0;
	Matrix4 _transform{};

	Vk::DescriptorPool _descriptorPool;
	Vk::DescriptorSet _descriptorSet;
	Vk::DescriptorSetLayout _descriptorSetLayout;
	Graphics::Image _image;
	Vk::ImageView _imageView;
	Graphics::Buffer _indexBuffer;
	Graphics::Buffer _vertexBuffer;
	Vk::Pipeline _pipeline;
	Vk::PipelineLayout _pipelineLayout;
	Vk::RenderPass _renderPass;
	Vk::Sampler _sampler;

	void initialiseWindow();
	void initialiseDeviceAndObjects();
	void destroyDeviceAndObjects();
	void update(const float deltaTime);
	void draw(const float deltaTime);
	void createRenderPass();
	void createLayouts();
	void createPipeline();
	void createBuffers();
	void createImage();
	void createDescriptorSet();
	Vk::ShaderModule loadShader(const std::string_view& path) const;
};
