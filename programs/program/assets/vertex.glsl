#version 450 core

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColour;
layout(location = 2) in vec2 inTextureCoordinate;

layout(push_constant) uniform Constants
{
	mat4 transform;
} constants;

layout(location = 0) out struct Outputs
{
	vec4 colour;
	vec2 textureCoordinate;
} outputs;

void main()
{
	gl_Position = constants.transform * inPosition;
	outputs.colour = inColour;
	outputs.textureCoordinate = inTextureCoordinate;
}
