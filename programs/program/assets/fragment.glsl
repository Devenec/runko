#version 450 core

layout(location = 0) in struct Inputs
{
	vec4 colour;
	vec2 textureCoordinate;
} inputs;

layout(set = 0, binding = 0) uniform sampler2D imageSampler;

layout(location = 0) out vec4 outColour;

void main()
{
	outColour = inputs.colour * texture(imageSampler, inputs.textureCoordinate);
}
