/**
 * @file graphics/SwapchainCreator.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <vector>

#include <core/Types.h>
#include <graphics/Swapchain.h>
#include <graphics/vulkan/Vulkan.h>

namespace Graphics
{
	class GraphicsDevice;
	class VulkanGraphics;

	class SwapchainCreator
	{
	public:

		explicit SwapchainCreator(const VulkanGraphics& graphics) :
			_graphics(graphics) { }

		SwapchainCreator(const SwapchainCreator& SwapchainCreator) = delete;
		SwapchainCreator(SwapchainCreator&& swapchainCreator) = delete;

		~SwapchainCreator() = default;

		Swapchain createSwapchain(const GraphicsDevice& device, const Vk::SurfaceKHR surface,
			const Vk::Extent2D& surfaceExtent, const Vk::ImageUsageFlags additionalUsageFlags);

		SwapchainCreator& operator =(const SwapchainCreator& swapchainCreator) = delete;
		SwapchainCreator& operator =(SwapchainCreator&& swapchainCreator) = delete;

		static void destroySwapchain(const GraphicsDevice& device, Swapchain& swapchain);

	private:

		const GraphicsDevice* _device = nullptr;
		const VulkanGraphics& _graphics;
		Vk::SurfaceKHR _surface;
		Vk::Extent2D _imageExtent;
		Vk::SurfaceCapabilitiesKHR _surfaceCapabilities{};
		Vk::SurfaceFormatKHR _surfaceFormat;

		Vk::SurfaceFormatKHR chooseSurfaceFormat() const;
		void initialiseImageExtent(const Vk::Extent2D& surfaceExtent);
		Vk::SwapchainKHR createSwapchainObject(const Vk::ImageUsageFlags additionalUsageFlags) const;
		Vk::RenderPass createRenderPass() const;
		std::vector<Swapchain::Framebuffer> createSwapchainFramebuffers(const Vk::SwapchainKHR swapchain,
			const Vk::RenderPass renderPass) const;

		uint32_t getMinSwapchainImageCount() const;
		Vk::CompositeAlphaFlagBitsKHR chooseSwapchainCompositeAlpha() const;
	};
}
