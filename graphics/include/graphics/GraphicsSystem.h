/**
 * @file graphics/GraphicsSystem.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include <core/Assert.h>
#include <core/Rectangle.h>
#include <core/Types.h>
#include <graphics/GraphicsContext.h>
#include <graphics/platform/WindowManager.h>
#include <graphics/vulkan/VulkanGraphics.h>
#include <gsl/span>

namespace Platform
{
	class WindowInputEventHandler;
}

namespace Graphics
{
	class GraphicsDevice;
	class Swapchain;

	struct Display
	{
		uint32_t frequency;
		Core::Rectangle rectangle;

		Display(const Core::Rectangle& rectangle, const uint32_t frequency) :
			frequency(frequency),
			rectangle(rectangle) { }
	};

	using DisplayVector = std::vector<Display>;

	struct GraphicsAdapter
	{
		DisplayVector displays;
		std::string name;

		GraphicsAdapter(const std::string& name, DisplayVector&& displays) :
			displays(std::move(displays)),
			name(name) { }
	};

	using GraphicsAdapterVector = std::vector<GraphicsAdapter>;

	class GraphicsSystem
	{
	public:

		explicit GraphicsSystem(const Bool enableSwapchain = false) :
			_graphics(enableSwapchain)
		{
			enumerateAdapters();
		}

		GraphicsSystem(const GraphicsSystem& graphicsSystem) = delete;
		GraphicsSystem(GraphicsSystem&& graphicsSystem) = delete;

		~GraphicsSystem() = default;

		const GraphicsAdapterVector& adapters() const
		{
			return _adapters;
		}

		void addWindowInputEventHandler(Platform::WindowInputEventHandler& eventHandler)
		{
			_windowManager.addInputEventHandler(eventHandler);
		}

		void createContexts(const GraphicsDevice& device, const gsl::span<GraphicsContext>& contexts) const
		{
			RUNKO_ASSERT(!contexts.empty());

			const Vk::Semaphore timelineSemaphore = device.createSemaphore(0);

			for(size_t i = 0; i < contexts.size(); ++i)
				contexts[i] = GraphicsContext(device, timelineSemaphore, static_cast<uint32_t>(i));
		}

		GraphicsDevice createDevice(const size_t adapterIndex) const;

		Swapchain createSwapchain(const GraphicsDevice& device, const WindowHandle windowHandle,
			const Vk::ImageUsageFlags additionalUsageFlags = {}) const;

		WindowHandle createWindow(const Display& display, const uint32_t width, const uint32_t height);

		void destroyContexts(const GraphicsDevice& device, const gsl::span<GraphicsContext>& contexts) const
		{
			RUNKO_ASSERT(!contexts.empty());

			const Vk::Semaphore timelineSemaphore = contexts.front()._timelineSemaphore;

			for(GraphicsContext& context : contexts)
				context.reset();

			device.destroyObject(timelineSemaphore);
		}

		void destroyDevice(GraphicsDevice& device) const;

		void destroySwapchain(const GraphicsDevice& device, Swapchain& swapchain) const;

		void destroyWindow(const WindowHandle windowHandle);

		Bool isWindowOpen(const WindowHandle windowHandle) const
		{
			return _windowManager.isWindowOpen(windowHandle);
		}

		void processWindowMessages() const
		{
			_windowManager.processWindowMessages();
		}

		void removeWindowInputEventHandler(Platform::WindowInputEventHandler& eventHandler)
		{
			_windowManager.removeInputEventHandler(eventHandler);
		}

		void showWindow(const WindowHandle windowHandle) const
		{
			_windowManager.showWindow(windowHandle);
		}

		GraphicsSystem& operator =(const GraphicsSystem& graphicsSystem) = delete;
		GraphicsSystem& operator =(GraphicsSystem&& graphicsSystem) = delete;

	private:

		static constexpr std::string_view tag = "[Graphics::GraphicsSystem]";

		GraphicsAdapterVector _adapters;
		VulkanGraphics _graphics;
		Platform::WindowManager _windowManager;

		void enumerateAdapters();
		Vk::SurfaceKHR createSurface(const WindowHandle windowHandle) const;
		Bool checkDeviceSurfaceSupport(const size_t deviceIndex, const Display& display);
		void createAdapterVector(std::vector<DisplayVector>& displayVectors);
	};
}
