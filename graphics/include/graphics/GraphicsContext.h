/**
 * @file graphics/GraphicsContext.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <array>

#include <core/Types.h>
#include <core/Utility.h>
#include <graphics/CommandBuffer.h>
#include <graphics/GraphicsDevice.h>
#include <graphics/vulkan/Vulkan.h>

namespace Graphics
{
	class Swapchain;

	class GraphicsContext
	{
	public:

		GraphicsContext() = default;

		GraphicsContext(const GraphicsContext& graphicsContext) = delete;
		GraphicsContext(GraphicsContext&& graphicsContext) = default;

		~GraphicsContext() = default;

		void begin() const
		{
			_device->waitForSemaphore(_timelineSemaphore, _timelineSemaphoreWaitValue);
			_device->resetCommandPool(_commandPool);
			_commandBuffer.begin();
		}

		const CommandBuffer& commandBuffer() const
		{
			return _commandBuffer;
		}

		void end(const uint64_t timelineSemaphoreSignalValue)
		{
			_commandBuffer.end();
			submit(timelineSemaphoreSignalValue);
			_timelineSemaphoreWaitValue = timelineSemaphoreSignalValue;
		}

		uint32_t getNextSwapchainImage(const Swapchain& swapchain) const
		{
			return _device->getNextSwapchainImage(swapchain, _semaphores[0]).value;
		}

		void present(const Swapchain& swapchain, const uint32_t swapchainImageIndex) const
		{
			_device->present(swapchain, swapchainImageIndex, _semaphores[1]);
		}

		GraphicsContext& operator =(const GraphicsContext& graphicsContext) = delete;
		GraphicsContext& operator =(GraphicsContext&& graphicsContext) = default;

	private:

		friend class GraphicsSystem;

		GraphicsContext(const GraphicsDevice& device, const Vk::Semaphore timelineSemaphore,
			const uint32_t debugIndex);

		void submit(const uint64_t timelineSemaphoreSignalValue) const
		{
			const std::array signalSemaphores{ _semaphores[1], _timelineSemaphore };
			const std::array signalSemaphoreValues{ 0ull, timelineSemaphoreSignalValue };
			_device->submit(QueueType::Graphics, _commandBuffer, Core::toSpan(_semaphores[0]),
				Core::toSpan(0ull), signalSemaphores, signalSemaphoreValues);
		}

		void reset();

		CommandBuffer _commandBuffer;
		Vk::CommandPool _commandPool;
		const GraphicsDevice* _device = nullptr;
		std::array<Vk::Semaphore, 2> _semaphores;
		Vk::Semaphore _timelineSemaphore;
		uint64_t _timelineSemaphoreWaitValue = 0;
	};
}
