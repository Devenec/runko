/**
 * @file graphics/vulkan/VulkanGraphics.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <memory>
#include <limits>
#include <string_view>
#include <type_traits>
#include <utility>
#include <vector>

#include <core/Config.h>
#include <core/Types.h>
#include <graphics/vulkan/Vulkan.h>
#include <gsl/span>

VK_DEFINE_HANDLE(VmaAllocator)

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	#define RUNKO_VULKAN_ERROR(result, errorCode) \
		Graphics::VulkanGraphics::invokeError(result, static_cast<uint32_t>(errorCode), __FILE__, __LINE__)

#else

	#define RUNKO_VULKAN_ERROR(result, errorCode) \
		Graphics::VulkanGraphics::invokeError(result, static_cast<uint32_t>(errorCode))

#endif

namespace Platform
{
	struct SurfaceInfo;
}

namespace Graphics
{
	enum class Features
	{
		None      = 0,
		Debug     = 1,
		Swapchain = 2
	};

	struct Extension
	{
		const char* name;
		Features features;

		constexpr Extension(const char* name, const Features features) :
			name(name),
			features(features) { }
	};

	struct PhysicalDevice
	{
		static constexpr uint32_t invalidQueueFamilyIndex = std::numeric_limits<uint32_t>::max();

		Vk::PhysicalDevice object;
		uint32_t queueFamilyIndexGraphics = invalidQueueFamilyIndex;
		uint32_t queueFamilyIndexTransfer = invalidQueueFamilyIndex;

		explicit PhysicalDevice(const Vk::PhysicalDevice object) :
			object(object) { }
	};

	using DevicePair = std::pair<Vk::Device, std::unique_ptr<Vk::DeviceDispatch>>;

	inline Features operator |(const Features featuresA, const Features featuresB)
	{
		using Type = std::underlying_type_t<Features>;
		return static_cast<Features>(static_cast<Type>(featuresA) | static_cast<Type>(featuresB));
	}

	inline Features& operator |=(Features& featuresA, const Features featuresB)
	{
		featuresA = featuresA | featuresB;
		return featuresA;
	}

	class VulkanGraphics
	{
	public:

		explicit VulkanGraphics(const Bool enableSwapchain);

		VulkanGraphics(const VulkanGraphics& vulkanGraphics) = delete;
		VulkanGraphics(VulkanGraphics&& vulkanGraphics) = delete;

		~VulkanGraphics();

		Bool checkDeviceSurfaceSupport(const size_t deviceIndex, const Vk::SurfaceKHR surface) const;

		DevicePair createDevice(const PhysicalDevice& physicalDevice, const std::string_view& deviceName)
			const;

		VmaAllocator createMemoryAllocator(const size_t deviceIndex, const Vk::Device device,
			const Vk::DeviceDispatch& deviceDispatch) const;

		Vk::SurfaceKHR createSurface(const Platform::SurfaceInfo& surfaceInfo) const;

		void destroyDevice(const Vk::Device device, const std::unique_ptr<Vk::DeviceDispatch>& deviceDispatch)
			const
		{
			device.destroy(*deviceDispatch, {});
		}

		void destroyMemoryAllocator(const VmaAllocator allocator) const;

		void destroySurface(const Vk::SurfaceKHR surface) const
		{
			_instance.destroySurfaceKHR(*_instanceDispatch, surface, {});
		}

		const Vk::PhysicalDeviceProperties getPhysicalDeviceProperties(const size_t deviceIndex) const
		{
			return _physicalDevices[deviceIndex].object.getProperties(*_instanceDispatch);
		}

		Vk::SurfaceCapabilitiesKHR getSurfaceCapabilities(const size_t deviceIndex,
			const Vk::SurfaceKHR surface) const;

		std::vector<Vk::SurfaceFormatKHR> getSurfaceFormats(const size_t deviceIndex,
			const Vk::SurfaceKHR surface) const;

		const PhysicalDevice& physicalDevice(const size_t deviceIndex) const
		{
			return _physicalDevices[deviceIndex];
		}

		size_t physicalDeviceCount() const
		{
			return _physicalDevices.size();
		}

		VulkanGraphics& operator =(const VulkanGraphics& vulkanGraphics) = delete;
		VulkanGraphics& operator =(VulkanGraphics&& vulkanGraphics) = delete;

#if defined(RUNKO_CONFIG_DEVELOPMENT)

		static void invokeError(const Vk::Result result, const uint32_t errorCode, const char* filename,
			const uint32_t line);

#else

		static void invokeError(const Vk::Result result, const uint32_t errorCode);

#endif

	private:

		using ExtensionVector = std::vector<const char*>;

		static constexpr Features initialFeatures()
		{
#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

			return Features::Debug;

#else

			return Features::None;

#endif
		}

		Vk::Instance _instance;
		std::unique_ptr<Vk::InstanceDispatch> _instanceDispatch;
		std::vector<PhysicalDevice> _physicalDevices;
		Features _features = initialFeatures();

		void enumerateVersion() const;
		void enumerateLayers() const;
		void enumerateInstanceExtensions() const;
		void createInstance();
		void enumerateDevices();
		void enumerateDeviceExtensions(const Vk::PhysicalDevice device) const;
		ExtensionVector parseExtensions(const gsl::span<const Extension>& extensions) const;
		ExtensionVector parseInstanceExtensions() const;
		void queryDeviceQueueFamilies(PhysicalDevice& device) const;

		static gsl::span<const Extension> getInstanceExtensionsPlatform();
	};
}
