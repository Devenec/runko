/**
 * @file graphics/vulkan/VulkanMemoryAllocator.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Assert.h>
#include <core/Platform.h>

#define VMA_ASSERT                  RUNKO_ASSERT
#define VMA_STATIC_VULKAN_FUNCTIONS 0

#if defined(RUNKO_PLATFORM_COMPILER_MSVC)

	#pragma warning(push)
	#pragma warning(disable: 4100)
	#pragma warning(disable: 4127)
	#pragma warning(disable: 4189)
	#pragma warning(disable: 4324)
	#pragma warning(disable: 4701)
	#pragma warning(disable: 4703)

#endif

#include <vk_mem_alloc.h>

#if defined(RUNKO_PLATFORM_COMPILER_MSVC)

	#pragma warning(pop)

#endif
