/**
 * @file graphics/vulkan/VulkanDebugger.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <string_view>

#include <core/Config.h>
#include <core/Types.h>
#include <graphics/vulkan/Vulkan.h>

namespace Graphics
{
	class VulkanDebugger
	{
	public:

		VulkanDebugger() = default;

		VulkanDebugger(const VulkanDebugger& vulkanDebugger) = delete;
		VulkanDebugger(VulkanDebugger& vulkanDebugger) = delete;

		~VulkanDebugger() = default;

		void deinitialise(const Vk::Instance instance, const Vk::InstanceDispatch& instanceDispatch);

		void initialise(const Vk::Instance instance, const Vk::InstanceDispatch& instanceDispatch);

		template<typename T>
		void setObjectName(const Vk::Device device, const Vk::DeviceDispatch& deviceDispatch, const T object,
			const std::string_view& name) const
		{
#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

			const uint64_t handle = reinterpret_cast<uintptr_t>(object.handle());
			const Vk::DebugUtilsObjectNameInfoEXT nameInfo(T::cObjectType, handle, name.data());
			device.setDebugUtilsObjectNameEXT(deviceDispatch, nameInfo);

#else

			static_cast<void>(device);
			static_cast<void>(deviceDispatch);
			static_cast<void>(object);
			static_cast<void>(name);

#endif
		}

		VulkanDebugger& operator =(const VulkanDebugger& vulkanDebugger) = delete;
		VulkanDebugger& operator =(VulkanDebugger& vulkanDebugger) = delete;

	private:

#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

		Vk::DebugUtilsMessengerEXT _messenger;

#endif
	};

	extern VulkanDebugger vulkanDebugger;
}
