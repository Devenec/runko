/**
 * @file graphics/Common.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Types.h>
#include <graphics/vulkan/Vulkan.h>
#include <gsl/span>

VK_DEFINE_HANDLE(VmaAllocation)

namespace Graphics
{
	using MemoryAllocation = VmaAllocation;

	struct Buffer
	{
		MemoryAllocation memory = {};
		Vk::Buffer object;

		Buffer() = default;

		Buffer(const Vk::Buffer object, const MemoryAllocation memory) :
			memory(memory),
			object(object) { }
	};

	struct Framebuffer
	{
		Vk::Framebuffer object;
		Vk::Extent2D extent;

		Framebuffer() = default;

		Framebuffer(const Vk::Framebuffer object, const Vk::Extent2D extent) :
			object(object),
			extent(extent) { }
	};

	struct Image
	{
		MemoryAllocation memory = {};
		Vk::Image object;

		Image() = default;

		Image(const Vk::Image object, const MemoryAllocation memory) :
			memory(memory),
			object(object) { }
	};

	using ByteSpan = gsl::span<const std::byte>;
}
