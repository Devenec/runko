/**
 * @file graphics/MemoryAllocator.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <utility>

#include <core/Types.h>
#include <graphics/Common.h>
#include <graphics/vulkan/Vulkan.h>
#include <gsl/span>

VK_DEFINE_HANDLE(VmaAllocator)

namespace Graphics
{
	enum class MemoryUsage
	{
		Device,
		Host
	};

	class MemoryAllocator
	{
	public:

		MemoryAllocator() = default;

		MemoryAllocator(const MemoryAllocator& memoryAllocator) = delete;
		MemoryAllocator(MemoryAllocator&& memoryAllocator) = default;

		~MemoryAllocator() = default;

		MemoryAllocation allocate(const MemoryUsage usage, const Vk::Buffer buffer) const;

		MemoryAllocation allocate(const MemoryUsage usage, const Vk::Image image) const;

		void deallocate(const MemoryAllocation allocation) const;

		template<typename T = std::byte>
		gsl::span<T> mapMemory(const MemoryAllocation allocation) const
		{
			const MapMemoryPair pair = mapMemoryInternal(allocation);
			return gsl::span(static_cast<T*>(pair.first), pair.second);
		}

		void unmapMemory(const MemoryAllocation allocation) const;

		MemoryAllocator& operator =(const MemoryAllocator& memoryAllocator) = delete;
		MemoryAllocator& operator =(MemoryAllocator&& memoryAllocator) = default;

	private:

		friend class GraphicsDevice;
		friend class GraphicsSystem;

		using MapMemoryPair = std::pair<void*, Vk::DeviceSize>;

		VmaAllocator _allocator = nullptr;

		explicit MemoryAllocator(const VmaAllocator allocator) :
			_allocator(allocator) { }

		void reset()
		{
			_allocator = nullptr;
		}

		void bindMemory(const Vk::Buffer buffer, const MemoryAllocation allocation) const;
		void bindMemory(const Vk::Image image, const MemoryAllocation allocation) const;
		MapMemoryPair mapMemoryInternal(const MemoryAllocation allocation) const;
	};
}
