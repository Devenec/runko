/**
 * @file graphics/StagingManager.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <vector>

#include <core/Types.h>
#include <core/Utility.h>
#include <graphics/CommandBuffer.h>
#include <graphics/Common.h>
#include <graphics/GraphicsDevice.h>
#include <graphics/vulkan/Vulkan.h>
#include <gsl/span>

namespace Graphics
{
	class StagingManager
	{
	public:

		StagingManager() = default;

		StagingManager(const StagingManager& stagingManager) = delete;
		StagingManager(StagingManager&& stagingManager) = delete;

		~StagingManager() = default;

		void beginStaging()
		{
			_commandBuffers[_commandBufferIndex].begin();
		}

		void deinitialise();

		void endStaging();

		Buffer getStagingBuffer(const Vk::DeviceSize size)
		{
			return getBuffer(size);
		}

		void initialise(const GraphicsDevice& device, const uint32_t commandBufferCount);

		void stageBuffer(const Vk::Buffer sourceBuffer, const Vk::BufferCopy& region,
			const Vk::Buffer destinationBuffer, const Vk::PipelineStageFlags destinationStages,
			const Vk::AccessFlags destinationAccess) const
		{
			const CommandBuffer& commandBuffer = _commandBuffers[_commandBufferIndex];
			commandBuffer.copyBuffer(sourceBuffer, Core::toSpan(region), destinationBuffer);
			commandBuffer.bufferMemoryBarrier(destinationBuffer, region.dstOffset, region.size,
				Vk::PipelineStageFlagBits::Transfer, destinationStages, Vk::AccessFlagBits::TransferWrite,
				destinationAccess);
		}

		void stageBuffer(const ByteSpan& sourceData, const Vk::BufferCopy& region,
			const Vk::Buffer destinationBuffer, const Vk::PipelineStageFlags destinationStages,
			const Vk::AccessFlags destinationAccess)
		{
			const Buffer& buffer = getBuffer(sourceData.size_bytes());
			copyToBuffer(buffer, sourceData);
			stageBuffer(buffer.object, region, destinationBuffer, destinationStages, destinationAccess);
		}

		void stageImage(const Vk::Buffer sourceBuffer, const Vk::BufferImageCopy& region,
			const Vk::Image destinationImage, const Vk::PipelineStageFlags sourceStages,
			const Vk::PipelineStageFlags destinationStages, const Vk::ImageLayout oldLayout,
			const Vk::ImageLayout newLayout) const
		{
			const CommandBuffer& commandBuffer = _commandBuffers[_commandBufferIndex];
			commandBuffer.imageMemoryBarrier(destinationImage, oldLayout, Vk::ImageLayout::TransferDstOptimal,
				sourceStages, Vk::PipelineStageFlagBits::Transfer);

			commandBuffer.copyBufferToImage(sourceBuffer, Core::toSpan(region), destinationImage,
				Vk::ImageLayout::TransferDstOptimal);

			commandBuffer.imageMemoryBarrier(destinationImage, Vk::ImageLayout::TransferDstOptimal, newLayout,
				Vk::PipelineStageFlagBits::Transfer, destinationStages);
		}

		void stageImage(const ByteSpan& sourceData, const Vk::BufferImageCopy& region,
			const Vk::Image destinationImage, const Vk::PipelineStageFlags sourceStages,
			const Vk::PipelineStageFlags destinationStages, const Vk::ImageLayout oldLayout,
			const Vk::ImageLayout newLayout)
		{
			const Buffer& buffer = getBuffer(sourceData.size_bytes());
			copyToBuffer(buffer, sourceData);
			stageImage(buffer.object, region, destinationImage, sourceStages, destinationStages, oldLayout,
				newLayout);
		}

		StagingManager& operator =(const StagingManager& stagingManager) = delete;
		StagingManager& operator =(StagingManager&& stagingManager) = delete;

	private:

		struct StagingBuffer : Buffer
		{
			uint64_t semaphoreValue = 0;
			Vk::DeviceSize size = 0;

			StagingBuffer(const Buffer& buffer, const Vk::DeviceSize size, const uint64_t semaphoreValue) :
				Buffer(buffer),
				semaphoreValue(semaphoreValue),
				size(size) { }
		};

		std::vector<CommandBuffer> _commandBuffers;
		Vk::CommandPool _commandPool;
		const GraphicsDevice* _device = nullptr;
		std::vector<StagingBuffer> _stagingBuffers;
		Vk::Semaphore _stagingSemaphore;
		uint64_t _stagingSemaphoreValue = 0;
		uint32_t _commandBufferIndex = 0;

		const Buffer& getBuffer(const Vk::DeviceSize size);

		void copyToBuffer(const Buffer& buffer, const ByteSpan& data) const
		{
			const gsl::span memory = _device->mapMemory(buffer);
			std::copy(data.begin(), data.end(), memory.begin());
			_device->unmapMemory(buffer);
		}

		const Buffer& createBuffer(const Vk::DeviceSize size)
		{
			const Buffer buffer = createBufferObject(size);
			return _stagingBuffers.emplace_back(buffer, size, _stagingSemaphoreValue + 1);
		}

		void resizeBuffer(StagingBuffer& buffer, const Vk::DeviceSize size);
		Buffer createBufferObject(const Vk::DeviceSize size) const;
	};
}
