/**
 * @file graphics/Swapchain.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <utility>
#include <vector>

#include <core/Types.h>
#include <graphics/Common.h>
#include <graphics/vulkan/Vulkan.h>

namespace Graphics
{
	class Swapchain
	{
	public:

		struct Framebuffer
		{
			Graphics::Framebuffer framebuffer;
			Vk::Image image;
			Vk::ImageView view;
		};

		Swapchain() = default;

		Swapchain(const Swapchain& Swapchain) = delete;
		Swapchain(Swapchain&& swapchain) = default;

		~Swapchain() = default;

		Vk::Format colourImageFormat() const
		{
			return _colourImageFormat;
		}

		const Framebuffer& framebuffer(const uint32_t index) const
		{
			return _framebuffers[index];
		}

		Swapchain& operator =(const Swapchain& swapchain) = delete;
		Swapchain& operator =(Swapchain&& swapchain) = default;

	private:

		friend class GraphicsDevice;
		friend class GraphicsSystem;
		friend class SwapchainCreator;

		std::vector<Framebuffer> _framebuffers;
		Vk::SwapchainKHR _object;
		Vk::SurfaceKHR _surface;
		Vk::Format _colourImageFormat = Vk::Format::Undefined;

		Swapchain(const Vk::SurfaceKHR surface, const Vk::SwapchainKHR object,
			std::vector<Framebuffer>&& framebuffers, const Vk::Format colourImageFormat) :
			_framebuffers(std::move(framebuffers)),
			_object(object),
			_surface(surface),
			_colourImageFormat(colourImageFormat) { }

		void reset()
		{
			_surface = {};
			_object = {};
			_framebuffers.clear();
		}
	};
}
