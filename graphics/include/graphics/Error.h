/**
 * @file graphics/Error.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Types.h>

namespace Graphics
{
	/**
	 * Graphics error codes
	 *
	 * 0x 01     | XXXX      | XX
	 *    Module | Component | Error
	 */
	enum class Error : uint32_t
	{
		// Vulkan
		VulkanAcquireNextImage                     = 0x01000000,
		VulkanAllocateCommandBuffers               = 0x01000001,
		VulkanAllocateDescriptorSets               = 0x01000002,
		VulkanAllocateMemory                       = 0x01000003,
		VulkanBeginCommandBuffer                   = 0x01000004,
		VulkanBindBufferMemory                     = 0x01000005,
		VulkanBindImageMemory                      = 0x01000006,
		VulkanChooseSwapchainCompositeAlpha        = 0x01000007,
		VulkanCreateBuffer                         = 0x01000008,
		VulkanCreateCommandPool                    = 0x01000009,
		VulkanCreateDebugMessenger                 = 0x0100000A,
		VulkanCreateDescriptorPool                 = 0x0100000B,
		VulkanCreateDescriptorSetLayout            = 0x0100000C,
		VulkanCreateDevice                         = 0x0100000D,
		VulkanCreateFramebuffer                    = 0x0100000E,
		VulkanCreateImage                          = 0x0100000F,
		VulkanCreateImageView                      = 0x01000010,
		VulkanCreateMemoryAllocator                = 0x01000011,
		VulkanCreatePipeline                       = 0x01000012,
		VulkanCreatePipelineLayout                 = 0x01000013,
		VulkanCreateInstance                       = 0x01000014,
		VulkanCreateRenderPass                     = 0x01000015,
		VulkanCreateSampler                        = 0x01000016,
		VulkanCreateSemaphore                      = 0x01000017,
		VulkanCreateShaderModule                   = 0x01000018,
		VulkanCreateSurface                        = 0x01000019,
		VulkanCreateSwapchain                      = 0x0100001A,
		VulkanEndCommandBuffer                     = 0x0100001B,
		VulkanEnumerateDevices                     = 0x0100001C,
		VulkanEnumerateDeviceExtensions            = 0x0100001D,
		VulkanEnumerateInstanceExtensions          = 0x0100001E,
		VulkanEnumerateLayers                      = 0x0100001F,
		VulkanEnumerateVersion                     = 0x01000020,
		VulkanGetSurfaceCapabilities               = 0x01000021,
		VulkanGetSurfaceFormats                    = 0x01000022,
		VulkanGetSurfaceSupport                    = 0x01000023,
		VulkanGetSwapchainImages                   = 0x01000024,
		VulkanMapMemory                            = 0x01000025,
		VulkanPresent                              = 0x01000026,
		VulkanResetCommandPool                     = 0x01000027,
		VulkanResetDescriptorPool                  = 0x01000028,
		VulkanSubmit                               = 0x01000029,
		VulkanWaitIdle                             = 0x0100002A,
		VulkanWaitSemaphores                       = 0x0100002B,

		// Windows Graphics System
		WindowsGraphicsSystemGetDisplayMode        = 0x01000100,

		// Windows Window Input Manager
		WindowsInputManagerInitialiseRawInput      = 0x01000200,

		// Windows Window Manager
		WindowsWindowManagerCreateWindow           = 0x01000300,
		WindowsWindowManagerCreateWindowRectangle  = 0x01000301,
		WindowsWindowManagerDestroyWindow          = 0x01000302,
		WindowsWindowManagerGetWindowRectangle     = 0x01000303,
		WindowsWindowManagerGetWindowUserData      = 0x01000304,
		WindowsWindowManagerRegisterWindowClass    = 0x01000305,
		WindowsWindowManagerUnregisterWindowClass  = 0x01000306,
		WindowsWindowManagerSetWindowUserData      = 0x01000307
	};
}
