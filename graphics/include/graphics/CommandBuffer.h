/**
 * @file graphics/CommandBuffer.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Types.h>
#include <core/Utility.h>
#include <graphics/Common.h>
#include <graphics/vulkan/Vulkan.h>
#include <gsl/span>

namespace Graphics
{
	struct Framebuffer;

	class CommandBuffer
	{
	public:

		CommandBuffer() = default;

		CommandBuffer(const CommandBuffer& commandBuffer) = delete;
		CommandBuffer(CommandBuffer&& commandBuffer) = default;

		~CommandBuffer() = default;

		void begin() const;

		void beginRenderPass(const Vk::RenderPass renderPass, const Framebuffer& framebuffer,
			const gsl::span<const Vk::ClearValue>& clearValues) const;

		void bindDescriptorSets(const Vk::PipelineBindPoint pipelineBindPoint,
			const Vk::PipelineLayout pipelineLayout, const uint32_t firstSetIndex,
			const gsl::span<const Vk::DescriptorSet>& descriptorSets,
			const gsl::span<const uint32_t>& dynamicOffsets) const
		{
			_object.bindDescriptorSets(*_dispatch, pipelineBindPoint, pipelineLayout, firstSetIndex,
				descriptorSets, dynamicOffsets);
		}

		void bindIndexBuffer(const Vk::Buffer buffer, const Vk::IndexType indexType,
			const Vk::DeviceSize offset = 0) const
		{
			_object.bindIndexBuffer(*_dispatch, buffer, offset, indexType);
		}

		void bindPipeline(const Vk::PipelineBindPoint bindPoint, const Vk::Pipeline pipeline) const
		{
			_object.bindPipeline(*_dispatch, bindPoint, pipeline);
		}

		void bindVertexBuffer(const Vk::Buffer buffer, const Vk::DeviceSize offset = 0) const
		{
			_object.bindVertexBuffers(*_dispatch, 0, Core::toSpan(buffer), Core::toSpan(offset));
		}

		void bufferMemoryBarrier(const Vk::Buffer buffer, const Vk::DeviceSize offset,
			const Vk::DeviceSize size, const Vk::PipelineStageFlags sourceStages,
			const Vk::PipelineStageFlags destinationStages, const Vk::AccessFlags sourceAccess,
			const Vk::AccessFlags destinationAccess) const
		{
			const Vk::BufferMemoryBarrier memoryBarrier(sourceAccess, destinationAccess, 0, 0, buffer, offset,
				size);

			_object.pipelineBarrier(*_dispatch, sourceStages, destinationStages, Vk::DependencyFlags(), {},
				Core::toSpan(memoryBarrier), {});
		}

		void clearColourImage(const Vk::Image image, const Vk::ImageLayout layout,
			const Vk::ClearColorValue colour) const
		{
			const Vk::ImageSubresourceRange subresourceRange(Vk::ImageAspectFlagBits::Color, 0, 1, 0, 1);
			_object.clearColorImage(*_dispatch, image, layout, colour, Core::toSpan(subresourceRange));
		}

		void copyBuffer(const Vk::Buffer sourceBuffer, const gsl::span<const Vk::BufferCopy>& regions,
			const Vk::Buffer destinationBuffer) const
		{
			_object.copyBuffer(*_dispatch, sourceBuffer, destinationBuffer, regions);
		}

		void copyBufferToImage(const Vk::Buffer buffer, const gsl::span<const Vk::BufferImageCopy>& regions,
			const Vk::Image image, const Vk::ImageLayout imageLayout) const
		{
			_object.copyBufferToImage(*_dispatch, buffer, image, imageLayout, regions);
		}

		void draw(const uint32_t vertexCount, const uint32_t instanceCount = 1,
			const uint32_t firstVertex = 0, const uint32_t firstInstance = 0) const
		{
			_object.draw(*_dispatch, vertexCount, instanceCount, firstVertex, firstInstance);
		}

		void drawIndexed(const uint32_t indexCount, const uint32_t instanceCount = 1,
			const uint32_t firstIndex = 0, const uint32_t vertexOffset = 0, const uint32_t firstInstance = 0)
			const
		{
			_object.drawIndexed(*_dispatch, indexCount, instanceCount, firstIndex, vertexOffset,
				firstInstance);
		}

		void end() const;

		void endRenderPass() const
		{
			_object.endRenderPass(*_dispatch);
		}

		void imageMemoryBarrier(const Vk::Image image, const Vk::ImageLayout oldLayout,
			const Vk::ImageLayout newLayout) const;

		void imageMemoryBarrier(const Vk::Image image, const Vk::ImageLayout oldLayout,
			const Vk::ImageLayout newLayout, const Vk::PipelineStageFlags sourceStages,
			const Vk::PipelineStageFlags destinationStages) const;

		void pushConstants(const Vk::PipelineLayout pipelineLayout, const Vk::ShaderStageFlags shaderStages,
			const uint32_t offset, const ByteSpan& values) const
		{
			_object.pushConstants(*_dispatch, pipelineLayout, shaderStages, offset, values);
		}

		template<typename T>
		void pushConstants(const Vk::PipelineLayout pipelineLayout, const Vk::ShaderStageFlags shaderStages,
			const uint32_t offset, const gsl::span<const T>& values) const
		{
			const ByteSpan byteValues(reinterpret_cast<const std::byte*>(values.data()), values.size_bytes());
			pushConstants(pipelineLayout, shaderStages, offset, byteValues);
		}

		void setScissor(const uint32_t x, const uint32_t y, const uint32_t width, const uint32_t height) const
		{
			const Vk::Rect2D rectangle(x, y, width, height);
			_object.setScissor(*_dispatch, 0, Core::toSpan(rectangle));
		}

		void setViewport(const float x, const float y, const float width, const float height,
			const float minDepth, const float maxDepth) const
		{
			const Vk::Viewport viewport(x, y, width, height, minDepth, maxDepth);
			_object.setViewport(*_dispatch, 0, Core::toSpan(viewport));
		}

		CommandBuffer& operator =(const CommandBuffer& commandBuffer) = delete;
		CommandBuffer& operator =(CommandBuffer&& commandBuffer) = default;

	private:

		friend class GraphicsDevice;

		const Vk::DeviceDispatch* _dispatch = nullptr;
		Vk::CommandBuffer _object;

		CommandBuffer(const Vk::CommandBuffer object, const Vk::DeviceDispatch& deviceDispatch) :
			_dispatch(&deviceDispatch),
			_object(object) { }
	};
}
