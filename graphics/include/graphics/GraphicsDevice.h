/**
 * @file graphics/GraphicsDevice.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <array>
#include <limits>
#include <memory>
#include <string_view>
#include <utility>
#include <vector>

#include <core/Types.h>
#include <graphics/Common.h>
#include <graphics/MemoryAllocator.h>
#include <graphics/vulkan/Vulkan.h>
#include <graphics/vulkan/VulkanDebugger.h>
#include <gsl/span>

namespace Graphics
{
	class CommandBuffer;
	class GraphicsSystem;
	class Swapchain;

	enum class QueueType : uint32_t
	{
		Graphics,
		Transfer
	};

	struct GraphicsDeviceInfo
	{
		size_t deviceIndex;
		uint32_t queueFamilyIndexGraphics;
		uint32_t queueFamilyIndexTransfer;

		GraphicsDeviceInfo(const size_t deviceIndex, const uint32_t queueFamilyIndexGraphics,
			const uint32_t queueFamilyIndexTransfer) :
			deviceIndex(deviceIndex),
			queueFamilyIndexGraphics(queueFamilyIndexGraphics),
			queueFamilyIndexTransfer(queueFamilyIndexTransfer) { }
	};

	using DeviceDispatch = std::unique_ptr<Vk::DeviceDispatch>;

	class GraphicsDevice
	{
	public:

		using SemaphoreSpan = gsl::span<const Vk::Semaphore>;
		using Uint64Span    = gsl::span<const uint64_t>;

		static constexpr uint64_t maxTimeout = std::numeric_limits<uint64_t>::max();

		GraphicsDevice() = default;

		GraphicsDevice(const GraphicsDevice& graphicsDevice) = delete;
		GraphicsDevice(GraphicsDevice&& graphicsDevice) = default;

		~GraphicsDevice() = default;

		CommandBuffer allocateCommandBuffer(const Vk::CommandPool commandPool) const;

		std::vector<CommandBuffer> allocateCommandBuffers(const Vk::CommandPool commandPool,
			const uint32_t commandBufferCount) const;

		Vk::DescriptorSet allocateDescriptorSet(const Vk::DescriptorPool pool,
			const Vk::DescriptorSetLayout layout) const;

		Buffer createBuffer(const Vk::DeviceSize size, const Vk::BufferUsageFlags bufferUsage,
			const MemoryUsage memoryUsage) const;

		Vk::CommandPool createCommandPool(const QueueType queueType, const Bool allowCommandBufferReset)
			const;

		Vk::DescriptorPool createDescriptorPool(const uint32_t maxSetCount,
			const gsl::span<const Vk::DescriptorPoolSize>& poolSizes) const;

		Vk::DescriptorSetLayout createDescriptorSetLayout(
			const gsl::span<const Vk::DescriptorSetLayoutBinding>& bindings) const;

		Framebuffer createFramebuffer(const Vk::RenderPass renderPass, const Vk::Extent2D& extent,
			const gsl::span<const Vk::ImageView>& attachments) const;

		Image createImage(const Vk::Format format, const Vk::Extent2D extent,
			const Vk::ImageUsageFlags imageUsage, const MemoryUsage memoryUsage) const;

		Vk::ImageView createImageView(const Vk::Image image, const Vk::Format format) const;

		Vk::Pipeline createPipeline(const Vk::GraphicsPipelineCreateInfo& createInfo) const;

		Vk::PipelineLayout createPipelineLayout(
			const gsl::span<const Vk::DescriptorSetLayout>& descriptorSetLayouts,
			const gsl::span<const Vk::PushConstantRange>& pushConstantRanges) const;

		Vk::RenderPass createRenderPass(const Vk::RenderPassCreateInfo& createInfo) const;

		Vk::Sampler createSampler(const Vk::SamplerCreateInfo& createInfo) const;

		Vk::Semaphore createSemaphore() const;

		Vk::Semaphore createSemaphore(const uint64_t timelineSemaphoreInitialValue) const;

		Vk::ShaderModule createShaderModule(const ByteSpan& shaderCode) const;

		Vk::SwapchainKHR createSwapchain(const Vk::SwapchainCreateInfoKHR& createInfo) const;

		void destroyObject(const Buffer& buffer) const
		{
			_memoryAllocator.deallocate(buffer.memory);
			_object.destroyBuffer(*_dispatch, buffer.object, {});
		}

		void destroyObject(const Vk::CommandPool commandPool) const
		{
			_object.destroyCommandPool(*_dispatch, commandPool, {});
		}

		void destroyObject(const Vk::DescriptorPool descriptorPool) const
		{
			_object.destroyDescriptorPool(*_dispatch, descriptorPool, {});
		}

		void destroyObject(const Vk::DescriptorSetLayout descriptorSetLayout) const
		{
			_object.destroyDescriptorSetLayout(*_dispatch, descriptorSetLayout, {});
		}

		void destroyObject(const Framebuffer& framebuffer) const
		{
			_object.destroyFramebuffer(*_dispatch, framebuffer.object, {});
		}

		void destroyObject(const Image& image) const
		{
			_memoryAllocator.deallocate(image.memory);
			_object.destroyImage(*_dispatch, image.object, {});
		}

		void destroyObject(const Vk::ImageView imageView) const
		{
			_object.destroyImageView(*_dispatch, imageView, {});
		}

		void destroyObject(const Vk::Pipeline pipeline) const
		{
			_object.destroyPipeline(*_dispatch, pipeline, {});
		}

		void destroyObject(const Vk::PipelineLayout pipelineLayout) const
		{
			_object.destroyPipelineLayout(*_dispatch, pipelineLayout, {});
		}

		void destroyObject(const Vk::RenderPass renderPass) const
		{
			_object.destroyRenderPass(*_dispatch, renderPass, {});
		}

		void destroyObject(const Vk::Sampler sampler) const
		{
			_object.destroySampler(*_dispatch, sampler, {});
		}

		void destroyObject(const Vk::Semaphore semaphore) const
		{
			_object.destroySemaphore(*_dispatch, semaphore, {});
		}

		void destroyObject(const Vk::ShaderModule shaderModule) const
		{
			_object.destroyShaderModule(*_dispatch, shaderModule, {});
		}

		void destroyObject(const Vk::SwapchainKHR swapchain) const
		{
			_object.destroySwapchainKHR(*_dispatch, swapchain, {});
		}

		size_t deviceIndex() const
		{
			return _deviceIndex;
		}

		Vk::ResultPair<uint32_t> getNextSwapchainImage(const Swapchain& swapchain,
			const Vk::Semaphore signalSemaphore) const;

		std::vector<Vk::Image> getSwapchainImages(const Vk::SwapchainKHR swapchain) const;

		template<typename T = std::byte>
		gsl::span<T> mapMemory(const Buffer& buffer) const
		{
			return _memoryAllocator.mapMemory<T>(buffer.memory);
		}

		Vk::Result present(const Swapchain& swapchain, const uint32_t swapchainImageIndex,
			const Vk::Semaphore waitSemaphore) const;

		void resetCommandPool(const Vk::CommandPool commandPool) const;

		void resetDescriptorPool(const Vk::DescriptorPool descriptorPool) const;

		template<typename T>
		void setObjectName(const T& object, const std::string_view& name) const
		{
			vulkanDebugger.setObjectName(_object, *_dispatch, object, name);
		}

		template<>
		inline void setObjectName(const Buffer& buffer, const std::string_view& name) const
		{
			setObjectName(buffer.object, name);
		}

		template<>
		void setObjectName(const CommandBuffer& commandBuffer, const std::string_view& name) const;

		template<>
		inline void setObjectName(const Framebuffer& framebuffer, const std::string_view& name) const
		{
			setObjectName(framebuffer.object, name);
		}

		template<>
		inline void setObjectName(const Image& image, const std::string_view& name) const
		{
			setObjectName(image.object, name);
		}

		template<>
		void setObjectName(const Swapchain& swapchain, const std::string_view& name) const;

		void submit(const QueueType queueType, const CommandBuffer& commandBuffer,
			const SemaphoreSpan& waitSemaphores, const Uint64Span& waitSemaphoreValues,
			const SemaphoreSpan& signalSemaphores, const Uint64Span& signalSemaphoreValues) const;

		void unmapMemory(const Buffer& buffer) const
		{
			_memoryAllocator.unmapMemory(buffer.memory);
		}

		void updateDescriptorSets(const gsl::span<const Vk::WriteDescriptorSet>& descriptorWrites) const
		{
			_object.updateDescriptorSets(*_dispatch, descriptorWrites, {});
		}

		void waitForIdle() const;

		Vk::Result waitForSemaphore(const Vk::Semaphore semaphore, const uint64_t value,
			const uint64_t timeout = maxTimeout) const;

		GraphicsDevice& operator =(const GraphicsDevice& graphicsDevice) = delete;
		GraphicsDevice& operator =(GraphicsDevice&& graphicsDevice) = default;

	private:

		friend class GraphicsSystem;

		static constexpr uint32_t queueCount = 2;

		DeviceDispatch _dispatch;
		MemoryAllocator _memoryAllocator;
		Vk::Device _object;
		std::array<Vk::Queue, queueCount> _queues;
		size_t _deviceIndex = 0;
		std::array<uint32_t, queueCount> _queueFamilyIndices{};

		GraphicsDevice(const Vk::Device object, DeviceDispatch&& deviceDispatch,
			const GraphicsDeviceInfo& info, MemoryAllocator&& memoryAllocator) :
			_dispatch(std::move(deviceDispatch)),
			_memoryAllocator(std::move(memoryAllocator)),
			_object(object),
			_deviceIndex(info.deviceIndex),
			_queueFamilyIndices{ info.queueFamilyIndexGraphics, info.queueFamilyIndexTransfer }
		{
			_queues[static_cast<uint32_t>(QueueType::Graphics)] =
				_object.getQueue(*_dispatch, info.queueFamilyIndexGraphics, 0);

			_queues[static_cast<uint32_t>(QueueType::Transfer)] =
				_object.getQueue(*_dispatch, info.queueFamilyIndexTransfer, 0);
		}

		void reset()
		{
			_queues = {};
			_queueFamilyIndices = {};
			_deviceIndex = 0;
			_object = {};
			_memoryAllocator.reset();
			_dispatch.reset();
		}
	};
}
