/**
 * @file graphics/platform/WindowManager.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#if defined(RUNKO_PLATFORM_SYSTEM_WINDOWS)

	#include <graphics/platform/windows/WindowsWindowManager.h>

#endif

namespace Platform
{
#if defined(RUNKO_PLATFORM_SYSTEM_WINDOWS)

	using WindowManager = WindowsWindowManager;

#endif
}
