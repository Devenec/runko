/**
 * @file graphics/platform/BaseWindowManager.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <vector>

#include <core/Types.h>
#include <graphics/platform/WindowHandle.h>

namespace Graphics
{
	struct Display;

	using WindowHandle = size_t;
}

namespace Platform
{
	class BaseWindowManager
	{
	public:

		BaseWindowManager() = default;

		BaseWindowManager(const BaseWindowManager& windowManager) = delete;
		BaseWindowManager(BaseWindowManager&& windowManager) = delete;

		~BaseWindowManager() = default;

		Graphics::WindowHandle createWindow(const WindowHandle windowHandle)
		{
			auto iterator = std::find_if(_windows.begin(), _windows.end(), [](const Window& window)
			{
				return window.handle == WindowHandle();
			});

			if(iterator == _windows.end())
			{
				_windows.emplace_back(windowHandle);
				return _windows.size() - 1;
			}

			*iterator = Window(windowHandle);
			return static_cast<Graphics::WindowHandle>(iterator - _windows.begin());
		}

		WindowHandle destroyWindow(const Graphics::WindowHandle windowHandle)
		{
			const WindowHandle platformHandle = _windows[windowHandle].handle;
			_windows[windowHandle].handle = {};

			return platformHandle;
		}

		Bool isWindowOpen(const Graphics::WindowHandle windowHandle) const
		{
			return _windows[windowHandle].isOpen;
		}

		BaseWindowManager& operator =(const BaseWindowManager& windowManager) = delete;
		BaseWindowManager& operator =(BaseWindowManager&& windowManager) = delete;

	protected:

		struct Window
		{
			WindowHandle handle;
			uint32_t width = 0;
			uint32_t height = 0;
			Bool isOpen = true;

			explicit Window(const WindowHandle& handle) :
				handle(handle) { }
		};

		std::vector<Window> _windows;
	};
}
