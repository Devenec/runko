/**
 * @file graphics/platform/windows/WindowsWindowManager.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Types.h>
#include <core/platform/windows/WindowsForward.h>
#include <graphics/platform/BaseWindowManager.h>
#include <graphics/platform/windows/WindowsWindowInputManager.h>

namespace Core
{
	class Rectangle;
}

namespace Graphics
{
	struct Display;
}

namespace Platform
{
	class WindowInputEventHandler;

	class WindowsWindowManager : public BaseWindowManager
	{
	public:

		WindowsWindowManager();

		WindowsWindowManager(const WindowsWindowManager& windowManager) = delete;
		WindowsWindowManager(WindowsWindowManager&& windowManager) = delete;

		~WindowsWindowManager();

		void addInputEventHandler(WindowInputEventHandler& eventHandler)
		{
			_inputManager.addEventHandler(eventHandler);
		}

		Graphics::WindowHandle createWindow(const Graphics::Display& display, const uint32_t width,
			const uint32_t height);

		HWND createWindowForSurfaceSupportCheck(const Graphics::Display& display) const;

		void destroyWindow(const Graphics::WindowHandle windowHandle);

		void destroyWindow(const HWND windowHandle) const;

		HWND getNativeWindowHandle(const Graphics::WindowHandle windowHandle) const
		{
			return _windows[windowHandle].handle.nativeHandle;
		}

		uintptr_t getWindowHandleId(const Graphics::WindowHandle windowHandle) const
		{
			return reinterpret_cast<uintptr_t>(getNativeWindowHandle(windowHandle));
		}

		Core::Rectangle getWindowRectangle(const Graphics::WindowHandle windowHandle) const;

		void processWindowMessages() const;

		void removeInputEventHandler(WindowInputEventHandler& eventHandler)
		{
			_inputManager.removeEventHandler(eventHandler);
		}

		void showWindow(const Graphics::WindowHandle windowHandle) const;

		WindowsWindowManager& operator =(const WindowsWindowManager& windowManager) = delete;
		WindowsWindowManager& operator =(WindowsWindowManager&& windowManager) = delete;

	private:

		WindowsWindowInputManager _inputManager;

		void setWindowUserData(const HWND windowHandle) const;
		Window& getWindow(const HWND windowHandle);

		static LRESULT CALLBACK onWindowMessage(const HWND windowHandle, const uint32_t message,
			const WPARAM wordParameter, const LPARAM longParameter);

		static void onWindowClose(const HWND windowHandle);
		static Bool onWindowInput(const HWND windowHandle, const uint32_t message, const WPARAM wordParameter,
			const LPARAM longParameter);

		static Bool onWindowMouseMove(const HWND windowHandle, const LPARAM longParameter);
		static void onWindowSizeChanged(const HWND windowHandle, const LPARAM longParameter);
	};
}
