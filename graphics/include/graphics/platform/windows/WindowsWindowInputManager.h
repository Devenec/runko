/**
 * @file graphics/platform/windows/WindowsWindowInputManager.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <vector>

#include <core/Assert.h>
#include <core/Types.h>
#include <core/platform/windows/WindowsForward.h>
#include <graphics/platform/WindowInput.h>

struct tagRAWKEYBOARD;
using RAWKEYBOARD = tagRAWKEYBOARD;

namespace Platform
{
	class WindowsWindowInputManager
	{
	public:

		WindowsWindowInputManager()
		{
			initialiseRawInput();
		}

		WindowsWindowInputManager(const WindowsWindowInputManager& windowInputManager) = delete;
		WindowsWindowInputManager(WindowsWindowInputManager&& windowInputManager) = delete;

		~WindowsWindowInputManager() = default;

		void addEventHandler(WindowInputEventHandler& eventHandler)
		{
			_eventHandlers.push_back(&eventHandler);
		}

		Bool processMessage(const uint32_t message, const WPARAM wordParameter, const LPARAM longParameter,
			const uint32_t windowWidth = 0, const uint32_t windowHeight = 0) const;

		void removeEventHandler(WindowInputEventHandler& eventHandler)
		{
			auto iterator = std::find(_eventHandlers.cbegin(), _eventHandlers.cend(),
				&eventHandler);

			RUNKO_ASSERT(iterator != _eventHandlers.end());
			_eventHandlers.erase(iterator);
		}

		WindowsWindowInputManager& operator =(const WindowsWindowInputManager& windowInputManager) = delete;
		WindowsWindowInputManager& operator =(WindowsWindowInputManager&& windowInputManager) = delete;

	private:

		std::vector<WindowInputEventHandler*> _eventHandlers;

		void initialiseRawInput() const;
		void processCharacterMessage(const WPARAM wordParameter) const;
		void processRawInputMessage(const LPARAM longParameter) const;
		void processMouseButtonDownMessage(const uint32_t message) const;
		void processMouseButtonUpMessage(const uint32_t message) const;
		void processMouseMoveMessage(const LPARAM longParameter, const uint32_t windowWidth,
			const uint32_t windowHeight) const;

		void processMouseWheelMessage(const WPARAM wordParameter) const;
		void processRawInputKeyboardData(const RAWKEYBOARD& data) const;
	};
}
