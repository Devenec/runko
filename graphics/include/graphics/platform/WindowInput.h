/**
 * @file graphics/platform/WindowInput.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Input.h>
#include <core/Types.h>

namespace Platform
{
	class WindowInputEventHandler
	{
	public:

		enum class MouseButton
		{
			Left,
			Right
		};

		WindowInputEventHandler() = default;

		WindowInputEventHandler(const WindowInputEventHandler& windowInputEventHandler) = delete;
		WindowInputEventHandler(WindowInputEventHandler&& windowInputEventHandler) = delete;

		virtual ~WindowInputEventHandler() = default;

		virtual void onCharacter(const uint32_t character)
		{
			static_cast<void>(character);
		}

		virtual void onKey(const Core::KeyCode keyCode, const Bool pressed)
		{
			static_cast<void>(keyCode);
			static_cast<void>(pressed);
		}

		virtual void onMouseButton(const MouseButton button, const Bool pressed)
		{
			static_cast<void>(button);
			static_cast<void>(pressed);
		}

		virtual void onMouseMove(const uint32_t x, const uint32_t y)
		{
			static_cast<void>(x);
			static_cast<void>(y);
		}

		virtual void onMouseWheel(const int32_t direction, const int32_t delta)
		{
			static_cast<void>(direction);
			static_cast<void>(delta);
		}

		WindowInputEventHandler& operator =(const WindowInputEventHandler& windowInputEventHandler) = delete;
		WindowInputEventHandler& operator =(WindowInputEventHandler&& windowInputEventHandler) = delete;
	};
}
