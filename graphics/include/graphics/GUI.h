/**
 * @file graphics/GUI.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <string_view>
#include <vector>

#include <core/Input.h>
#include <core/Types.h>
#include <graphics/Common.h>
#include <graphics/StagingManager.h>
#include <graphics/vulkan/Vulkan.h>
#include <graphics/platform/WindowInput.h>

#define IMGUI_USER_CONFIG "UserConfig.h"

#include <imgui/imgui.h>

namespace Graphics
{
	class CommandBuffer;
	class GraphicsDevice;
	class GraphicsSystem;

	class GUI
	{
	public:

		GUI() = default;

		GUI(const GUI& gui) = delete;
		GUI(GUI&& gui) = delete;

		~GUI() = default;

		void begin(const float deltaTime, const Vk::Extent2D& displayExtent);

		void deinitialise(GraphicsSystem& graphicsSystem);

		void draw(const CommandBuffer& commandBuffer);

		void end();

		void initialise(GraphicsSystem& graphicsSystem, const GraphicsDevice& device,
			const Vk::RenderPass renderPass, const uint32_t drawBufferCount);

		GUI& operator =(const GUI& gui) = delete;
		GUI& operator =(GUI&& gui) = delete;

	private:

		class InputEventHandler : public Platform::WindowInputEventHandler
		{
		public:

			InputEventHandler() = default;

			InputEventHandler(const InputEventHandler& inputEventHandler) = delete;
			InputEventHandler(InputEventHandler&& inputEventHandler) = delete;

			~InputEventHandler() = default;

			void initialise()
			{
				io = &ImGui::GetIO();
			}

			void onCharacter(const uint32_t character)
			{
				io->AddInputCharacterUTF16(static_cast<ImWchar16>(character));
			}

			void onKey(const Core::KeyCode keyCode, const Bool pressed) override
			{
				io->KeysDown[static_cast<int32_t>(keyCode)] = pressed;

				switch(keyCode)
				{
					case Core::KeyCode::AltLeft:
						io->KeyAlt = pressed;
						break;

					case Core::KeyCode::ControlLeft:
					case Core::KeyCode::ControlRight:
						io->KeyCtrl = pressed;
						break;

					case Core::KeyCode::ShiftLeft:
					case Core::KeyCode::ShiftRight:
						io->KeyShift = pressed;
						break;

					default:
						break;
				}
			}

			void onMouseButton(const MouseButton button, const Bool pressed) override
			{
				io->MouseDown[static_cast<int32_t>(button)] = pressed;
			}

			void onMouseMove(const uint32_t x, const uint32_t y) override
			{
				io->MousePos = ImVec2(static_cast<float>(x), static_cast<float>(y));
			}

			void onMouseWheel(const int32_t direction, const int32_t delta) override
			{
				static_cast<void>(delta);

				io->MouseWheel = static_cast<float>(direction);
			}

			InputEventHandler& operator =(const InputEventHandler& inputEventHandler) = delete;
			InputEventHandler& operator =(InputEventHandler&& inputEventHandler) = delete;

		private:

			ImGuiIO* io = nullptr;
		};

		struct DrawBuffer : Buffer
		{
			Vk::DeviceSize size = 0;

			DrawBuffer() = default;

			DrawBuffer(const Buffer& buffer, const Vk::DeviceSize size) :
				Buffer(buffer),
				size(size) { }
		};

		struct DrawBuffers
		{
			DrawBuffer indexBuffer;
			DrawBuffer vertexBuffer;
		};

		Vk::DescriptorPool _descriptorPool;
		Vk::DescriptorSet _descriptorSet;
		Vk::DescriptorSetLayout _descriptorSetLayout;
		std::vector<DrawBuffers> _drawBuffers;
		Image _fontImage;
		Vk::ImageView _fontImageView;
		InputEventHandler _inputEventHandler;
		Vk::PipelineLayout _pipelineLayout;
		const GraphicsDevice* _device = nullptr;
		Vk::Pipeline _pipeline;
		Vk::Sampler _sampler;
		StagingManager _stagingManager;
		uint32_t _drawBuffersIndex = 0;

		void updateIo(const float deltaTime, const Vk::Extent2D& displayExtent);
		void destroyDeviceObjects();
		const DrawBuffers& getDrawBuffers(const ImDrawData& drawData);
		void stageDrawBuffers(const DrawBuffers& buffers, const ImDrawData& drawData);
		void draw(const CommandBuffer& commandBuffer, const DrawBuffers& buffers, const ImDrawData& drawData)
			const;

		void initialiseImGui();
		void createDeviceObjects(const Vk::RenderPass renderPass);
		void resizeDrawBuffer(DrawBuffer& drawBuffer, const Vk::DeviceSize size,
			const Vk::BufferUsageFlags usage);

		void setDrawState(const CommandBuffer& commandBuffer, const DrawBuffers& buffers,
			const ImDrawData& drawData) const;

		void setScissor(const CommandBuffer& commandBuffer, const ImVec4& rectangle) const;
		void createSampler();
		void createLayouts();
		void createPipeline(const Vk::RenderPass renderPass);
		void createAndStageFontImage();
		void initialiseDescriptors();
	};
}
