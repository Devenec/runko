/**
 * @file graphics/MemoryAllocator.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/MemoryAllocator.h>

#define VMA_IMPLEMENTATION

#include <graphics/Error.h>
#include <graphics/vulkan/VulkanGraphics.h>
#include <graphics/vulkan/VulkanMemoryAllocator.h>

using namespace Graphics;

// Internal

static Vk::MemoryPropertyFlags getPreferredMemoryPropertyFlags(const MemoryUsage usage);
static Vk::MemoryPropertyFlags getRequiredMemoryPropertyFlags(const MemoryUsage usage);
static VmaAllocationCreateInfo initialiseCreateInfo(const MemoryUsage usage);


// Public

MemoryAllocation MemoryAllocator::allocate(const MemoryUsage usage, const Vk::Buffer buffer) const
{
	const VmaAllocationCreateInfo createInfo = initialiseCreateInfo(usage);
	VmaAllocation allocation = {};
	const VkResult result = vmaAllocateMemoryForBuffer(_allocator, buffer.handle(), &createInfo, &allocation,
		nullptr);

	if(result == VK_SUCCESS)
		bindMemory(buffer, allocation);
	else
		RUNKO_VULKAN_ERROR(static_cast<Vk::Result>(result), Error::VulkanAllocateMemory);

	return allocation;
}

MemoryAllocation MemoryAllocator::allocate(const MemoryUsage usage, const Vk::Image image) const
{
	const VmaAllocationCreateInfo createInfo = initialiseCreateInfo(usage);
	VmaAllocation allocation = {};
	const VkResult result = vmaAllocateMemoryForImage(_allocator, image.handle(), &createInfo, &allocation,
		nullptr);

	if(result == VK_SUCCESS)
		bindMemory(image, allocation);
	else
		RUNKO_VULKAN_ERROR(static_cast<Vk::Result>(result), Error::VulkanAllocateMemory);

	return allocation;
}

void MemoryAllocator::deallocate(const MemoryAllocation allocation) const
{
	vmaFreeMemory(_allocator, allocation);
}

// Private

void MemoryAllocator::bindMemory(const Vk::Buffer buffer, const MemoryAllocation allocation) const
{
	const VkResult result = vmaBindBufferMemory(_allocator, allocation, buffer.handle());

	if(result != VK_SUCCESS)
		RUNKO_VULKAN_ERROR(static_cast<Vk::Result>(result), Error::VulkanBindBufferMemory);
}

void MemoryAllocator::bindMemory(const Vk::Image image, const MemoryAllocation allocation) const
{
	const VkResult result = vmaBindImageMemory(_allocator, allocation, image.handle());

	if(result != VK_SUCCESS)
		RUNKO_VULKAN_ERROR(static_cast<Vk::Result>(result), Error::VulkanBindImageMemory);
}

MemoryAllocator::MapMemoryPair MemoryAllocator::mapMemoryInternal(const MemoryAllocation allocation) const
{
	MapMemoryPair pair{};
	const VkResult result = vmaMapMemory(_allocator, allocation, &pair.first);

	if(result == VK_SUCCESS)
	{
		VmaAllocationInfo allocationInfo;
		vmaGetAllocationInfo(_allocator, allocation, &allocationInfo);
		pair.second = allocationInfo.size;
	}
	else
	{
		RUNKO_VULKAN_ERROR(static_cast<Vk::Result>(result), Error::VulkanMapMemory);
	}

	return pair;
}

void MemoryAllocator::unmapMemory(const MemoryAllocation allocation) const
{
	vmaUnmapMemory(_allocator, allocation);
}


// Internal

static Vk::MemoryPropertyFlags getPreferredMemoryPropertyFlags(const MemoryUsage usage)
{
	switch(usage)
	{
		case MemoryUsage::Device:
			return {};

		case MemoryUsage::Host:
			return Vk::MemoryPropertyFlagBits::HostCached;
	}

	return {};
}

static Vk::MemoryPropertyFlags getRequiredMemoryPropertyFlags(const MemoryUsage usage)
{
	switch(usage)
	{
		case MemoryUsage::Device:
			return Vk::MemoryPropertyFlagBits::DeviceLocal;

		case MemoryUsage::Host:
			return Vk::MemoryPropertyFlagBits::HostVisible | Vk::MemoryPropertyFlagBits::HostCoherent;
	}

	return {};
}

static VmaAllocationCreateInfo initialiseCreateInfo(const MemoryUsage usage)
{
	VmaAllocationCreateInfo createInfo{};
	createInfo.preferredFlags = getPreferredMemoryPropertyFlags(usage).mask();
	createInfo.requiredFlags = getRequiredMemoryPropertyFlags(usage).mask();

	return createInfo;
}
