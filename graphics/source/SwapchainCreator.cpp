/**
 * @file graphics/SwapchainCreator.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/SwapchainCreator.h>

#include <algorithm>
#include <array>
#include <utility>
#include <vector>

#include <core/Utility.h>
#include <graphics/Error.h>
#include <graphics/GraphicsDevice.h>
#include <graphics/vulkan/VulkanGraphics.h>
#include <gsl/span>

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	#include <core/StaticString.h>

#endif

using namespace Core;
using namespace Graphics;

// Internal

static constexpr std::array preferredSwapchainColourFormats
{
	Vk::Format::R8G8B8A8Srgb,
	Vk::Format::B8G8R8A8Srgb,
	Vk::Format::R8G8B8A8Unorm,
	Vk::Format::B8G8R8A8Unorm
};

static constexpr uint32_t preferredSwapchainImageCount = 3;
static constexpr uint32_t specialImageExtent           = 0xFFFFFFFF;


// Public

Swapchain SwapchainCreator::createSwapchain(const GraphicsDevice& device, const Vk::SurfaceKHR surface,
	const Vk::Extent2D& surfaceExtent, const Vk::ImageUsageFlags additionalUsageFlags)
{
	_device = &device;
	_surface = surface;
	_surfaceCapabilities = _graphics.getSurfaceCapabilities(device.deviceIndex(), surface);
	_surfaceFormat = chooseSurfaceFormat();
	initialiseImageExtent(surfaceExtent);
	const Vk::SwapchainKHR swapchain = createSwapchainObject(additionalUsageFlags);
	const Vk::RenderPass renderPass = createRenderPass();
	std::vector framebuffers = createSwapchainFramebuffers(swapchain, renderPass);
	_device->destroyObject(renderPass);

	return Swapchain(surface, swapchain, std::move(framebuffers), _surfaceFormat.format);
}

// Static

void SwapchainCreator::destroySwapchain(const GraphicsDevice& device, Swapchain& swapchain)
{
	for(const Swapchain::Framebuffer& framebuffer : swapchain._framebuffers)
	{
		device.destroyObject(framebuffer.framebuffer);
		device.destroyObject(framebuffer.view);
	}

	device.destroyObject(swapchain._object);
	swapchain.reset();
}

// Private

Vk::SurfaceFormatKHR SwapchainCreator::chooseSurfaceFormat() const
{
	const std::vector formats = _graphics.getSurfaceFormats(_device->deviceIndex(), _surface);

	if(formats.size() == 1 && formats.front().format == Vk::Format::Undefined)
		return Vk::SurfaceFormatKHR(preferredSwapchainColourFormats.front(), formats.front().colorSpace);

	for(const Vk::Format colourFormat : preferredSwapchainColourFormats)
	{
		for(const Vk::SurfaceFormatKHR& surfaceFormat : formats)
		{
			if(surfaceFormat.format == colourFormat)
				return surfaceFormat;
		}
	}

	return formats.front();
}

void SwapchainCreator::initialiseImageExtent(const Vk::Extent2D& surfaceExtent)
{
	if(_surfaceCapabilities.currentExtent.width != specialImageExtent &&
		_surfaceCapabilities.currentExtent.height != specialImageExtent)
	{
		_imageExtent = _surfaceCapabilities.currentExtent;
		return;
	}

	_imageExtent.width = std::clamp(surfaceExtent.width, _surfaceCapabilities.minImageExtent.width,
		_surfaceCapabilities.maxImageExtent.width);

	_imageExtent.height = std::clamp(surfaceExtent.height, _surfaceCapabilities.minImageExtent.height,
		_surfaceCapabilities.maxImageExtent.height);
}

Vk::SwapchainKHR SwapchainCreator::createSwapchainObject(const Vk::ImageUsageFlags additionalUsageFlags) const
{
	const Vk::SwapchainCreateInfoKHR createInfo
	(
		{},
		_surface,
		getMinSwapchainImageCount(),
		_surfaceFormat.format, _surfaceFormat.colorSpace,
		_imageExtent,
		1,
		additionalUsageFlags | Vk::ImageUsageFlagBits::ColorAttachment,
		Vk::SharingMode::Exclusive, {},
		_surfaceCapabilities.currentTransform,
		chooseSwapchainCompositeAlpha(),
		Vk::PresentModeKHR::Fifo,
		true,
		Vk::SwapchainKHR()
	);

	return _device->createSwapchain(createInfo);
}

Vk::RenderPass SwapchainCreator::createRenderPass() const
{
	const Vk::AttachmentDescription attachment({}, _surfaceFormat.format, Vk::SampleCountFlagBits::e1,
		Vk::AttachmentLoadOp::DontCare, Vk::AttachmentStoreOp::DontCare, Vk::AttachmentLoadOp::DontCare,
		Vk::AttachmentStoreOp::DontCare, Vk::ImageLayout::Undefined, Vk::ImageLayout::PresentSrcKHR);

	const Vk::AttachmentReference colourAttachment(0, Vk::ImageLayout::ColorAttachmentOptimal);
	const Vk::SubpassDescription subpass({}, Vk::PipelineBindPoint::Graphics, {}, toSpan(colourAttachment));
	const Vk::RenderPassCreateInfo createInfo({}, toSpan(attachment), toSpan(subpass));

	return _device->createRenderPass(createInfo);
}

std::vector<Swapchain::Framebuffer> SwapchainCreator::createSwapchainFramebuffers(
	const Vk::SwapchainKHR swapchain, const Vk::RenderPass renderPass) const
{
	const std::vector images = _device->getSwapchainImages(swapchain);
	std::vector<Swapchain::Framebuffer> framebuffers(images.size());

	for(size_t i = 0; i < framebuffers.size(); ++i)
	{
		framebuffers[i].image = images[i];
		framebuffers[i].view = _device->createImageView(images[i], _surfaceFormat.format);
		const gsl::span attachments(&(framebuffers[i].view), 1);
		framebuffers[i].framebuffer = _device->createFramebuffer(renderPass, _imageExtent, attachments);

#if defined(RUNKO_CONFIG_DEVELOPMENT)

		const Core::StaticString name = Core::StaticString<15>::format("Swapchain {}", i);
		_device->setObjectName(framebuffers[i].framebuffer, name);
		_device->setObjectName(framebuffers[i].image, name);
		_device->setObjectName(framebuffers[i].view, name);

#endif
	}

	return framebuffers;
}

uint32_t SwapchainCreator::getMinSwapchainImageCount() const
{
	const uint32_t maxImageCount = _surfaceCapabilities.maxImageCount == 0 ? preferredSwapchainImageCount :
		_surfaceCapabilities.maxImageCount;

	return std::clamp(preferredSwapchainImageCount, _surfaceCapabilities.minImageCount, maxImageCount);
}

Vk::CompositeAlphaFlagBitsKHR SwapchainCreator::chooseSwapchainCompositeAlpha() const
{
	constexpr Vk::CompositeAlphaFlagsKHR supportedFlags = Vk::CompositeAlphaFlagBitsKHR::Opaque |
		Vk::CompositeAlphaFlagBitsKHR::PostMultiplied;

	if(!_surfaceCapabilities.supportedCompositeAlpha.any(supportedFlags))
		RUNKO_VULKAN_ERROR(Vk::Result::ErrorFormatNotSupported, Error::VulkanChooseSwapchainCompositeAlpha);

	return _surfaceCapabilities.supportedCompositeAlpha.in(Vk::CompositeAlphaFlagBitsKHR::Opaque) ?
		Vk::CompositeAlphaFlagBitsKHR::Opaque : Vk::CompositeAlphaFlagBitsKHR::PostMultiplied;
}
