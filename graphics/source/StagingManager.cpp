/**
 * @file graphics/StagingManager.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/StagingManager.h>

#include <core/Config.h>

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	#include <core/Assert.h>
	#include <core/StaticString.h>

#endif

using namespace Graphics;

// Public

void StagingManager::deinitialise()
{
	for(const StagingBuffer& buffer : _stagingBuffers)
		_device->destroyObject(buffer);

	_stagingBuffers.clear();
	_commandBufferIndex = 0;
	_stagingSemaphoreValue = 0;
	_device->destroyObject(_stagingSemaphore);
	_stagingSemaphore = {};
	_commandBuffers.clear();
	_device->destroyObject(_commandPool);
	_commandPool = {};
	_device = nullptr;
}

void StagingManager::endStaging()
{
	const CommandBuffer& commandBuffer = _commandBuffers[_commandBufferIndex];
	commandBuffer.end();
	++_stagingSemaphoreValue;
	_device->submit(QueueType::Graphics, commandBuffer, {}, {}, Core::toSpan(_stagingSemaphore),
		Core::toSpan(_stagingSemaphoreValue));

	_commandBufferIndex = (_commandBufferIndex + 1) % static_cast<uint32_t>(_commandBuffers.size());
}

void StagingManager::initialise(const GraphicsDevice& device, const uint32_t commandBufferCount)
{
	_device = &device;
	_commandPool = device.createCommandPool(QueueType::Graphics, true);
	_commandBuffers = device.allocateCommandBuffers(_commandPool, commandBufferCount);
	_stagingSemaphore = device.createSemaphore(_stagingSemaphoreValue);

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	RUNKO_ASSERT(_commandBuffers.size() < 1000);
	device.setObjectName(_commandPool, "Staging");

	for(size_t i = 0; i < _commandBuffers.size(); ++i)
	{
		const Core::StaticString name = Core::StaticString<15>::format("Staging {}", i);
		device.setObjectName(_commandBuffers[i], name);
	}

	device.setObjectName(_stagingSemaphore, "Staging");

#endif
}

// Private

const Buffer& StagingManager::getBuffer(const Vk::DeviceSize size)
{
	if(_stagingBuffers.empty())
		return createBuffer(size);

	auto compareFunction = [](const StagingBuffer& bufferA, const StagingBuffer& bufferB)
	{
		if(bufferA.semaphoreValue == bufferB.semaphoreValue)
			return bufferA.size > bufferB.size;

		return bufferA.semaphoreValue < bufferB.semaphoreValue;
	};

	auto iterator = std::min_element(_stagingBuffers.begin(), _stagingBuffers.end(), compareFunction);
	const Vk::Result result = _device->waitForSemaphore(_stagingSemaphore, iterator->semaphoreValue, 0);

	if(result != Vk::Result::Success)
		return createBuffer(size);

	if(size > iterator->size)
		resizeBuffer(*iterator, size);

	iterator->semaphoreValue = _stagingSemaphoreValue + 1;
	return *iterator;
}

void StagingManager::resizeBuffer(StagingBuffer& buffer, const Vk::DeviceSize size)
{
	_device->destroyObject(buffer);
	buffer = StagingBuffer(createBufferObject(size), size, 0);
}

Buffer StagingManager::createBufferObject(const Vk::DeviceSize size) const
{
	const Buffer buffer = _device->createBuffer(size, Vk::BufferUsageFlagBits::TransferSrc,
		MemoryUsage::Host);

	_device->setObjectName(buffer, "Staging");
	return buffer;
}
