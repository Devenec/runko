/**
 * @file graphics/GraphicsDevice.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/GraphicsDevice.h>

#include <core/Utility.h>
#include <graphics/CommandBuffer.h>
#include <graphics/Error.h>
#include <graphics/Swapchain.h>
#include <graphics/vulkan/VulkanGraphics.h>

using namespace Core;
using namespace Graphics;

// Public

CommandBuffer GraphicsDevice::allocateCommandBuffer(const Vk::CommandPool commandPool) const
{
	const Vk::CommandBufferAllocateInfo allocateInfo(commandPool, Vk::CommandBufferLevel::Primary, 1);
	Vk::CommandBuffer commandBuffer;
	const Vk::Result result = _object.allocateCommandBuffers(*_dispatch, allocateInfo, toSpan(commandBuffer));

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanAllocateCommandBuffers);

	return CommandBuffer(commandBuffer, *_dispatch);
}

std::vector<CommandBuffer> GraphicsDevice::allocateCommandBuffers(const Vk::CommandPool commandPool,
	const uint32_t commandBufferCount) const
{
	const Vk::CommandBufferAllocateInfo allocateInfo(commandPool, Vk::CommandBufferLevel::Primary,
		commandBufferCount);

	const Vk::ResultPair result = _object.allocateCommandBuffers(*_dispatch, allocateInfo);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanAllocateCommandBuffers);

	std::vector<CommandBuffer> commandBuffers(commandBufferCount);

	for(uint32_t i = 0; i < commandBufferCount; ++i)
		commandBuffers[i] = CommandBuffer(result.value[i], *_dispatch);

	return commandBuffers;
}

Vk::DescriptorSet GraphicsDevice::allocateDescriptorSet(const Vk::DescriptorPool pool,
	const Vk::DescriptorSetLayout layout) const
{
	const Vk::DescriptorSetAllocateInfo allocateInfo(pool, toSpan(layout));
	Vk::DescriptorSet descriptorSet;
	const Vk::Result result = _object.allocateDescriptorSets(*_dispatch, allocateInfo, toSpan(descriptorSet));

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanAllocateDescriptorSets);

	return descriptorSet;
}

Buffer GraphicsDevice::createBuffer(const Vk::DeviceSize size, const Vk::BufferUsageFlags bufferUsage,
	const MemoryUsage memoryUsage) const
{
	const Vk::BufferCreateInfo createInfo({}, size, bufferUsage, Vk::SharingMode::Exclusive, {});
	const Vk::ResultPair result = _object.createBuffer(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateBuffer);

	const MemoryAllocation memory = _memoryAllocator.allocate(memoryUsage, result.value);
	return Buffer(result.value, memory);
}

Vk::CommandPool GraphicsDevice::createCommandPool(const QueueType queueType,
	const Bool allowCommandBufferReset) const
{
	Vk::CommandPoolCreateFlags flags = Vk::CommandPoolCreateFlagBits::Transient;

	if(allowCommandBufferReset)
		flags |= Vk::CommandPoolCreateFlagBits::ResetCommandBuffer;

	const uint32_t queueFamilyIndex = _queueFamilyIndices[static_cast<uint32_t>(queueType)];
	const Vk::CommandPoolCreateInfo createInfo(flags, queueFamilyIndex);
	const Vk::ResultPair result = _object.createCommandPool(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateCommandPool);

	return result.value;
}

Vk::DescriptorPool GraphicsDevice::createDescriptorPool(const uint32_t maxSetCount,
	const gsl::span<const Vk::DescriptorPoolSize>& poolSizes) const
{
	const Vk::DescriptorPoolCreateInfo createInfo({}, maxSetCount, poolSizes);
	const Vk::ResultPair result = _object.createDescriptorPool(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateDescriptorPool);

	return result.value;
}

Vk::DescriptorSetLayout GraphicsDevice::createDescriptorSetLayout(
	const gsl::span<const Vk::DescriptorSetLayoutBinding>& bindings) const
{
	const Vk::DescriptorSetLayoutCreateInfo createInfo({}, bindings);
	const Vk::ResultPair result = _object.createDescriptorSetLayout(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateDescriptorSetLayout);

	return result.value;
}

Framebuffer GraphicsDevice::createFramebuffer(const Vk::RenderPass renderPass,
	const Vk::Extent2D& extent, const gsl::span<const Vk::ImageView>& attachments) const
{
	const Vk::FramebufferCreateInfo createInfo({}, renderPass, attachments, extent.width, extent.height, 1);
	const Vk::ResultPair result = _object.createFramebuffer(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateFramebuffer);

	return Framebuffer(result.value, extent);
}

Image GraphicsDevice::createImage(const Vk::Format format, const Vk::Extent2D extent,
	const Vk::ImageUsageFlags imageUsage, const MemoryUsage memoryUsage) const
{
	const Vk::Extent3D extent3D(extent.width, extent.height, 1);
	const Vk::ImageCreateInfo createInfo({}, Vk::ImageType::e2D, format, extent3D, 1, 1,
		Vk::SampleCountFlagBits::e1, Vk::ImageTiling::Optimal, imageUsage, Vk::SharingMode::Exclusive, {},
		Vk::ImageLayout::Undefined);

	const Vk::ResultPair result = _object.createImage(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateImage);

	const MemoryAllocation memory = _memoryAllocator.allocate(memoryUsage, result.value);
	return Image(result.value, memory);
}

Vk::ImageView GraphicsDevice::createImageView(const Vk::Image image, const Vk::Format format) const
{
	const Vk::ImageSubresourceRange subresourceRange(Vk::ImageAspectFlagBits::Color, 0, 1, 0, 1);
	const Vk::ImageViewCreateInfo createInfo({}, image, Vk::ImageViewType::e2D, format,
		Vk::ComponentMapping(), subresourceRange);

	const Vk::ResultPair result = _object.createImageView(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateImageView);

	return result.value;
}

Vk::Pipeline GraphicsDevice::createPipeline(const Vk::GraphicsPipelineCreateInfo& createInfo) const
{
	Vk::Pipeline pipeline;
	const Vk::Result result = _object.createGraphicsPipelines(*_dispatch, Vk::PipelineCache(),
		toSpan(createInfo), {}, toSpan(pipeline));

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanCreatePipeline);

	return pipeline;
}

Vk::PipelineLayout GraphicsDevice::createPipelineLayout(
	const gsl::span<const Vk::DescriptorSetLayout>& descriptorSetLayouts,
	const gsl::span<const Vk::PushConstantRange>& pushConstantRanges) const
{
	const Vk::PipelineLayoutCreateInfo createInfo({}, descriptorSetLayouts, pushConstantRanges);
	const Vk::ResultPair result = _object.createPipelineLayout(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreatePipelineLayout);

	return result.value;
}

Vk::RenderPass GraphicsDevice::createRenderPass(const Vk::RenderPassCreateInfo& createInfo) const
{
	const Vk::ResultPair result = _object.createRenderPass(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateRenderPass);

	return result.value;
}

Vk::Sampler GraphicsDevice::createSampler(const Vk::SamplerCreateInfo& createInfo) const
{
	const Vk::ResultPair result = _object.createSampler(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateSampler);

	return result.value;
}

Vk::Semaphore GraphicsDevice::createSemaphore() const
{
	const Vk::ResultPair result = _object.createSemaphore(*_dispatch, Vk::SemaphoreCreateInfo(), {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateSemaphore);

	return result.value;
}

Vk::Semaphore GraphicsDevice::createSemaphore(const uint64_t timelineSemaphoreInitialValue) const
{
	Vk::SemaphoreCreateInfo createInfo;
	const Vk::SemaphoreTypeCreateInfo typeInfo(Vk::SemaphoreType::Timeline, timelineSemaphoreInitialValue);
	createInfo.next = &typeInfo;
	const Vk::ResultPair result = _object.createSemaphore(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateSemaphore);

	return result.value;
}

Vk::ShaderModule GraphicsDevice::createShaderModule(const gsl::span<const std::byte>& shaderCode) const
{
	const gsl::span code(reinterpret_cast<const uint32_t*>(shaderCode.data()), shaderCode.size() / 4);
	const Vk::ShaderModuleCreateInfo createInfo({}, code);
	const Vk::ResultPair result = _object.createShaderModule(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateShaderModule);

	return result.value;
}

Vk::SwapchainKHR GraphicsDevice::createSwapchain(const Vk::SwapchainCreateInfoKHR& createInfo) const
{
	const Vk::ResultPair result = _object.createSwapchainKHR(*_dispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateSwapchain);

	return result.value;
}

Vk::ResultPair<uint32_t> GraphicsDevice::getNextSwapchainImage(const Swapchain& swapchain,
	const Vk::Semaphore signalSemaphore) const
{
	const Vk::ResultPair result = _object.acquireNextImageKHR(*_dispatch, swapchain._object, maxTimeout,
		signalSemaphore, Vk::Fence());

	switch(result.result)
	{
		case Vk::Result::Success:
		case Vk::Result::SuboptimalKHR:
		case Vk::Result::ErrorOutOfDateKHR:
			break;

		default:
			RUNKO_VULKAN_ERROR(result.result, Error::VulkanAcquireNextImage);
			break;
	}

	return result;
}

std::vector<Vk::Image> GraphicsDevice::getSwapchainImages(const Vk::SwapchainKHR swapchain) const
{
	const Vk::ResultPair result = _object.getSwapchainImagesKHR(*_dispatch, swapchain);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanGetSwapchainImages);

	return result.value;
}

Vk::Result GraphicsDevice::present(const Swapchain& swapchain, const uint32_t swapchainImageIndex,
	const Vk::Semaphore waitSemaphore) const
{
	const Vk::Queue queue = _queues[static_cast<uint32_t>(QueueType::Graphics)];
	const Vk::PresentInfoKHR presentInfo(toSpan(waitSemaphore), toSpan(swapchain._object),
		toSpan(swapchainImageIndex));

	const Vk::Result result = queue.presentKHR(*_dispatch, presentInfo);

	switch(result)
	{
		case Vk::Result::Success:
		case Vk::Result::SuboptimalKHR:
		case Vk::Result::ErrorOutOfDateKHR:
			break;

		default:
			RUNKO_VULKAN_ERROR(result, Error::VulkanPresent);
			break;
	}

	return result;
}

void GraphicsDevice::resetCommandPool(const Vk::CommandPool commandPool) const
{
	const Vk::Result result = _object.resetCommandPool(*_dispatch, commandPool, Vk::CommandPoolResetFlags());

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanResetCommandPool);
}

void GraphicsDevice::resetDescriptorPool(const Vk::DescriptorPool descriptorPool) const
{
	const Vk::Result result = _object.resetDescriptorPool(*_dispatch, descriptorPool,
		Vk::DescriptorPoolResetFlags());

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanResetDescriptorPool);
}

template<>
void GraphicsDevice::setObjectName(const CommandBuffer& commandBuffer, const std::string_view& name) const
{
	setObjectName(commandBuffer._object, name);
}

template<>
void GraphicsDevice::setObjectName(const Swapchain& swapchain, const std::string_view& name) const
{
	setObjectName(swapchain._object, name);
}

void GraphicsDevice::submit(const QueueType queueType, const CommandBuffer& commandBuffer,
	const SemaphoreSpan& waitSemaphores, const Uint64Span& waitSemaphoreValues,
	const SemaphoreSpan& signalSemaphores, const Uint64Span& signalSemaphoreValues) const
{
	const Vk::Queue queue = _queues[static_cast<uint32_t>(queueType)];
	const Vk::PipelineStageFlags waitStage = Vk::PipelineStageFlagBits::ColorAttachmentOutput;
	Vk::SubmitInfo submitInfo(waitSemaphores, toSpan(waitStage), toSpan(commandBuffer._object),
		signalSemaphores);

	const Vk::TimelineSemaphoreSubmitInfo timelineSemaphoreInfo(waitSemaphoreValues, signalSemaphoreValues);
	submitInfo.next = &timelineSemaphoreInfo;

	const Vk::Result result = queue.submit(*_dispatch, toSpan(submitInfo), Vk::Fence());

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanSubmit);
}

void GraphicsDevice::waitForIdle() const
{
	const Vk::Result result = _object.waitIdle(*_dispatch);

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanWaitIdle);
}

Vk::Result GraphicsDevice::waitForSemaphore(const Vk::Semaphore semaphore, const uint64_t value,
	const uint64_t timeout) const
{
	const Vk::SemaphoreWaitInfo waitInfo({}, toSpan(semaphore), toSpan(value));
	const Vk::Result result = _object.waitSemaphores(*_dispatch, waitInfo, timeout);

	if(result != Vk::Result::Success && result != Vk::Result::Timeout)
		RUNKO_VULKAN_ERROR(result, Error::VulkanWaitSemaphores);

	return result;
}
