/**
 * @file graphics/GraphicsContext.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/GraphicsContext.h>

#include <core/Config.h>

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	#include <core/Assert.h>
	#include <core/StaticString.h>

#endif

using namespace Graphics;

// Internal

#if defined(RUNKO_CONFIG_DEVELOPMENT)

using SemaphoreName = Core::StaticString<31>;

static constexpr std::array semaphoreNameFormats
{
	"Context {} Swapchain Image",
	"Context {} Submit"
};

#endif


// Private

GraphicsContext::GraphicsContext(const GraphicsDevice& device, const Vk::Semaphore timelineSemaphore,
	const uint32_t debugIndex) :
	_device(&device),
	_timelineSemaphore(timelineSemaphore)
{
	static_cast<void>(debugIndex);

	_commandPool = device.createCommandPool(QueueType::Graphics, false);
	_commandBuffer = device.allocateCommandBuffer(_commandPool);

	for(Vk::Semaphore& semaphore : _semaphores)
		semaphore = _device->createSemaphore();

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	RUNKO_ASSERT(debugIndex < 1000);
	const Core::StaticString name = Core::StaticString<15>::format("Context {}", debugIndex);
	device.setObjectName(_commandPool, name);
	device.setObjectName(_commandBuffer, name);

	for(size_t i = 0; i < _semaphores.size(); ++i)
	{
		const SemaphoreName semaphoreName = SemaphoreName::format(semaphoreNameFormats[i], debugIndex);
		device.setObjectName(_semaphores[i], semaphoreName);
	}

#endif
}

void GraphicsContext::reset()
{
	for(const Vk::Semaphore semaphore : _semaphores)
		_device->destroyObject(semaphore);

	_semaphores = {};
	_commandBuffer = {};
	_device->destroyObject(_commandPool);
	_commandPool = {};
	_timelineSemaphoreWaitValue = 0;
	_timelineSemaphore = {};
	_device = nullptr;
}
