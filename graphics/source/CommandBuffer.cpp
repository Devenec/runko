/**
 * @file graphics/CommandBuffer.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/CommandBuffer.h>

#include <core/Assert.h>
#include <graphics/Common.h>
#include <graphics/Error.h>
#include <graphics/vulkan/VulkanGraphics.h>

using namespace Core;
using namespace Graphics;

// Internal

static Vk::AccessFlags getAccessFlagsForImageLayout(const Vk::ImageLayout layout);
static Vk::PipelineStageFlags getPipelineStageFlagsForImageLayout(const Vk::ImageLayout layout);


// Public

void CommandBuffer::begin() const
{
	const Vk::CommandBufferBeginInfo beginInfo(Vk::CommandBufferUsageFlagBits::OneTimeSubmit);
	const Vk::Result result = _object.begin(*_dispatch, beginInfo);

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanBeginCommandBuffer);
}

void CommandBuffer::beginRenderPass(const Vk::RenderPass renderPass, const Framebuffer& framebuffer,
	const gsl::span<const Vk::ClearValue>& clearValues) const
{
	const Vk::Rect2D renderArea(Vk::Offset2D(), framebuffer.extent);
	const Vk::RenderPassBeginInfo beginInfo(renderPass, framebuffer.object, renderArea, clearValues);
	_object.beginRenderPass(*_dispatch, beginInfo, Vk::SubpassContents::Inline);
}

void CommandBuffer::end() const
{
	const Vk::Result result = _object.end(*_dispatch);

	if(result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result, Error::VulkanEndCommandBuffer);
}

void CommandBuffer::imageMemoryBarrier(const Vk::Image image, const Vk::ImageLayout oldLayout,
	const Vk::ImageLayout newLayout) const
{
	const Vk::PipelineStageFlags sourceStage = getPipelineStageFlagsForImageLayout(oldLayout);
	const Vk::PipelineStageFlags destinationStage = getPipelineStageFlagsForImageLayout(newLayout);
	imageMemoryBarrier(image, oldLayout, newLayout, sourceStage, destinationStage);
}

void CommandBuffer::imageMemoryBarrier(const Vk::Image image, const Vk::ImageLayout oldLayout,
	const Vk::ImageLayout newLayout, const Vk::PipelineStageFlags sourceStages,
	const Vk::PipelineStageFlags destinationStages) const
{
	const Vk::AccessFlags sourceAccess = getAccessFlagsForImageLayout(oldLayout);
	const Vk::AccessFlags destinationAccess = getAccessFlagsForImageLayout(newLayout);
	const Vk::ImageSubresourceRange subresourceRange(Vk::ImageAspectFlagBits::Color, 0, 1, 0, 1);
	const Vk::ImageMemoryBarrier memoryBarrier(sourceAccess, destinationAccess, oldLayout, newLayout, 0, 0,
		image, subresourceRange);

	_object.pipelineBarrier(*_dispatch, sourceStages, destinationStages, Vk::DependencyFlags(), {}, {},
		toSpan(memoryBarrier));
}


// Internal

static Vk::AccessFlags getAccessFlagsForImageLayout(const Vk::ImageLayout layout)
{
	switch(layout)
	{
		case Vk::ImageLayout::Undefined:
		case Vk::ImageLayout::PresentSrcKHR:
			return {};

		case Vk::ImageLayout::ShaderReadOnlyOptimal:
			return Vk::AccessFlagBits::ShaderRead;

		case Vk::ImageLayout::TransferDstOptimal:
			return Vk::AccessFlagBits::TransferWrite;

		case Vk::ImageLayout::TransferSrcOptimal:
			return Vk::AccessFlagBits::TransferRead;

		default:
			RUNKO_ASSERT(false);
			return {};
	}
}

static Vk::PipelineStageFlags getPipelineStageFlagsForImageLayout(const Vk::ImageLayout layout)
{
	switch(layout)
	{
		case Vk::ImageLayout::Undefined:
			return Vk::PipelineStageFlagBits::TopOfPipe;

		case Vk::ImageLayout::PresentSrcKHR:
			return Vk::PipelineStageFlagBits::BottomOfPipe;

		case Vk::ImageLayout::TransferDstOptimal:
		case Vk::ImageLayout::TransferSrcOptimal:
			return Vk::PipelineStageFlagBits::Transfer;

		default:
			RUNKO_ASSERT(false);
			return {};
	}
}
