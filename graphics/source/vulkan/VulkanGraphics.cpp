/**
 * @file graphics/vulkan/VulkanGraphics.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#define RUNKO_VULKAN_PLATFORM

#include <graphics/vulkan/VulkanGraphics.h>

#include <algorithm>
#include <array>
#include <cstdlib>
#include <cstring>

#include <core/Logger.h>
#include <core/Utility.h>
#include <core/Version.h>
#include <graphics/Error.h>
#include <graphics/vulkan/VulkanDebugger.h>
#include <graphics/vulkan/VulkanMemoryAllocator.h>
#include <gsl/span>

using namespace Core;
using namespace Graphics;

// Internal

static constexpr uint32_t apiVersion = Vk::apiVersion_1_2;

static constexpr std::array deviceExtensions
{
	Extension("VK_KHR_swapchain", Features::Swapchain)
};

static constexpr std::array instanceExtensions
{
	Extension("VK_KHR_surface",     Features::Swapchain),
	Extension("VK_EXT_debug_utils", Features::Debug)
};

static constexpr std::array instanceLayers
{
	Extension("VK_LAYER_KHRONOS_validation", Features::Debug)
};

static void enumerateExtensions(std::vector<Vk::ExtensionProperties>& extensions, const char* category);
static const char* getResultName(const Vk::Result result);


// Public

VulkanGraphics::VulkanGraphics(const Bool enableSwapchain) :
	_instanceDispatch(std::make_unique<Vk::InstanceDispatch>())
{
	if(enableSwapchain)
		_features |= Features::Swapchain;

	enumerateVersion();
	enumerateLayers();
	enumerateInstanceExtensions();
	createInstance();
	_instanceDispatch->initialise(_instance);
	vulkanDebugger.initialise(_instance, *_instanceDispatch);
	enumerateDevices();
}

VulkanGraphics::~VulkanGraphics()
{
	vulkanDebugger.deinitialise(_instance, *_instanceDispatch);
	_instance.destroy(*_instanceDispatch, {});
}

Bool VulkanGraphics::checkDeviceSurfaceSupport(const size_t deviceIndex, const Vk::SurfaceKHR surface) const
{
	const PhysicalDevice& device = _physicalDevices[deviceIndex];
	const Vk::ResultPair result = device.object.getSurfaceSupportKHR(*_instanceDispatch,
		device.queueFamilyIndexGraphics, surface);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanGetSurfaceSupport);

	return static_cast<Bool>(result.value);
}

DevicePair VulkanGraphics::createDevice(const PhysicalDevice& device, const std::string_view& deviceName)
	const
{
	enumerateDeviceExtensions(device.object);
	const std::array queuePriorities{ 1.0f };

	const std::array queueCreateInfos
	{
		Vk::DeviceQueueCreateInfo({}, device.queueFamilyIndexGraphics, queuePriorities),
		Vk::DeviceQueueCreateInfo({}, device.queueFamilyIndexTransfer, queuePriorities)
	};

	const gsl::span queueCreateInfoSpan(queueCreateInfos.data(),
		device.queueFamilyIndexGraphics == device.queueFamilyIndexTransfer ? 1 : 2);

	const ExtensionVector extensions = parseExtensions(deviceExtensions);
	const Vk::DeviceCreateInfo createInfo({}, queueCreateInfoSpan, {}, extensions);
	const Vk::ResultPair result = device.object.createDevice(*_instanceDispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateDevice);

	std::unique_ptr deviceDispatch = std::make_unique<Vk::DeviceDispatch>();
	deviceDispatch->initialise(_instance, *_instanceDispatch, result.value);
	vulkanDebugger.setObjectName(result.value, *deviceDispatch, result.value, deviceName);

	return std::make_pair(result.value, std::move(deviceDispatch));
}

VmaAllocator VulkanGraphics::createMemoryAllocator(const size_t deviceIndex, const Vk::Device device,
	const Vk::DeviceDispatch& deviceDispatch) const
{
	VmaVulkanFunctions functions{};
	functions.vkAllocateMemory = deviceDispatch.allocateMemory;
	functions.vkBindBufferMemory = deviceDispatch.bindBufferMemory;
	functions.vkBindBufferMemory2KHR = deviceDispatch.bindBufferMemory2;
	functions.vkBindImageMemory = deviceDispatch.bindImageMemory;
	functions.vkBindImageMemory2KHR = deviceDispatch.bindImageMemory2;
	functions.vkCmdCopyBuffer = deviceDispatch.cmdCopyBuffer;
	functions.vkCreateBuffer = deviceDispatch.createBuffer;
	functions.vkCreateImage = deviceDispatch.createImage;
	functions.vkDestroyBuffer = deviceDispatch.destroyBuffer;
	functions.vkDestroyImage = deviceDispatch.destroyImage;
	functions.vkFlushMappedMemoryRanges = deviceDispatch.flushMappedMemoryRanges;
	functions.vkFreeMemory = deviceDispatch.freeMemory;
	functions.vkGetBufferMemoryRequirements = deviceDispatch.getBufferMemoryRequirements;
	functions.vkGetBufferMemoryRequirements2KHR = deviceDispatch.getBufferMemoryRequirements2;
	functions.vkGetImageMemoryRequirements = deviceDispatch.getImageMemoryRequirements;
	functions.vkGetImageMemoryRequirements2KHR = deviceDispatch.getImageMemoryRequirements2;
	functions.vkGetPhysicalDeviceProperties = _instanceDispatch->getPhysicalDeviceProperties;
	functions.vkGetPhysicalDeviceMemoryProperties = _instanceDispatch->getPhysicalDeviceMemoryProperties;
	functions.vkGetPhysicalDeviceMemoryProperties2KHR = _instanceDispatch->getPhysicalDeviceMemoryProperties2;
	functions.vkInvalidateMappedMemoryRanges = deviceDispatch.invalidateMappedMemoryRanges;
	functions.vkMapMemory = deviceDispatch.mapMemory;
	functions.vkUnmapMemory = deviceDispatch.unmapMemory;

	VmaAllocatorCreateInfo createInfo{};
	createInfo.device = device.handle();
	createInfo.instance = _instance.handle();
	createInfo.physicalDevice = _physicalDevices[deviceIndex].object.handle();
	createInfo.pVulkanFunctions = &functions;
	createInfo.vulkanApiVersion = Vk::apiVersion_1_1;

	VmaAllocator allocator = nullptr;
	const VkResult result = vmaCreateAllocator(&createInfo, &allocator);

	if(result != VK_SUCCESS)
		RUNKO_VULKAN_ERROR(static_cast<Vk::Result>(result), Error::VulkanCreateMemoryAllocator);

	return allocator;
}

void VulkanGraphics::destroyMemoryAllocator(const VmaAllocator allocator) const
{
	vmaDestroyAllocator(allocator);
}

Vk::SurfaceCapabilitiesKHR VulkanGraphics::getSurfaceCapabilities(const size_t deviceIndex,
	const Vk::SurfaceKHR surface) const
{
	const Vk::ResultPair result = _physicalDevices[deviceIndex].object.getSurfaceCapabilitiesKHR(
		*_instanceDispatch, surface);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanGetSurfaceCapabilities);

	return result.value;
}

std::vector<Vk::SurfaceFormatKHR> VulkanGraphics::getSurfaceFormats(const size_t deviceIndex,
	const Vk::SurfaceKHR surface) const
{
	Vk::ResultPair result = _physicalDevices[deviceIndex].object.getSurfaceFormatsKHR(*_instanceDispatch,
		surface);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanGetSurfaceFormats);

	return std::move(result.value);
}

// Static

#if defined(RUNKO_CONFIG_DEVELOPMENT)

void VulkanGraphics::invokeError(const Vk::Result result, const uint32_t errorCode, const char* filename,
	const uint32_t line)
{
	const char* resultName = getResultName(result);
	logger.writeFlushAndWait(LogLevel::Error, "Error occurred: {} (Vulkan result '{}' {}) @ {}.",
		ErrorCode(errorCode), resultName, static_cast<int32_t>(result), SourceLocation(filename, line));

	RUNKO_BREAK_DEBUGGER();
	std::abort();
}

#else

void VulkanGraphics::invokeError(const Vk::Result result, const uint32_t errorCode)
{
	const char* resultName = getResultName(result);
	logger.writeFlushAndWait(LogLevel::Error, "Error occurred: {} (Vulkan result '{}' {}).",
		ErrorCode(errorCode), resultName, static_cast<int32_t>(result));

	std::abort();
}

#endif

// Private

void VulkanGraphics::enumerateVersion() const
{
	Vk::ResultPair result = Vk::Instance::enumerateVersion(*_instanceDispatch);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanEnumerateVersion);

	logger.write(LogLevel::Info, "Vulkan version {}.{}.{}", Vk::versionMajor(result.value),
		Vk::versionMinor(result.value), Vk::versionPatch(result.value));
}

void VulkanGraphics::enumerateLayers() const
{
	Vk::ResultPair result = Vk::Instance::enumerateLayerProperties(*_instanceDispatch);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanEnumerateLayers);

	auto compareFunction = [](const Vk::LayerProperties& propertiesA, const Vk::LayerProperties& propertiesB)
	{
		return std::strcmp(propertiesA.layerName, propertiesB.layerName) < 0;
	};

	std::sort(result.value.begin(), result.value.end(), compareFunction);
	logger.write(LogLevel::Info, "Vulkan layers:");

	for(const Vk::LayerProperties& properties : result.value)
		logger.write(LogLevel::Info, "  {}", properties.layerName);
}

void VulkanGraphics::enumerateInstanceExtensions() const
{
	Vk::ResultPair result = Vk::Instance::enumerateExtensionProperties(*_instanceDispatch, nullptr);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanEnumerateInstanceExtensions);

	enumerateExtensions(result.value, "instance");
}

void VulkanGraphics::createInstance()
{
	const Vk::ApplicationInfo applicationInfo("Runko Application", 0, "Runko", Version::number, apiVersion);
	const ExtensionVector layers = parseExtensions(instanceLayers);
	const ExtensionVector extensions = parseInstanceExtensions();
	const Vk::InstanceCreateInfo createInfo({}, applicationInfo, layers, extensions);
	const Vk::ResultPair result = Vk::Instance::create(*_instanceDispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateInstance);

	_instance = result.value;
}

void VulkanGraphics::enumerateDevices()
{
	const Vk::ResultPair result = _instance.enumeratePhysicalDevices(*_instanceDispatch);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanEnumerateDevices);

	for(const Vk::PhysicalDevice device : result.value)
	{
		PhysicalDevice physicalDevice(device);
		queryDeviceQueueFamilies(physicalDevice);
		_physicalDevices.push_back(physicalDevice);
	}
}

void VulkanGraphics::enumerateDeviceExtensions(const Vk::PhysicalDevice device) const
{
	Vk::ResultPair result = device.enumerateDeviceExtensionProperties(*_instanceDispatch, nullptr);

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanEnumerateDeviceExtensions);

	enumerateExtensions(result.value, "device");
}

VulkanGraphics::ExtensionVector VulkanGraphics::parseExtensions(const gsl::span<const Extension>& extensions)
	const
{
	ExtensionVector extensionNames;

	for(const Extension& extension : extensions)
	{
		if(in(_features, extension.features))
			extensionNames.push_back(extension.name);
	}

	return extensionNames;
}

VulkanGraphics::ExtensionVector VulkanGraphics::parseInstanceExtensions() const
{
	ExtensionVector extensionNames = parseExtensions(instanceExtensions);
	const ExtensionVector platformExtensionNames = parseExtensions(getInstanceExtensionsPlatform());
	extensionNames.insert(extensionNames.end(), platformExtensionNames.begin(), platformExtensionNames.end());

	return extensionNames;
}

void VulkanGraphics::queryDeviceQueueFamilies(PhysicalDevice& device) const
{
	const std::vector propertiesVector = device.object.getQueueFamilyProperties(*_instanceDispatch);

	for(uint32_t i = 0; i < propertiesVector.size(); ++i)
	{
		const Vk::QueueFamilyProperties& properties = propertiesVector[i];

		if(properties.queueFlags.in(Vk::QueueFlagBits::Graphics))
		{
			if(device.queueFamilyIndexGraphics == PhysicalDevice::invalidQueueFamilyIndex)
				device.queueFamilyIndexGraphics = i;

			if(device.queueFamilyIndexTransfer == PhysicalDevice::invalidQueueFamilyIndex)
				device.queueFamilyIndexTransfer = i;
		}

		if(properties.queueFlags == Vk::QueueFlagBits::Transfer)
			device.queueFamilyIndexTransfer = i;
	}
}


// Internal

static void enumerateExtensions(std::vector<Vk::ExtensionProperties>& extensions, const char* category)
{
	auto compareFunction = [](const Vk::ExtensionProperties& propertiesA,
		const Vk::ExtensionProperties& propertiesB)
	{
		return std::strcmp(propertiesA.extensionName, propertiesB.extensionName) < 0;
	};

	std::sort(extensions.begin(), extensions.end(), compareFunction);
	logger.write(LogLevel::Info, "Vulkan {} extensions:", category);

	for(const Vk::ExtensionProperties& properties : extensions)
		logger.write(LogLevel::Info, "  {}", properties.extensionName);
}

static const char* getResultName(const Vk::Result result)
{
	switch(result)
	{
		case Vk::Result::Success:
			return "Success";

		case Vk::Result::NotReady:
			return "NotReady";

		case Vk::Result::Timeout:
			return "Timeout";

		case Vk::Result::EventSet:
			return "EventSet";

		case Vk::Result::EventReset:
			return "EventReset";

		case Vk::Result::Incomplete:
			return "Incomplete";

		case Vk::Result::ErrorOutOfHostMemory:
			return "ErrorOutOfHostMemory";

		case Vk::Result::ErrorOutOfDeviceMemory:
			return "ErrorOutOfDeviceMemory";

		case Vk::Result::ErrorInitializationFailed:
			return "ErrorInitializationFailed";

		case Vk::Result::ErrorDeviceLost:
			return "ErrorDeviceLost";

		case Vk::Result::ErrorMemoryMapFailed:
			return "ErrorMemoryMapFailed";

		case Vk::Result::ErrorLayerNotPresent:
			return "ErrorLayerNotPresent";

		case Vk::Result::ErrorExtensionNotPresent:
			return "ErrorExtensionNotPresent";

		case Vk::Result::ErrorFeatureNotPresent:
			return "ErrorFeatureNotPresent";

		case Vk::Result::ErrorIncompatibleDriver:
			return "ErrorIncompatibleDriver";

		case Vk::Result::ErrorTooManyObjects:
			return "ErrorTooManyObjects";

		case Vk::Result::ErrorFormatNotSupported:
			return "ErrorFormatNotSupported";

		case Vk::Result::ErrorFragmentedPool:
			return "ErrorFragmentedPool";

		case Vk::Result::ErrorUnknown:
			return "ErrorUnknown";

		case Vk::Result::ErrorOutOfPoolMemory:
			return "ErrorOutOfPoolMemory";

		case Vk::Result::ErrorInvalidExternalHandle:
			return "ErrorInvalidExternalHandle";

		case Vk::Result::ErrorFragmentation:
			return "ErrorFragmentation";

		case Vk::Result::ErrorInvalidOpaqueCaptureAddress:
			return "ErrorInvalidOpaqueCaptureAddress";

		case Vk::Result::ErrorSurfaceLostKHR:
			return "ErrorSurfaceLostKHR";

		case Vk::Result::ErrorNativeWindowInUseKHR:
			return "ErrorNativeWindowInUseKHR";

		case Vk::Result::SuboptimalKHR:
			return "SuboptimalKHR";

		case Vk::Result::ErrorOutOfDateKHR:
			return "ErrorOutOfDateKHR";

		case Vk::Result::ErrorIncompatibleDisplayKHR:
			return "ErrorIncompatibleDisplayKHR";

		case Vk::Result::ErrorValidationFailedEXT:
			return "ErrorValidationFailedEXT";

		case Vk::Result::ErrorInvalidShaderNV:
			return "ErrorInvalidShaderNV";

		case Vk::Result::ErrorInvalidDrmFormatModifierPlaneLayoutEXT:
			return "ErrorInvalidDrmFormatModifierPlaneLayoutEXT";

		case Vk::Result::ErrorNotPermittedEXT:
			return "ErrorNotPermittedEXT";

		case Vk::Result::ErrorFullScreenExclusiveModeLostEXT:
			return "ErrorFullScreenExclusiveModeLostEXT";

		case Vk::Result::ThreadIdleKHR:
			return "ThreadIdleKHR";

		case Vk::Result::ThreadDoneKHR:
			return "ThreadDoneKHR";

		case Vk::Result::OperationDeferredKHR:
			return "OperationDeferredKHR";

		case Vk::Result::OperationNotDeferredKHR:
			return "OperationNotDeferredKHR";

		case Vk::Result::PipelineCompileRequiredEXT:
			return "PipelineCompileRequiredEXT";
	}

	return "Unknown";
}
