/**
 * @file graphics/vulkan/VulkanDebugger.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/vulkan/VulkanDebugger.h>

#include <array>
#include <utility>

#include <core/Logger.h>
#include <core/StaticString.h>
#include <graphics/Error.h>
#include <graphics/vulkan/VulkanGraphics.h>

using namespace Core;
using namespace Graphics;

// Internal

#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

using MessageTypeString = StaticString<34>;

static constexpr Vk::DebugUtilsMessageSeverityFlagsEXT messageSeverityFlags =
	Vk::DebugUtilsMessageSeverityFlagBitsEXT::Verbose |
	Vk::DebugUtilsMessageSeverityFlagBitsEXT::Info |
	Vk::DebugUtilsMessageSeverityFlagBitsEXT::Warning |
	Vk::DebugUtilsMessageSeverityFlagBitsEXT::Error;

static constexpr Vk::DebugUtilsMessageTypeFlagsEXT messageTypeFlags =
	//Vk::DebugUtilsMessageTypeFlagBitsEXT::General |
	Vk::DebugUtilsMessageTypeFlagBitsEXT::Validation |
	Vk::DebugUtilsMessageTypeFlagBitsEXT::Performance;

static constexpr std::array messageTypeNames
{
	std::pair(Vk::DebugUtilsMessageTypeFlagBitsEXT::General,     "General"),
	std::pair(Vk::DebugUtilsMessageTypeFlagBitsEXT::Validation,  "Validation"),
	std::pair(Vk::DebugUtilsMessageTypeFlagBitsEXT::Performance, "Performance")
};

static LogLevel getLogLevel(const Vk::DebugUtilsMessageSeverityFlagBitsEXT messageSeverity);
static MessageTypeString getMessageTypeString(const Vk::DebugUtilsMessageTypeFlagsEXT messageTypes);
static const char* getObjectTypeName(const Vk::ObjectType type);

static Vk::Bool32 VKAPI_PTR onDebugMessage(const Vk::DebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	const Vk::DebugUtilsMessageTypeFlagsEXT messageTypes,
	const Vk::DebugUtilsMessengerCallbackDataEXT* messageData, void* userData);

#endif


// Graphics

VulkanDebugger Graphics::vulkanDebugger;


// Public

void VulkanDebugger::deinitialise(const Vk::Instance instance, const Vk::InstanceDispatch& instanceDispatch)
{
#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

	instance.destroyDebugUtilsMessengerEXT(instanceDispatch, _messenger, {});
	_messenger = {};

#else

	static_cast<void>(instance);
	static_cast<void>(instanceDispatch);

#endif
}

void VulkanDebugger::initialise(const Vk::Instance instance, const Vk::InstanceDispatch& instanceDispatch)
{
#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

	const Vk::DebugUtilsMessengerCreateInfoEXT createInfo(Vk::DebugUtilsMessengerCreateFlagsEXT(),
		messageSeverityFlags, messageTypeFlags, onDebugMessage);

	const Vk::ResultPair result = instance.createDebugUtilsMessengerEXT(instanceDispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateDebugMessenger);

	_messenger = result.value;

#else

	static_cast<void>(instance);
	static_cast<void>(instanceDispatch);

#endif
}


// Internal

#if defined(RUNKO_CONFIG_GRAPHICS_VALIDATE)

static LogLevel getLogLevel(const Vk::DebugUtilsMessageSeverityFlagBitsEXT messageSeverity)
{
	switch(messageSeverity)
	{
		case Vk::DebugUtilsMessageSeverityFlagBitsEXT::Verbose:
			return LogLevel::Debug;

		case Vk::DebugUtilsMessageSeverityFlagBitsEXT::Info:
			return LogLevel::Info;

		case Vk::DebugUtilsMessageSeverityFlagBitsEXT::Warning:
			return LogLevel::Warning;

		case Vk::DebugUtilsMessageSeverityFlagBitsEXT::Error:
			return LogLevel::Error;
	}

	return LogLevel::Debug;
}

static MessageTypeString getMessageTypeString(const Vk::DebugUtilsMessageTypeFlagsEXT messageTypes)
{
	MessageTypeString string;

	for(const auto& typeName : messageTypeNames)
	{
		if(messageTypes.in(typeName.first))
		{
			if(!string.isEmpty())
				string.append('|');

			string.append(typeName.second);
		}
	}

	return string;
}

static const char* getObjectTypeName(const Vk::ObjectType type)
{
	switch(type)
	{
		case Vk::ObjectType::Unknown:
			return "Unknown";

		case Vk::ObjectType::Instance:
			return "Instance";

		case Vk::ObjectType::PhysicalDevice:
			return "PhysicalDevice";

		case Vk::ObjectType::Device:
			return "Device";

		case Vk::ObjectType::Queue:
			return "Queue";

		case Vk::ObjectType::Semaphore:
			return "Semaphore";

		case Vk::ObjectType::CommandBuffer:
			return "CommandBuffer";

		case Vk::ObjectType::Fence:
			return "Fence";

		case Vk::ObjectType::DeviceMemory:
			return "DeviceMemory";

		case Vk::ObjectType::Buffer:
			return "Buffer";

		case Vk::ObjectType::Image:
			return "Image";

		case Vk::ObjectType::Event:
			return "Event";

		case Vk::ObjectType::QueryPool:
			return "QueryPool";

		case Vk::ObjectType::BufferView:
			return "BufferView";

		case Vk::ObjectType::ImageView:
			return "ImageView";

		case Vk::ObjectType::ShaderModule:
			return "ShaderModule";

		case Vk::ObjectType::PipelineCache:
			return "PipelineCache";

		case Vk::ObjectType::PipelineLayout:
			return "PipelineLayout";

		case Vk::ObjectType::RenderPass:
			return "RenderPass";

		case Vk::ObjectType::Pipeline:
			return "Pipeline";

		case Vk::ObjectType::DescriptorSetLayout:
			return "DescriptorSetLayout";

		case Vk::ObjectType::Sampler:
			return "Sampler";

		case Vk::ObjectType::DescriptorPool:
			return "DescriptorPool";

		case Vk::ObjectType::DescriptorSet:
			return "DescriptorSet";

		case Vk::ObjectType::Framebuffer:
			return "Framebuffer";

		case Vk::ObjectType::CommandPool:
			return "CommandPool";

		case Vk::ObjectType::SamplerYcbcrConversion:
			return "SamplerYcbcrConversion";

		case Vk::ObjectType::DescriptorUpdateTemplate:
			return "DescriptorUpdateTemplate";

		case Vk::ObjectType::SurfaceKHR:
			return "SurfaceKHR";

		case Vk::ObjectType::SwapchainKHR:
			return "SwapchainKHR";

		case Vk::ObjectType::DisplayKHR:
			return "DisplayKHR";

		case Vk::ObjectType::DisplayModeKHR:
			return "DisplayModeKHR";

		case Vk::ObjectType::DebugReportCallbackEXT:
			return "DebugReportCallbackEXT";

		case Vk::ObjectType::DebugUtilsMessengerEXT:
			return "DebugUtilsMessengerEXT";

		case Vk::ObjectType::AccelerationStructureKHR:
			return "AccelerationStructureKHR";

		case Vk::ObjectType::ValidationCacheEXT:
			return "ValidationCacheEXT";

		case Vk::ObjectType::AccelerationStructureNV:
			return "AccelerationStructureNV";

		case Vk::ObjectType::PerformanceConfigurationINTEL:
			return "PerformanceConfigurationINTEL";

		case Vk::ObjectType::DeferredOperationKHR:
			return "DeferredOperationKHR";

		case Vk::ObjectType::IndirectCommandsLayoutNV:
			return "IndirectCommandsLayoutNV";

		case Vk::ObjectType::PrivateDataSlotEXT:
			return "PrivateDataSlotEXT";
	}

	return "Unknown";
}

static Vk::Bool32 VKAPI_PTR onDebugMessage(const Vk::DebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	const Vk::DebugUtilsMessageTypeFlagsEXT messageTypes,
	const Vk::DebugUtilsMessengerCallbackDataEXT* messageData, void* userData)
{
	static_cast<void>(userData);

	const LogLevel logLevel = getLogLevel(messageSeverity);
	const MessageTypeString messageTypeString = getMessageTypeString(messageTypes);
	const char* messageIdName = messageData->messageIdName == nullptr ? "Unknown" :
		messageData->messageIdName;

	logger.write(logLevel, "Vulkan debug message [{}] {} ({}):", messageTypeString, messageIdName,
		messageData->messageIdNumber);

	for(uint32_t i = 0; i < messageData->objectCount; ++i)
	{
		const Vk::DebugUtilsObjectNameInfoEXT& nameInfo = messageData->objects[i];
		const char* objectTypeName = getObjectTypeName(nameInfo.objectType);
		const char* objectName = nameInfo.objectName == nullptr ? "Unknown" : nameInfo.objectName;
		logger.write(logLevel, "  {}: {} '{}' ({})", i, objectTypeName, objectName,
			Address(nameInfo.objectHandle));
	}

	logger.write(logLevel, "  {}\n", messageData->message);
	return false;
}

#endif
