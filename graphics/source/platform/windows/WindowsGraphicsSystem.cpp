/**
 * @file graphics/platform/windows/WindowsGraphicsSystem.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/GraphicsSystem.h>

#include <core/Utility.h>
#include <core/platform/windows/Windows.h>
#include <graphics/Error.h>
#include <graphics/platform/SurfaceInfo.h>

using namespace Core;
using namespace Graphics;
using namespace Platform;

// Internal

static void addDisplay(const Display& display, const DISPLAY_DEVICEW& adapterInfo,
	DisplayVector& displayVector);

static DISPLAY_DEVICEW createAdapterInfo();
static Display createDisplay(const DISPLAY_DEVICEW& adapterInfo);
static Bool enumerateAdapter(const uint32_t index, DISPLAY_DEVICEW& adapterInfo);
static DisplayVector enumerateDisplays();


// Private

void GraphicsSystem::enumerateAdapters()
{
	const DisplayVector displays = enumerateDisplays();
	std::vector<DisplayVector> displayVectors(_graphics.physicalDeviceCount());

	for(const Display& display : displays)
	{
		for(size_t i = 0; i < displayVectors.size(); ++i)
		{
			const Bool isDeviceDisplay = checkDeviceSurfaceSupport(i, display);

			if(!isDeviceDisplay)
				continue;

			displayVectors[i].push_back(display);
			break;
		}
	}

	createAdapterVector(displayVectors);
}

Vk::SurfaceKHR GraphicsSystem::createSurface(const WindowHandle windowHandle) const
{
	const HWND nativeWindowHandle = _windowManager.getNativeWindowHandle(windowHandle);
	return _graphics.createSurface(SurfaceInfo(nativeWindowHandle));
}

Bool GraphicsSystem::checkDeviceSurfaceSupport(const size_t deviceIndex, const Display& display)
{
	const HWND windowHandle = _windowManager.createWindowForSurfaceSupportCheck(display);
	const Vk::SurfaceKHR surface = _graphics.createSurface(SurfaceInfo(windowHandle));
	const Bool result = _graphics.checkDeviceSurfaceSupport(deviceIndex, surface);
	_graphics.destroySurface(surface);
	_windowManager.destroyWindow(windowHandle);

	return result;
}


// Internal

static void addDisplay(const Display& display, const DISPLAY_DEVICEW& adapterInfo,
	DisplayVector& displayVector)
{
	const Bool isPrimary = in(adapterInfo.StateFlags, static_cast<uint32_t>(DISPLAY_DEVICE_PRIMARY_DEVICE));
	DisplayVector::iterator iterator = isPrimary ? displayVector.begin() : displayVector.end();
	displayVector.insert(iterator, display);
}

static DISPLAY_DEVICEW createAdapterInfo()
{
	DISPLAY_DEVICEW adapterInfo{};
	adapterInfo.cb = sizeof(DISPLAY_DEVICEW);

	return adapterInfo;
}

static Display createDisplay(const DISPLAY_DEVICEW& adapterInfo)
{
	DEVMODEW displayMode{};
	displayMode.dmSize = sizeof(DEVMODEW);

	const int32_t result = EnumDisplaySettingsExW(adapterInfo.DeviceName, ENUM_CURRENT_SETTINGS, &displayMode,
		0);

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsGraphicsSystemGetDisplayMode);

	const Core::Rectangle rectangle(displayMode.dmPosition.x, displayMode.dmPosition.y,
		static_cast<int32_t>(displayMode.dmPelsWidth), static_cast<int32_t>(displayMode.dmPelsHeight));

	return Display(rectangle, displayMode.dmDisplayFrequency);
}

static Bool enumerateAdapter(const uint32_t index, DISPLAY_DEVICEW& adapterInfo)
{
	const int32_t result = EnumDisplayDevicesW(nullptr, index, &adapterInfo, 0);
	return result != 0;
}

static DisplayVector enumerateDisplays()
{
	DISPLAY_DEVICEW adapterInfo = createAdapterInfo();
	DisplayVector displays;

	for(uint32_t i = 0; ; ++i)
	{
		const Bool result = enumerateAdapter(i, adapterInfo);

		if(!result)
			break;

		if(!in(adapterInfo.StateFlags, static_cast<uint32_t>(DISPLAY_DEVICE_ACTIVE)))
			continue;

		const Display display = createDisplay(adapterInfo);
		addDisplay(display, adapterInfo, displays);
	}

	return displays;
}
