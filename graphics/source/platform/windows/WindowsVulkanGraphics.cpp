/**
 * @file graphics/platform/windows/WindowsVulkanGraphics.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#define RUNKO_VULKAN_PLATFORM

#include <graphics/vulkan/VulkanGraphics.h>

#include <array>

#include <core/platform/windows/Windows.h>
#include <graphics/Error.h>
#include <graphics/platform/SurfaceInfo.h>

using namespace Graphics;
using namespace Platform;

// Internal

static constexpr std::array instanceExtensions
{
	Extension("VK_KHR_win32_surface", Features::Swapchain)
};


// Public

Vk::SurfaceKHR VulkanGraphics::createSurface(const SurfaceInfo& surfaceInfo) const
{
	const Vk::Win32SurfaceCreateInfoKHR createInfo(Vk::Win32SurfaceCreateFlagsKHR(),
		GetModuleHandleW(nullptr), surfaceInfo.windowHandle);

	const Vk::ResultPair result = _instance.createWin32SurfaceKHR(*_instanceDispatch, createInfo, {});

	if(result.result != Vk::Result::Success)
		RUNKO_VULKAN_ERROR(result.result, Error::VulkanCreateSurface);

	return result.value;
}

// Private

// Static

gsl::span<const Extension> VulkanGraphics::getInstanceExtensionsPlatform()
{
	return instanceExtensions;
}
