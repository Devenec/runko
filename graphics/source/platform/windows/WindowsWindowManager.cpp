/**
 * @file graphics/platform/windows/WindowsWindowManager.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/platform/windows/WindowsWindowManager.h>

#include <algorithm>

#include <core/Assert.h>
#include <core/Rectangle.h>
#include <graphics/Error.h>
#include <graphics/GraphicsSystem.h>
#include <graphics/platform/WindowHandle.h>

#define OEMRESOURCE
#include <core/platform/windows/Windows.h>

using namespace Graphics;
using namespace Platform;

// Internal

static constexpr const wchar_t* windowClassName = L"RunkoWindowClass";
static constexpr uint32_t windowStyle           = WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU;
static constexpr const wchar_t* windowTitle     = L"Runko Application";

static HWND createWindow(const Display& display, const uint32_t width, const uint32_t height);
static Core::Rectangle createWindowRectangle(const Display& display, const uint32_t width,
	const uint32_t height);

static WindowsWindowManager& getWindowUserData(const HWND windowHandle);
static HCURSOR loadDefaultPointer();


// Public

WindowsWindowManager::WindowsWindowManager()
{
	WNDCLASSEXW windowClassInfo{};
	windowClassInfo.cbSize = sizeof(WNDCLASSEXW);
	windowClassInfo.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	windowClassInfo.hCursor = loadDefaultPointer();
	windowClassInfo.hInstance = GetModuleHandleW(nullptr);
	windowClassInfo.lpfnWndProc = onWindowMessage;
	windowClassInfo.lpszClassName = windowClassName;

	const uint32_t result = RegisterClassExW(&windowClassInfo);

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerRegisterWindowClass);
}

WindowsWindowManager::~WindowsWindowManager()
{
	const int32_t result = UnregisterClassW(windowClassName, GetModuleHandleW(nullptr));

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerUnregisterWindowClass);
}

Graphics::WindowHandle WindowsWindowManager::createWindow(const Display& display, const uint32_t width,
	const uint32_t height)
{
	const HWND handle = ::createWindow(display, width, height);
	setWindowUserData(handle);

	return BaseWindowManager::createWindow(WindowHandle(handle));
}

HWND WindowsWindowManager::createWindowForSurfaceSupportCheck(const Graphics::Display& display) const
{
	return ::createWindow(display, 100, 100);
}

void WindowsWindowManager::destroyWindow(const Graphics::WindowHandle windowHandle)
{
	const WindowHandle platformHandle = BaseWindowManager::destroyWindow(windowHandle);
	destroyWindow(platformHandle.nativeHandle);
}

void WindowsWindowManager::destroyWindow(const HWND windowHandle) const
{
	const int32_t result = DestroyWindow(windowHandle);

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerDestroyWindow);
}

Core::Rectangle WindowsWindowManager::getWindowRectangle(const Graphics::WindowHandle windowHandle) const
{
	RECT rectangle{};
	const int32_t result = GetClientRect(getNativeWindowHandle(windowHandle), &rectangle);

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerGetWindowRectangle);

	return Core::Rectangle(0, 0, rectangle.right, rectangle.bottom);
}

void WindowsWindowManager::processWindowMessages() const
{
	MSG message;

	while(PeekMessageW(&message, nullptr, 0, 0, PM_REMOVE) != 0)
	{
		DispatchMessageW(&message);

		if(message.message == WM_KEYDOWN)
			TranslateMessage(&message);
	}
}

void WindowsWindowManager::showWindow(const Graphics::WindowHandle windowHandle) const
{
	ShowWindow(getNativeWindowHandle(windowHandle), SW_SHOWNORMAL);
}

// Private

void WindowsWindowManager::setWindowUserData(const HWND windowHandle) const
{
	SetLastError(0);
	SetWindowLongPtrW(windowHandle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

	if(GetLastError() != 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerSetWindowUserData);
}

WindowsWindowManager::Window& WindowsWindowManager::getWindow(const HWND windowHandle)
{
	auto compareFunction = [windowHandle](const Window& window)
	{
		return window.handle.nativeHandle == windowHandle;
	};

	auto iterator = std::find_if(_windows.begin(), _windows.end(), compareFunction);
	RUNKO_ASSERT(iterator != _windows.end());

	return *iterator;
}

// Static

LRESULT CALLBACK WindowsWindowManager::onWindowMessage(const HWND windowHandle, const uint32_t message,
	const WPARAM wordParameter, const LPARAM longParameter)
{
	switch(message)
	{
		case WM_CLOSE:
			onWindowClose(windowHandle);
			return 0;

		case WM_CHAR:
		case WM_INPUT:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_MOUSEWHEEL:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
			if(onWindowInput(windowHandle, message, wordParameter, longParameter))
				return 0;

			break;

		case WM_KEYDOWN:
		case WM_KEYUP:
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
			return 0;

		case WM_MOUSEMOVE:
			if(onWindowMouseMove(windowHandle, longParameter))
				return 0;

			break;

		case WM_SIZE:
			onWindowSizeChanged(windowHandle, longParameter);
			return 0;

		default:
			break;
	}

	return DefWindowProcW(windowHandle, message, wordParameter, longParameter);
}

void WindowsWindowManager::onWindowClose(const HWND windowHandle)
{
	WindowsWindowManager& windowManager = getWindowUserData(windowHandle);
	Window& window = windowManager.getWindow(windowHandle);
	window.isOpen = false;
}

Bool WindowsWindowManager::onWindowInput(const HWND windowHandle, const uint32_t message,
	const WPARAM wordParameter, const LPARAM longParameter)
{
	WindowsWindowManager& windowManager = getWindowUserData(windowHandle);
	return windowManager._inputManager.processMessage(message, wordParameter, longParameter);
}

Bool WindowsWindowManager::onWindowMouseMove(const HWND windowHandle, const LPARAM longParameter)
{
	WindowsWindowManager& windowManager = getWindowUserData(windowHandle);
	const Window& window = windowManager.getWindow(windowHandle);

	return windowManager._inputManager.processMessage(WM_MOUSEMOVE, 0, longParameter, window.width,
		window.height);
}

void WindowsWindowManager::onWindowSizeChanged(const HWND windowHandle, const LPARAM longParameter)
{
	WindowsWindowManager& windowManager = getWindowUserData(windowHandle);
	Window& window = windowManager.getWindow(windowHandle);
	window.width = LOWORD(longParameter);
	window.height = HIWORD(longParameter);
}


// Internal

static HWND createWindow(const Display& display, const uint32_t width, const uint32_t height)
{
	const Core::Rectangle rectangle = createWindowRectangle(display, width, height);
	const HWND handle = CreateWindowExW(0, windowClassName, windowTitle, windowStyle, rectangle.x,
		rectangle.y, rectangle.width, rectangle.height, nullptr, nullptr, GetModuleHandleW(nullptr), nullptr);

	if(handle == nullptr)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerCreateWindow);

	return handle;
}

static Core::Rectangle createWindowRectangle(const Display& display, const uint32_t width,
	const uint32_t height)
{
	RECT rectangle;
	rectangle.left = display.rectangle.x + (display.rectangle.width - static_cast<int32_t>(width)) / 2;
	rectangle.top = display.rectangle.y + (display.rectangle.height - static_cast<int32_t>(height)) / 2;
	rectangle.right = rectangle.left + static_cast<int32_t>(width);
	rectangle.bottom = rectangle.top + static_cast<int32_t>(height);

	const int32_t result = AdjustWindowRectEx(&rectangle, windowStyle, FALSE, 0);

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerCreateWindowRectangle);

	return Core::Rectangle(rectangle.left, rectangle.top, rectangle.right - rectangle.left,
		rectangle.bottom - rectangle.top);
}

static WindowsWindowManager& getWindowUserData(const HWND windowHandle)
{
	const LONG_PTR userData = GetWindowLongPtrW(windowHandle, GWLP_USERDATA);

	if(userData == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsWindowManagerGetWindowUserData);

	return *reinterpret_cast<WindowsWindowManager*>(userData);
}

static HCURSOR loadDefaultPointer()
{
	const HANDLE pointerHandle = LoadImageW(nullptr, MAKEINTRESOURCEW(OCR_NORMAL), IMAGE_CURSOR, 0, 0,
		LR_SHARED);

	return static_cast<HCURSOR>(pointerHandle);
}
