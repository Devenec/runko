/**
 * @file graphics/platform/windows/WindowsWindowInputManager.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/platform/windows/WindowsWindowInputManager.h>

#include <limits>
#include <windowsx.h>

#include <core/Input.h>
#include <core/Utility.h>
#include <core/platform/windows/Windows.h>
#include <graphics/Error.h>
#include <graphics/platform/WindowInput.h>

using namespace Core;
using namespace Graphics;
using namespace Platform;

// Internal

static KeyCode getKeyCode(const RAWKEYBOARD& data);


// Public

Bool WindowsWindowInputManager::processMessage(const uint32_t message, const WPARAM wordParameter,
	const LPARAM longParameter, const uint32_t windowWidth, const uint32_t windowHeight) const
{
	static_cast<void>(wordParameter);

	switch(message)
	{
		case WM_CHAR:
			processCharacterMessage(wordParameter);
			return true;

		case WM_INPUT:
			processRawInputMessage(longParameter);
			return true;

		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			processMouseButtonDownMessage(message);
			return true;

		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
			processMouseButtonUpMessage(message);
			return true;

		case WM_MOUSEMOVE:
			processMouseMoveMessage(longParameter, windowWidth, windowHeight);
			return true;

		case WM_MOUSEWHEEL:
			processMouseWheelMessage(wordParameter);
			return true;

		default:
			return false;
	}
}

// Private

void WindowsWindowInputManager::initialiseRawInput() const
{
	// Keyboard
	RAWINPUTDEVICE device{};
	device.usUsagePage = 0x0001;
	device.usUsage = 0x0006;

	const int32_t result = RegisterRawInputDevices(&device, 1, sizeof(RAWINPUTDEVICE));

	if(result == 0)
		RUNKO_WINDOWS_ERROR(Error::WindowsInputManagerInitialiseRawInput);
}

void WindowsWindowInputManager::processCharacterMessage(const WPARAM wordParameter) const
{
	for(WindowInputEventHandler* eventHandler : _eventHandlers)
		eventHandler->onCharacter(static_cast<uint32_t>(wordParameter));
}

void WindowsWindowInputManager::processRawInputMessage(const LPARAM longParameter) const
{
	HRAWINPUT inputHandle = reinterpret_cast<HRAWINPUT>(longParameter);
	RAWINPUT input;
	uint32_t inputSize = sizeof(RAWINPUT);
	const uint32_t result = GetRawInputData(inputHandle, RID_INPUT, &input, &inputSize,
		sizeof(RAWINPUTHEADER));

	RUNKO_ASSERT(result != std::numeric_limits<uint32_t>::max());
	RUNKO_ASSERT(input.header.dwType == RIM_TYPEKEYBOARD);
	processRawInputKeyboardData(input.data.keyboard);
}

void WindowsWindowInputManager::processMouseButtonDownMessage(const uint32_t message) const
{
	WindowInputEventHandler::MouseButton button;

	switch(message)
	{
		case WM_LBUTTONDOWN:
			button = WindowInputEventHandler::MouseButton::Left;
			break;

		case WM_RBUTTONDOWN:
			button = WindowInputEventHandler::MouseButton::Right;
			break;

		default:
			return;
	}

	for(WindowInputEventHandler* eventHandler : _eventHandlers)
		eventHandler->onMouseButton(button, true);
}


void WindowsWindowInputManager::processMouseButtonUpMessage(const uint32_t message) const
{
	WindowInputEventHandler::MouseButton button;

	switch(message)
	{
		case WM_LBUTTONUP:
			button = WindowInputEventHandler::MouseButton::Left;
			break;

		case WM_RBUTTONUP:
			button = WindowInputEventHandler::MouseButton::Right;
			break;

		default:
			return;
	}

	for(WindowInputEventHandler* eventHandler : _eventHandlers)
		eventHandler->onMouseButton(button, false);
}

void WindowsWindowInputManager::processMouseMoveMessage(const LPARAM longParameter,
	const uint32_t windowWidth, const uint32_t windowHeight) const
{
	const uint32_t x = static_cast<uint32_t>(std::clamp(GET_X_LPARAM(longParameter), 0,
		static_cast<int32_t>(windowWidth)));

	const uint32_t y = static_cast<uint32_t>(std::clamp(GET_Y_LPARAM(longParameter), 0,
		static_cast<int32_t>(windowHeight)));

	for(WindowInputEventHandler* eventHandler : _eventHandlers)
		eventHandler->onMouseMove(x, y);
}

void WindowsWindowInputManager::processMouseWheelMessage(const WPARAM wordParameter) const
{
	const int32_t sign = GET_WHEEL_DELTA_WPARAM(wordParameter) / WHEEL_DELTA;

	for(WindowInputEventHandler* eventHandler : _eventHandlers)
		eventHandler->onMouseWheel(sign, WHEEL_DELTA);
}

void WindowsWindowInputManager::processRawInputKeyboardData(const RAWKEYBOARD& data) const
{
	const KeyCode keyCode = getKeyCode(data);

	if(keyCode == KeyCode::Unknown)
		return;

	const Bool pressed = !in(data.Flags, RI_KEY_BREAK);

	for(WindowInputEventHandler* eventHandler : _eventHandlers)
		eventHandler->onKey(keyCode, pressed);

}


// Internal

static KeyCode getKeyCode(const RAWKEYBOARD& data)
{
	uint32_t keyCode = data.VKey;
	const Bool e0Set = in(data.Flags, RI_KEY_E0);

	switch(keyCode)
	{
		case 0xE1:
		case 0xE2:
		case VK_ADD:
		case VK_APPS:
		case VK_BACK:
		case VK_CAPITAL:
		case VK_DECIMAL:
		case VK_DIVIDE:
		case VK_ESCAPE:
		case VK_LWIN:
		case VK_MULTIPLY:
		case VK_NUMLOCK:
		case VK_PAUSE:
		case VK_PRINT:
		case VK_RWIN:
		case VK_SCROLL:
		case VK_SNAPSHOT:
		case VK_SPACE:
		case VK_SUBTRACT:
		case VK_TAB:
			break;

		case VK_CLEAR:
			return e0Set ? KeyCode::Unknown : KeyCode::Numpad5;

		case VK_CONTROL:
			keyCode = MapVirtualKeyW(data.MakeCode, MAPVK_VSC_TO_VK_EX);

			if(keyCode == VK_MENU || keyCode == VK_LMENU || keyCode == VK_RMENU)
				return KeyCode::Unknown;

			return e0Set ? KeyCode::ControlRight : KeyCode::ControlLeft;

		case VK_DELETE:
			return e0Set ? KeyCode::Delete : KeyCode::NumpadDecimal;

		case VK_DOWN:
			return e0Set ? KeyCode::Down : KeyCode::Numpad2;

		case VK_END:
			return e0Set ? KeyCode::End : KeyCode::Numpad1;

		case VK_HOME:
			return e0Set ? KeyCode::Home : KeyCode::Numpad7;

		case VK_INSERT:
			return e0Set ? KeyCode::Insert : KeyCode::Numpad0;

		case VK_LEFT:
			return e0Set ? KeyCode::Left : KeyCode::Numpad4;

		case VK_NEXT:
			return e0Set ? KeyCode::PageDown : KeyCode::Numpad3;

		case VK_PRIOR:
			return e0Set ? KeyCode::PageUp : KeyCode::Numpad9;

		case VK_MENU:
			return e0Set ? KeyCode::AltRight : KeyCode::AltLeft;

		case VK_RETURN:
			return e0Set ? KeyCode::NumpadEnter : KeyCode::Enter;

		case VK_RIGHT:
			return e0Set ? KeyCode::Right : KeyCode::Numpad6;

		case VK_SHIFT:
			keyCode = MapVirtualKeyW(data.MakeCode, MAPVK_VSC_TO_VK_EX);
			break;

		case VK_UP:
			return e0Set ? KeyCode::Up : KeyCode::Numpad8;

		default:
			if((keyCode >= 0x30 && keyCode <= 0x39)  || // 0-9
				(keyCode >= 0x41 && keyCode <= 0x5A) || // A-Z
				(keyCode >= 0x60 && keyCode <= 0x69) || // NUMPAD0-9
				(keyCode >= 0x70 && keyCode <= 0x87) || // F1-F24
				(keyCode >= 0xBA && keyCode <= 0xC0) || // OEM
				(keyCode >= 0xDB && keyCode <= 0xDF))   // OEM
			{
				break;
			}

			return KeyCode::Unknown;
	}

	return static_cast<KeyCode>(keyCode);
}
