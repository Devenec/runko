/**
 * @file graphics/GUI.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/GUI.h>

#include <algorithm>
#include <array>
#include <utility>

#include <core/Assert.h>
#include <core/Utility.h>
#include <graphics/CommandBuffer.h>
#include <graphics/GraphicsDevice.h>
#include <graphics/GraphicsSystem.h>
#include <graphics/GUIShaders.h>

using namespace Core;
using namespace Graphics;

// Internal

struct PushConstants
{
	ImVec2 scale;
	ImVec2 translation;

	PushConstants(const ImVec2& scale, const ImVec2& translation) :
		scale(scale),
		translation(translation) { }
};

static constexpr std::array<int32_t, ImGuiKey_COUNT> keyMap
{
	static_cast<int32_t>(KeyCode::Tab),
	static_cast<int32_t>(KeyCode::Left),
	static_cast<int32_t>(KeyCode::Right),
	static_cast<int32_t>(KeyCode::Up),
	static_cast<int32_t>(KeyCode::Down),
	static_cast<int32_t>(KeyCode::PageUp),
	static_cast<int32_t>(KeyCode::PageDown),
	static_cast<int32_t>(KeyCode::Home),
	static_cast<int32_t>(KeyCode::End),
	static_cast<int32_t>(KeyCode::Insert),
	static_cast<int32_t>(KeyCode::Delete),
	static_cast<int32_t>(KeyCode::Backspace),
	static_cast<int32_t>(KeyCode::Space),
	static_cast<int32_t>(KeyCode::Enter),
	static_cast<int32_t>(KeyCode::Escape),
	static_cast<int32_t>(KeyCode::NumpadEnter),
	static_cast<int32_t>(KeyCode::A),
	static_cast<int32_t>(KeyCode::C),
	static_cast<int32_t>(KeyCode::V),
	static_cast<int32_t>(KeyCode::X),
	static_cast<int32_t>(KeyCode::Y),
	static_cast<int32_t>(KeyCode::Z)
};

static std::pair<ByteSpan, Vk::Extent2D> getFontImageData();


// Public

void GUI::begin(const float deltaTime, const Vk::Extent2D& displayExtent)
{
	updateIo(deltaTime, displayExtent);
	ImGui::NewFrame();
}

void GUI::deinitialise(GraphicsSystem& graphicsSystem)
{
	graphicsSystem.removeWindowInputEventHandler(_inputEventHandler);
	destroyDeviceObjects();
	ImGui::DestroyContext();
	_stagingManager.deinitialise();
	_device = nullptr;
}

void GUI::draw(const CommandBuffer& commandBuffer)
{
	ImGui::Render();
	const ImDrawData& drawData = *ImGui::GetDrawData();

	if(drawData.TotalVtxCount == 0)
		return;

	const DrawBuffers& drawBuffers = getDrawBuffers(drawData);
	stageDrawBuffers(drawBuffers, drawData);
	draw(commandBuffer, drawBuffers, drawData);
}

void GUI::end()
{
	ImGui::EndFrame();
	ImGui::GetIO().MouseWheel = 0.0f;
}

void GUI::initialise(GraphicsSystem& graphicsSystem, const GraphicsDevice& device,
	const Vk::RenderPass renderPass, const uint32_t drawBufferCount)
{
	_device = &device;
	_drawBuffers.resize(drawBufferCount);
	_stagingManager.initialise(device, drawBufferCount);
	initialiseImGui();
	_inputEventHandler.initialise();
	createDeviceObjects(renderPass);
	graphicsSystem.addWindowInputEventHandler(_inputEventHandler);
}

// Private

void GUI::updateIo(const float deltaTime, const Vk::Extent2D& displayExtent)
{
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = deltaTime;
	io.DisplaySize = ImVec2(static_cast<float>(displayExtent.width),
		static_cast<float>(displayExtent.height));
}

void GUI::destroyDeviceObjects()
{
	for(DrawBuffers& drawBuffers : _drawBuffers)
	{
		_device->destroyObject(drawBuffers.indexBuffer);
		_device->destroyObject(drawBuffers.vertexBuffer);
	}

	_drawBuffers.clear();
	_descriptorSet = {};
	_device->destroyObject(_descriptorPool);
	_descriptorPool = {};
	_device->destroyObject(_fontImageView);
	_fontImageView = {};
	_device->destroyObject(_fontImage);
	_fontImage = {};
	_device->destroyObject(_pipeline);
	_pipeline = {};
	_device->destroyObject(_pipelineLayout);
	_pipelineLayout = {};
	_device->destroyObject(_descriptorSetLayout);
	_descriptorSetLayout = {};
	_device->destroyObject(_sampler);
	_sampler = {};
}

const GUI::DrawBuffers& GUI::getDrawBuffers(const ImDrawData& drawData)
{
	const Vk::DeviceSize verticesSize = static_cast<Vk::DeviceSize>(drawData.TotalVtxCount) *
		sizeof(ImDrawVert);

	DrawBuffers& drawBuffers = _drawBuffers[_drawBuffersIndex];

	if(verticesSize > drawBuffers.vertexBuffer.size)
		resizeDrawBuffer(drawBuffers.vertexBuffer, verticesSize, Vk::BufferUsageFlagBits::VertexBuffer);

	const Vk::DeviceSize indicesSize = static_cast<Vk::DeviceSize>(drawData.TotalIdxCount) *
		sizeof(ImDrawIdx);

	if(indicesSize > drawBuffers.indexBuffer.size)
		resizeDrawBuffer(drawBuffers.indexBuffer, indicesSize, Vk::BufferUsageFlagBits::IndexBuffer);

	_drawBuffersIndex = (_drawBuffersIndex + 1) % static_cast<uint32_t>(_drawBuffers.size());
	return drawBuffers;
}

void GUI::stageDrawBuffers(const DrawBuffers& buffers, const ImDrawData& drawData)
{
	_stagingManager.beginStaging();
	const Vk::DeviceSize vertexBufferSize = static_cast<Vk::DeviceSize>(drawData.TotalVtxCount) *
		sizeof(ImDrawVert);

	const Buffer vertexBuffer = _stagingManager.getStagingBuffer(vertexBufferSize);
	const gsl::span vertexData = _device->mapMemory<ImDrawVert>(vertexBuffer);
	auto vertexIterator = vertexData.begin();
	const Vk::DeviceSize indexBufferSize = static_cast<Vk::DeviceSize>(drawData.TotalIdxCount) *
		sizeof(ImDrawIdx);

	const Buffer indexBuffer = _stagingManager.getStagingBuffer(indexBufferSize);
	const gsl::span indexData = _device->mapMemory<ImDrawIdx>(indexBuffer);
	auto indexIterator = indexData.begin();

	for(int32_t i = 0; i < drawData.CmdListsCount; ++i)
	{
		const ImDrawList& commandList = *(drawData.CmdLists[i]);
		std::copy(commandList.VtxBuffer.begin(), commandList.VtxBuffer.end(), vertexIterator);
		vertexIterator += commandList.VtxBuffer.size();
		std::copy(commandList.IdxBuffer.begin(), commandList.IdxBuffer.end(), indexIterator);
		indexIterator += commandList.IdxBuffer.size();
	}

	_device->unmapMemory(indexBuffer);
	_device->unmapMemory(vertexBuffer);
	Vk::BufferCopy copyRegion(0, 0, vertexBufferSize);
	_stagingManager.stageBuffer(vertexBuffer.object, copyRegion, buffers.vertexBuffer.object,
		Vk::PipelineStageFlagBits::VertexInput, Vk::AccessFlagBits::VertexAttributeRead);

	copyRegion.size = indexBufferSize;
	_stagingManager.stageBuffer(indexBuffer.object, copyRegion, buffers.indexBuffer.object,
		Vk::PipelineStageFlagBits::VertexInput, Vk::AccessFlagBits::IndexRead);

	_stagingManager.endStaging();
}

void GUI::draw(const CommandBuffer& commandBuffer, const DrawBuffers& buffers, const ImDrawData& drawData)
	const
{
	setDrawState(commandBuffer, buffers, drawData);
	uint32_t vertexOffset = 0;
	uint32_t indexOffset = 0;

	for(int32_t i = 0; i < drawData.CmdListsCount; ++i)
	{
		const ImDrawList& commandList = *(drawData.CmdLists[i]);

		for(const ImDrawCmd& command : commandList.CmdBuffer)
		{
			if(command.UserCallback != nullptr)
			{
				if(command.UserCallback == ImDrawCallback_ResetRenderState)
					setDrawState(commandBuffer, buffers, drawData);
				else
					command.UserCallback(&commandList, &command);

				continue;
			}

			setScissor(commandBuffer, command.ClipRect);
			commandBuffer.drawIndexed(command.ElemCount, 1, indexOffset + command.IdxOffset,
				vertexOffset + command.VtxOffset, 0);
		}

		vertexOffset += static_cast<uint32_t>(commandList.VtxBuffer.size());
		indexOffset += static_cast<uint32_t>(commandList.IdxBuffer.size());
	}
}

void GUI::initialiseImGui()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();

	ImGuiIO& io = ImGui::GetIO();
	io.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset;
	io.BackendRendererName = "Vulkan";
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	io.IniFilename = nullptr;
	std::copy(keyMap.begin(), keyMap.end(), io.KeyMap);
}

void GUI::createDeviceObjects(const Vk::RenderPass renderPass)
{
	createSampler();
	createLayouts();
	createPipeline(renderPass);
	createAndStageFontImage();
	initialiseDescriptors();
}

void GUI::resizeDrawBuffer(DrawBuffer& drawBuffer, const Vk::DeviceSize size,
	const Vk::BufferUsageFlags usage)
{
	_device->destroyObject(drawBuffer);
	const Buffer buffer = _device->createBuffer(size, usage | Vk::BufferUsageFlagBits::TransferDst,
		MemoryUsage::Device);

	drawBuffer = DrawBuffer(buffer, size);
}

void GUI::setDrawState(const CommandBuffer& commandBuffer, const DrawBuffers& buffers,
	const ImDrawData& drawData) const
{
	commandBuffer.bindPipeline(Vk::PipelineBindPoint::Graphics, _pipeline);
	commandBuffer.bindDescriptorSets(Vk::PipelineBindPoint::Graphics, _pipelineLayout, 0,
		toSpan(_descriptorSet), {});

	commandBuffer.setViewport(0.0f, 0.0f, drawData.DisplaySize.x, drawData.DisplaySize.y, 0.0f, 1.0f);
	commandBuffer.bindVertexBuffer(buffers.vertexBuffer.object);
	commandBuffer.bindIndexBuffer(buffers.indexBuffer.object, Vk::IndexType::Uint16);

	const ImVec2 scale(2.0f / drawData.DisplaySize.x, 2.0f / drawData.DisplaySize.y);
	const ImVec2 translation(-1.0f - scale.x * drawData.DisplayPos.x,
		-1.0f - scale.y * drawData.DisplayPos.y);

	const PushConstants pushConstants(scale, translation);
	commandBuffer.pushConstants(_pipelineLayout, Vk::ShaderStageFlagBits::Vertex, 0, toSpan(pushConstants));
}

void GUI::setScissor(const CommandBuffer& commandBuffer, const ImVec4& rectangle) const
{
	const uint32_t scissorWidth = static_cast<uint32_t>(rectangle.z - rectangle.x);
	const uint32_t scissorHeight = static_cast<uint32_t>(rectangle.w - rectangle.y);
	commandBuffer.setScissor(static_cast<uint32_t>(rectangle.x), static_cast<uint32_t>(rectangle.y),
		scissorWidth, scissorHeight);
}

void GUI::createSampler()
{
	const Vk::Filter filter = Vk::Filter::Linear;
	const Vk::SamplerAddressMode addressMode = Vk::SamplerAddressMode::Repeat;
	const Vk::SamplerCreateInfo createInfo({}, filter, filter, Vk::SamplerMipmapMode::Linear, addressMode,
		addressMode, addressMode, 0.0f, false, 0.0f, false, Vk::CompareOp::Never, 0.0f, 1000.0f);

	_sampler = _device->createSampler(createInfo);
}

void GUI::createLayouts()
{
	const Vk::DescriptorSetLayoutBinding layoutBinding(0, Vk::DescriptorType::CombinedImageSampler, 1,
		Vk::ShaderStageFlagBits::Fragment, toSpan(_sampler));

	_descriptorSetLayout = _device->createDescriptorSetLayout(toSpan(layoutBinding));
	const Vk::PushConstantRange pushConstantRange(Vk::ShaderStageFlagBits::Vertex, 0, sizeof(PushConstants));
	_pipelineLayout = _device->createPipelineLayout(toSpan(_descriptorSetLayout), toSpan(pushConstantRange));
}

void GUI::createPipeline(const Vk::RenderPass renderPass)
{
	const Vk::ShaderModule vertexShader = _device->createShaderModule(guiShaderCodeVertex);
	const Vk::ShaderModule fragmentShader = _device->createShaderModule(guiShaderCodeFragment);

	const std::array shaderStages
	{
		Vk::PipelineShaderStageCreateInfo({}, Vk::ShaderStageFlagBits::Vertex, vertexShader, "main"),
		Vk::PipelineShaderStageCreateInfo({}, Vk::ShaderStageFlagBits::Fragment, fragmentShader, "main")
	};

	const Vk::VertexInputBindingDescription vertexInputBinding(0, sizeof(ImDrawVert),
		Vk::VertexInputRate::Vertex);

	const std::array vertexInputAttributes
	{
		Vk::VertexInputAttributeDescription(0, 0, Vk::Format::R32G32Sfloat, offsetof(ImDrawVert, pos)),
		Vk::VertexInputAttributeDescription(1, 0, Vk::Format::R32G32Sfloat, offsetof(ImDrawVert, uv)),
		Vk::VertexInputAttributeDescription(2, 0, Vk::Format::R8G8B8A8Unorm, offsetof(ImDrawVert, col)),
	};

	const Vk::PipelineVertexInputStateCreateInfo vertexInputState({}, toSpan(vertexInputBinding),
		vertexInputAttributes);

	const Vk::PipelineInputAssemblyStateCreateInfo inputAssemblyState({},
		Vk::PrimitiveTopology::TriangleList);

	const Vk::Viewport viewport;
	const Vk::Rect2D scissorRectangle;
	const Vk::PipelineViewportStateCreateInfo viewportState({}, toSpan(viewport), toSpan(scissorRectangle));

	const Vk::PipelineRasterizationStateCreateInfo rasterisationState({}, false, false, Vk::PolygonMode::Fill,
		Vk::CullModeFlagBits::None, Vk::FrontFace::CounterClockwise, false, 0.0f, 0.0f, 0.0f, 1.0f);

	const Vk::PipelineMultisampleStateCreateInfo multisampleState;
	const Vk::PipelineDepthStencilStateCreateInfo depthState;

	const Vk::ColorComponentFlags colourWriteMask = Vk::ColorComponentFlagBits::R |
		Vk::ColorComponentFlagBits::G | Vk::ColorComponentFlagBits::B | Vk::ColorComponentFlagBits::A;

	const Vk::PipelineColorBlendAttachmentState blendAttachmentState(true, Vk::BlendFactor::SrcAlpha,
		Vk::BlendFactor::OneMinusSrcAlpha, Vk::BlendOp::Add, Vk::BlendFactor::One,
		Vk::BlendFactor::OneMinusSrcAlpha, Vk::BlendOp::Add, colourWriteMask);

	const Vk::PipelineColorBlendStateCreateInfo blendState({}, false, Vk::LogicOp::Clear,
		toSpan(blendAttachmentState));

	const std::array dynamicStates{ Vk::DynamicState::Scissor, Vk::DynamicState::Viewport };
	const Vk::PipelineDynamicStateCreateInfo dynamicState({}, dynamicStates);

	const Vk::GraphicsPipelineCreateInfo createInfo({}, shaderStages, vertexInputState, inputAssemblyState,
		{}, viewportState, rasterisationState, multisampleState, depthState, blendState, dynamicState,
		_pipelineLayout, renderPass);

	_pipeline = _device->createPipeline(createInfo);

	_device->destroyObject(fragmentShader);
	_device->destroyObject(vertexShader);
}

void GUI::createAndStageFontImage()
{
	const Vk::Format format = Vk::Format::R8G8B8A8Srgb;
	const auto data = getFontImageData();
	const Vk::ImageUsageFlags usage = Vk::ImageUsageFlagBits::Sampled | Vk::ImageUsageFlagBits::TransferDst;
	_fontImage = _device->createImage(format, data.second, usage, MemoryUsage::Device);
	_stagingManager.beginStaging();

	const Vk::ImageSubresourceLayers subresourceLayers(Vk::ImageAspectFlagBits::Color, 0, 0, 1);
	const Vk::Extent3D copyExtent(data.second.width, data.second.height, 1);
	const Vk::BufferImageCopy copyRegion(0, data.second.width, data.second.height, subresourceLayers,
		Vk::Offset3D(), copyExtent);

	_stagingManager.stageImage(data.first, copyRegion, _fontImage.object,
		Vk::PipelineStageFlagBits::TopOfPipe, Vk::PipelineStageFlagBits::FragmentShader,
		Vk::ImageLayout::Undefined, Vk::ImageLayout::ShaderReadOnlyOptimal);

	_stagingManager.endStaging();
	_fontImageView = _device->createImageView(_fontImage.object, format);
}

void GUI::initialiseDescriptors()
{
	const Vk::DescriptorPoolSize poolSize(Vk::DescriptorType::CombinedImageSampler, 1);
	_descriptorPool = _device->createDescriptorPool(1, toSpan(poolSize));
	_descriptorSet = _device->allocateDescriptorSet(_descriptorPool, _descriptorSetLayout);

	const Vk::DescriptorImageInfo imageInfo(Vk::Sampler(), _fontImageView,
		Vk::ImageLayout::ShaderReadOnlyOptimal);

	const Vk::WriteDescriptorSet descriptorWrite(_descriptorSet, 0, 0,
		Vk::DescriptorType::CombinedImageSampler, toSpan(imageInfo));

	_device->updateDescriptorSets(toSpan(descriptorWrite));
}

// Internal

static std::pair<ByteSpan, Vk::Extent2D> getFontImageData()
{
	uint8_t* data = nullptr;
	int32_t width = 0;
	int32_t height = 0;
	ImGui::GetIO().Fonts->GetTexDataAsRGBA32(&data, &width, &height);
	const ByteSpan span(reinterpret_cast<std::byte*>(data), width * height * 4);

	return std::pair(span, Vk::Extent2D(static_cast<uint32_t>(width), static_cast<uint32_t>(height)));
}
