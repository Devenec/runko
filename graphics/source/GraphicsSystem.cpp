/**
 * @file graphics/GraphicsSystem.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <graphics/GraphicsSystem.h>

#include <core/Config.h>
#include <core/Logger.h>
#include <graphics/GraphicsDevice.h>
#include <graphics/MemoryAllocator.h>
#include <graphics/Swapchain.h>
#include <graphics/SwapchainCreator.h>

using namespace Core;
using namespace Graphics;

// Internal

static const char* getDeviceTypeName(const Vk::PhysicalDeviceType deviceType);
static void logDisplays(const DisplayVector& displays);


// Public

GraphicsDevice GraphicsSystem::createDevice(const size_t adapterIndex) const
{
	logger.write(LogLevel::Info, "{} Creating graphics device for adapter '{}. {}'...", tag,
		adapterIndex, _adapters[adapterIndex].name);

	const PhysicalDevice& physicalDevice = _graphics.physicalDevice(adapterIndex);
	DevicePair devicePair = _graphics.createDevice(physicalDevice, _adapters[adapterIndex].name);
	logger.write(LogLevel::Info, "{} Created graphics device {}", tag, Address(devicePair.first.handle()));
	const GraphicsDeviceInfo deviceInfo(adapterIndex, physicalDevice.queueFamilyIndexGraphics,
		physicalDevice.queueFamilyIndexTransfer);

	const VmaAllocator allocator =
		_graphics.createMemoryAllocator(adapterIndex, devicePair.first, *(devicePair.second));

	return GraphicsDevice(devicePair.first, std::move(devicePair.second), deviceInfo,
		MemoryAllocator(allocator));
}

Swapchain GraphicsSystem::createSwapchain(const GraphicsDevice& device, const WindowHandle windowHandle,
	const Vk::ImageUsageFlags additionalUsageFlags) const
{
	const Vk::SurfaceKHR surface = createSurface(windowHandle);

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	_graphics.checkDeviceSurfaceSupport(device._deviceIndex, surface);

#endif

	SwapchainCreator creator(_graphics);
	const Rectangle windowRectangle = _windowManager.getWindowRectangle(windowHandle);
	const Vk::Extent2D surfaceExtent(windowRectangle.width, windowRectangle.height);

	return creator.createSwapchain(device, surface, surfaceExtent, additionalUsageFlags);
}

WindowHandle GraphicsSystem::createWindow(const Display& display, const uint32_t width, const uint32_t height)
{
	const WindowHandle handle = _windowManager.createWindow(display, width, height);
	const Address handleId = _windowManager.getWindowHandleId(handle);
	logger.write(LogLevel::Info, "{} Created window {} ({}, {}) @ display {}", tag, handleId, width, height,
		display.rectangle);

	return handle;
}

void GraphicsSystem::destroyDevice(GraphicsDevice& device) const
{
	_graphics.destroyMemoryAllocator(device._memoryAllocator._allocator);
	const Address deviceAddress = reinterpret_cast<uintptr_t>(device._object.handle());
	_graphics.destroyDevice(device._object, device._dispatch);
	logger.write(LogLevel::Info, "{} Destroyed graphics device {}", tag, deviceAddress);
	device.reset();
}

void GraphicsSystem::destroySwapchain(const GraphicsDevice& device, Swapchain& swapchain) const
{
	const Vk::SurfaceKHR surface = swapchain._surface;
	SwapchainCreator::destroySwapchain(device, swapchain);
	_graphics.destroySurface(surface);
}

void GraphicsSystem::destroyWindow(const WindowHandle windowHandle)
{
	const Address handleId = _windowManager.getWindowHandleId(windowHandle);
	_windowManager.destroyWindow(windowHandle);
	logger.write(LogLevel::Info, "{} Destroyed window {}", tag, handleId);
}

// Private

void GraphicsSystem::createAdapterVector(std::vector<DisplayVector>& displayVectors)
{
	const size_t deviceCount = _graphics.physicalDeviceCount();
	logger.write(LogLevel::Info, "Graphics adapters and displays:");

	for(size_t i = 0; i < deviceCount; ++i)
	{
		const Vk::PhysicalDeviceProperties deviceProperties = _graphics.getPhysicalDeviceProperties(i);
		logger.write(LogLevel::Info, "  {}. {}", i, deviceProperties.deviceName);
		logger.write(LogLevel::Info, "    API version: {}.{}.{} | Type: {}\n",
			Vk::versionMajor(deviceProperties.apiVersion), Vk::versionMinor(deviceProperties.apiVersion),
			Vk::versionPatch(deviceProperties.apiVersion), getDeviceTypeName(deviceProperties.deviceType));

		logDisplays(displayVectors[i]);
		_adapters.emplace_back(deviceProperties.deviceName, std::move(displayVectors[i]));
	}
}


// Internal

static const char* getDeviceTypeName(const Vk::PhysicalDeviceType deviceType)
{
	switch(deviceType)
	{
		case Vk::PhysicalDeviceType::Other:
			return "Other";

		case Vk::PhysicalDeviceType::IntegratedGpu:
			return "Integrated GPU";

		case Vk::PhysicalDeviceType::DiscreteGpu:
			return "Discrete GPU";

		case Vk::PhysicalDeviceType::VirtualGpu:
			return "Virtual GPU";

		case Vk::PhysicalDeviceType::Cpu:
			return "CPU";
	}

	return "Unknown";
}

static void logDisplays(const DisplayVector& displays)
{
	for(uint32_t i = 0; i < displays.size(); ++i)
	{
		const Display& display = displays[i];
		logger.write(LogLevel::Info, "    Display {}", i);
		const Rectangle& rectangle = display.rectangle;
		const char* lineBreak = (i == displays.size() - 1) ? "\n" : "";
		logger.write(LogLevel::Info, "      {} {} Hz{}", rectangle, display.frequency, lineBreak);
	}
}
