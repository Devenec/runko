#version 450 core

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec2 inTextureCoordinate;
layout(location = 2) in vec4 inColour;

layout(push_constant) uniform Constants
{
	vec2 scale;
	vec2 translation;
} constants;

layout(location = 0) out struct Outputs
{
	vec4 colour;
	vec2 textureCoordinate;
} outputs;

void main()
{
	gl_Position = vec4(inPosition * constants.scale + constants.translation, 0.0f, 1.0f);
	outputs.colour = inColour;
	outputs.textureCoordinate = inTextureCoordinate;
}
