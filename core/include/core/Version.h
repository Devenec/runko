/**
 * @file core/Version.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/VersionInternal.h>

namespace Core
{
	namespace Version
	{
		constexpr std::string_view buildArchitecture = Internal::getBuildArchitecture();
		constexpr std::string_view buildConfig       = Internal::getBuildConfig();
		constexpr BuildDateTime buildDateTime        = Internal::formatBuildDateTime("+0200");
		constexpr std::string_view buildSystem       = Internal::getBuildSystem();

		constexpr uint32_t makeVersion(const uint32_t majorNumber, const uint32_t minorNumber,
			const uint32_t buildNumber)
		{
			return (majorNumber << 22) | (minorNumber << 12) | buildNumber;
		}

		constexpr uint32_t number = makeVersion(0, 0, 1);

		constexpr uint32_t getBuildNumber(const uint32_t versionNumber)
		{
			return versionNumber & 0x0FFF;
		}

		constexpr uint32_t getMajorNumber(const uint32_t versionNumber)
		{
			return versionNumber >> 22;
		}

		constexpr uint32_t getMinorNumber(const uint32_t versionNumber)
		{
			return (versionNumber >> 12) & 0x03FF;
		}
	}
}
