/**
 * @file core/Logger.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <string_view>
#include <utility>

#include <core/Config.h>
#include <core/LogSystem.h>

namespace Core
{
	enum class LogLevel
	{
		Debug,
		Info,
		Warning,
		Error
	};

	class Logger
	{
	public:

		Logger() = default;

		Logger(const Logger& logger) = delete;
		Logger(Logger&& logger) = delete;

		~Logger() = default;

		void setFilterLevel(const LogLevel level)
		{
			_filterLevel = level;
		}

		void write(const LogLevel level, const std::string_view& message) const
		{
			if(level >= _filterLevel)
				logSystem.write(level, message);
		}

		template<typename... Arguments>
		void write(const LogLevel level, const std::string_view& format, Arguments&&... arguments) const
		{
			if(level >= _filterLevel)
				logSystem.write(level, format, std::forward<Arguments>(arguments)...);
		}

		template<typename... Arguments>
		void writeFlushAndWait(const LogLevel level, const std::string_view& format, Arguments&&... arguments)
			const
		{
			if(level >= _filterLevel)
				logSystem.writeFlushAndWait(level, format, std::forward<Arguments>(arguments)...);
		}

		Logger& operator =(const Logger& logger) = delete;
		Logger& operator =(Logger&& logger) = delete;

	private:

		static constexpr LogLevel initialFilterLevel()
		{
#if defined(RUNKO_CONFIG_DEVELOPMENT)

			return LogLevel::Debug;

#else

			return LogLevel::Info;

#endif
		}

		LogLevel _filterLevel = initialFilterLevel();
	};

	extern Logger logger;
}
