/**
 * @file core/StringFormattingInternal.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <string_view>
#include <utility>
#include <gsl/span>

#include <core/Types.h>
#include <fmt/format.h>

namespace Core
{
	template<typename... Arguments>
	size_t formatStringToBuffer(const gsl::span<char>& buffer, const std::string_view& format,
		Arguments&&... arguments)
	{
		const fmt::format_to_n_result result = fmt::format_to_n(buffer.data(), buffer.size(), format,
			std::forward<Arguments>(arguments)...);

		buffer[std::min(result.size, buffer.size() - 1)] = '\0';
		return result.size;
	}
}
