/**
 * @file core/LogSystem.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <array>
#include <chrono>
#include <condition_variable>
#include <ctime>
#include <mutex>
#include <string>
#include <string_view>
#include <thread>
#include <utility>

#include <core/Assert.h>
#include <core/Config.h>
#include <core/StringFormatting.h>
#include <core/Types.h>

namespace Core
{
	enum class LogLevel;

	class LogSystem
	{
	public:

		LogSystem() = default;

		LogSystem(const LogSystem& logSystem) = delete;
		LogSystem(LogSystem&& logSystem) = delete;

		~LogSystem() = default;

		void deinitialise()
		{
			queueTerminateMessage();
			_messageProcessingThread.join();
		}

		void initialise();

		void write(const LogLevel logLevel, const std::string_view& message)
		{
			{
				std::unique_lock lock(_messageProcessingMutex);
				Message& queueMessage = getTailMessage(lock, Message::Actions::Write, logLevel);
				queueMessage.buffer = message;
			}

			_messagePopCondition.notify_one();
		}

		template<typename... Arguments>
		void write(const LogLevel logLevel, const std::string_view& format, Arguments&&... arguments)
		{
			{
				std::unique_lock lock(_messageProcessingMutex);
				Message& queueMessage = getTailMessage(lock, Message::Actions::Write, logLevel);
				formatStringToString(queueMessage.buffer, format, std::forward<Arguments>(arguments)...);
			}

			_messagePopCondition.notify_one();
		}

		template<typename... Arguments>
		void writeFlushAndWait(const LogLevel logLevel, const std::string_view& format,
			Arguments&&... arguments)
		{
			std::unique_lock lock(_messageProcessingMutex);
			Message& queueMessage = getTailMessage(lock, Message::Actions::WriteFlushSignal, logLevel);
			formatStringToString(queueMessage.buffer, format, std::forward<Arguments>(arguments)...);
			_messagePopCondition.notify_one();
			const uint64_t valueToWait = _waitValue + 1;

			_messageWaitCondition.wait(lock, [this, valueToWait]()
			{
				return valueToWait <= _waitValue;
			});
		}

		LogSystem& operator =(const LogSystem& logSystem) = delete;
		LogSystem& operator =(LogSystem&& logSystem) = delete;

	private:

		struct Message
		{
			enum class Actions
			{
				None             = 0,
				Flush            = 1,
				Signal           = 2,
				Terminate        = 4,
				Write            = 8,

				WriteFlushSignal = Write | Flush | Signal
			};

			std::string buffer = std::string(Config::logSystemMessageInitialBufferSize, '\0');
			std::time_t timestamp{};
			LogLevel level{};
			Actions actions = Actions::None;

			Message() = default;

			Message(const Message& message) = delete;
			Message(Message&& message) = default;

			Message& operator =(const Message& message) = delete;
			Message& operator =(Message&& message) = default;
		};

		class MessageQueue
		{
		public:

			MessageQueue() = default;

			~MessageQueue() = default;

			Message& getHead()
			{
				RUNKO_ASSERT(!isEmpty());

				Message& message = _messages[_head];
				_head = (_head + 1) % _messages.size();

				return message;
			}

			Message& getTail()
			{
				RUNKO_ASSERT(!isFull());

				Message& message = _messages[_tail];
				_tail = (_tail + 1) % _messages.size();

				return message;
			}

			bool isEmpty() const
			{
				return _head == _tail;
			}

			bool isFull() const
			{
				return _head == (_tail + 1) % _messages.size();
			}

		private:

			std::array<Message, Config::logSystemMessageQueueSize> _messages;
			uint32_t _head = 0;
			uint32_t _tail = 0;
		};

		std::condition_variable _messagePopCondition;
		std::condition_variable _messagePushCondition;
		std::condition_variable _messageWaitCondition;
		std::mutex _messageProcessingMutex;
		std::thread _messageProcessingThread;
		MessageQueue _messageQueue;
		uint64_t _waitValue = 0;
		uint32_t _currentDay = 0;

		void queueTerminateMessage()
		{
			{
				std::unique_lock lock(_messageProcessingMutex);
				getTailMessage(lock, Message::Actions::Terminate, {});
			}

			_messagePopCondition.notify_one();
		}

		void writeVersionInfo();
		Message& getTailMessage(std::unique_lock<std::mutex>& lock, const Message::Actions messageActions,
			const LogLevel logLevel);

		Message& getHeadMessage(std::unique_lock<std::mutex>& lock);
		void writeMessage(const Message& message);

		void messageProcessingThread();

		static std::time_t getTimestamp()
		{
			return std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		}

		static void writeDateInfo(const LogLevel logLevel, const std::tm& dateTime);

		static void initialisePlatform();
		static void writePlatform(const std::string_view& header, const std::string_view& message);
		static void flushPlatform();
	};

	extern LogSystem logSystem;
}
