/**
 * @file core/Assert.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Config.h>

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	#include <core/Types.h>

	#define RUNKO_ASSERT(expression) \
		((expression) ? static_cast<void>(0) : \
			Core::failAssert(#expression, __FILE__, __LINE__))

#else

	#define RUNKO_ASSERT(expression) \
		(static_cast<void>(0))

#endif

#if defined(RUNKO_CONFIG_DEVELOPMENT)

namespace Core
{
	constexpr uint32_t maxAssertMessageLength = 1023;

	void failAssert(const char* expression, const char* filename, const uint32_t line);
}

#endif
