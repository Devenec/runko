/**
 * @file core/Rectangle.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <core/Assert.h>
#include <core/Types.h>

namespace Core
{
	class Rectangle
	{
	public:

		int32_t x;

		int32_t y;

		int32_t width;

		int32_t height;

		Rectangle() = default;

		constexpr Rectangle(const int32_t x, const int32_t y, const int32_t width, const int32_t height) :
			x(x),
			y(y),
			width(width),
			height(height)
		{
			RUNKO_ASSERT(width >= 0);
			RUNKO_ASSERT(height >= 0);
		}

		constexpr Rectangle(const Rectangle& rectangle) = default;
		constexpr Rectangle(Rectangle&& rectangle) = default;

		~Rectangle() = default;

		constexpr int32_t bottom() const
		{
			return y + height;
		}

		constexpr int32_t left() const
		{
			return x;
		}

		constexpr int32_t right() const
		{
			return x + width;
		}

		constexpr int32_t top() const
		{
			return y;
		}

		constexpr Rectangle& operator =(const Rectangle& rectangle) = default;
		constexpr Rectangle& operator =(Rectangle&& rectangle) = default;
	};

	inline Bool operator ==(const Rectangle& rectangleA, const Rectangle& rectangleB)
	{
		return rectangleA.x == rectangleB.x && rectangleA.y == rectangleB.y &&
			rectangleA.width == rectangleB.width && rectangleA.height == rectangleB.height;
	}

	inline Bool operator !=(const Rectangle& rectangleA, const Rectangle& rectangleB)
	{
		return !(rectangleA == rectangleB);
	}
}
