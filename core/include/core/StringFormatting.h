/**
 * @file core/StringFormatting.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <string>

#include <core/Rectangle.h>
#include <core/StaticString.h>
#include <core/StringFormattingInternal.h>

namespace Core
{
	struct Address
	{
		uintptr_t value;

		constexpr Address(const uintptr_t value) :
			value(value) { }

		template<typename T>
		constexpr Address(const T value) :
			value(reinterpret_cast<uintptr_t>(value)) { }
	};

	struct ErrorCode
	{
		uint32_t value;

		constexpr ErrorCode(const uint32_t value) :
			value(value) { }
	};

	struct SourceLocation
	{
		std::string_view file;
		uint32_t line;

		constexpr SourceLocation(const std::string_view& file, const uint32_t line) :
			file(file),
			line(line) { }
	};

	template<typename... Arguments>
	std::string formatString(const std::string_view& format, Arguments&&... arguments)
	{
		const size_t length = fmt::formatted_size(format, std::forward<Arguments>(arguments)...);
		std::string string(length, '\0');
		const fmt::format_to_n_result result = fmt::format_to_n(string.begin(), length, format,
			std::forward<Arguments>(arguments)...);

		string.resize(result.size);
		return string;
	}

	template<typename... Arguments>
	void formatStringToString(std::string& string, const std::string_view& format, Arguments&&... arguments)
	{
		const size_t length = fmt::formatted_size(format, std::forward<Arguments>(arguments)...);
		string.resize(length);
		const fmt::format_to_n_result result = fmt::format_to_n(string.begin(), length, format,
			std::forward<Arguments>(arguments)...);

		string.resize(result.size);
	}
}

template<>
struct fmt::formatter<Core::Address>
{
	constexpr auto parse(format_parse_context& context) const
	{
		return context.begin();
	}

	template<typename FormatContext>
	auto format(const Core::Address address, FormatContext& context) const
	{
		return format_to(context.out(), "0x{:016X}", address.value);
	}
};

template<>
struct fmt::formatter<Core::ErrorCode>
{
	constexpr auto parse(format_parse_context& context) const
	{
		return context.begin();
	}

	template<typename FormatContext>
	auto format(const Core::ErrorCode errorCode, FormatContext& context) const
	{
		return format_to(context.out(), "0x{:08X}", errorCode.value);
	}
};

template<>
struct fmt::formatter<Core::Rectangle>
{
	constexpr auto parse(format_parse_context& context) const
	{
		return context.begin();
	}

	template<typename FormatContext>
	auto format(const Core::Rectangle& rectangle, FormatContext& context) const
	{
		return format_to(context.out(), "({}, {}, {}, {})", rectangle.x, rectangle.y, rectangle.width,
			rectangle.height);
	}
};

template<>
struct fmt::formatter<Core::SourceLocation>
{
	constexpr auto parse(format_parse_context& context) const
	{
		return context.begin();
	}

	template<typename FormatContext>
	auto format(const Core::SourceLocation& sourceLocation, FormatContext& context) const
	{
		return format_to(context.out(), "{}:{}", sourceLocation.file, sourceLocation.line);
	}
};

template<size_t Capacity>
struct fmt::formatter<Core::StaticString<Capacity>> : formatter<string_view>
{
	template<typename FormatContext>
	auto format(const Core::StaticString<Capacity>& string, FormatContext& context)
	{
		return formatter<string_view>::format(static_cast<std::string_view>(string), context);
	}
};
