/**
 * @file core/StaticString.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <array>
#include <limits>
#include <string_view>
#include <utility>

#include <core/Assert.h>
#include <core/StringFormattingInternal.h>
#include <core/Types.h>
#include <core/Utility.h>

namespace Core
{
	template<size_t Capacity>
	class StaticString
	{
	public:

		using ConstIterator = typename std::array<char, Capacity + 1>::const_iterator;
		using Iterator      = typename std::array<char, Capacity + 1>::iterator;

		constexpr StaticString()
		{
			_data[0] = '\0';
		}

		constexpr StaticString(const char* characters) :
			StaticString(std::string_view(characters)) { }

		constexpr StaticString(const std::string_view& characters) :
			_length(std::min(Capacity, characters.length()))
		{
			copy(characters.data(), _length, _data.data());
			_data[_length] = '\0';
		}

		constexpr StaticString(const StaticString& staticString) = default;
		constexpr StaticString(StaticString&& staticString) = default;

		~StaticString() = default;

		constexpr StaticString& append(const char character)
		{
			if(_length < Capacity)
			{
				_data[_length] = character;
				_data[++_length] = '\0';
			}

			return *this;
		}

		constexpr StaticString& append(const std::string_view& string)
		{
			const size_t appendLength = std::min(Capacity - _length, string.length());
			copy(string.data(), appendLength, _data.data() + _length);
			_length += appendLength;
			_data[_length] = '\0';

			return *this;
		}

		constexpr Iterator begin()
		{
			return _data.begin();
		}

		constexpr ConstIterator begin() const
		{
			return _data.begin();
		}

		constexpr size_t capacity() const
		{
			return Capacity;
		}

		constexpr void clear()
		{
			_data[0] = '\0';
			_length = 0;
		}

		constexpr char* data()
		{
			return _data.data();
		}

		constexpr const char* data() const
		{
			return _data.data();
		}

		constexpr Iterator end()
		{
			return _data.end();
		}

		constexpr ConstIterator end() const
		{
			return _data.end();
		}

		constexpr char& first()
		{
			return _data[0];
		}

		constexpr const char& first() const
		{
			return _data[0];
		}

		constexpr Bool isEmpty() const
		{
			return _length == 0;
		}

		constexpr char& last()
		{
			return _data[_length - 1];
		}

		constexpr const char& last() const
		{
			return _data[_length - 1];
		}

		constexpr size_t length() const
		{
			return _length;
		}

		constexpr size_t size() const
		{
			return _length;
		}

		constexpr StaticString& operator =(const StaticString& staticString) = default;
		constexpr StaticString& operator =(StaticString&& staticString) = default;

		constexpr char& operator [](const size_t index)
		{
			RUNKO_ASSERT(index < _length);
			return _data[index];
		}

		constexpr const char& operator [](const size_t index) const
		{
			RUNKO_ASSERT(index < _length);
			return _data[index];
		}

		constexpr operator std::string_view() const
		{
			return std::string_view(_data.data(), _length);
		}

		template<typename... Arguments>
		static constexpr StaticString format(const std::string_view& format, Arguments&&... arguments)
		{
			StaticString string;
			string._length = formatStringToBuffer(string._data, format,
				std::forward<Arguments>(arguments)...);

			return string;
		}

	private:

		size_t _length = 0;
		std::array<char, Capacity + 1> _data;
	};
}
