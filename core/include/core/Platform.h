/**
 * @file core/Platform.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

// Compiler

#if defined(_MSC_VER)

	#define RUNKO_PLATFORM_COMPILER_MSVC

	#define RUNKO_INTERNAL_BREAK_DEBUGGER() \
		__debugbreak()

#else

	#error The compiler is not supported.

#endif


// C++

#if !(defined(__cplusplus) && __cplusplus >= 201703L)

	#error The compiler does not support C++17.

#endif


// System

#if !(defined(RUNKO_PLATFORM_SYSTEM_WINDOWS))

	#error The target system is not supported.

#endif


// Architecture

#if !(defined(_WIN64))

	#error The target architecture is not supported.

#endif


// Config

#if !(defined(RUNKO_CONFIG_DEBUG) || defined(RUNKO_CONFIG_PRODUCTION) || defined(RUNKO_CONFIG_RELEASE))

	#error RUNKO_CONFIG_* is not defined. Define RUNKO_CONFIG_DEBUG, RUNKO_CONFIG_PRODUCTION or RUNKO_CONFIG_RELEASE.

#endif
