/**
 * @file core/platform/windows/Windows.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <string>

#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCOMM
#define NOCTLMGR
#define NODEFERWINDOWPOS
#define NODRAWTEXT
#define NOGDICAPMASKS
#define NOHELP
#define NOICONS
#define NOKANJI
#define NOKERNEL
#define NOKEYSTATES
#define NOMB
#define NOMCX
#define NOMEMMGR
#define NOMENUS
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOPROFILER
#define NORASTEROPS
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOSYSCOMMANDS
#define NOSYSMETRICS
#define NOTEXTMETRIC
#define NOWH

#define STRICT
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>

#include <core/Config.h>
#include <core/Types.h>

#if defined(RUNKO_CONFIG_DEVELOPMENT)

	#define RUNKO_WINDOWS_ERROR(errorCode) \
		Platform::Windows::invokeError(static_cast<uint32_t>(errorCode), __FILE__, __LINE__)

#else

	#define RUNKO_WINDOWS_ERROR(errorCode) \
		Platform::Windows::invokeError(static_cast<uint32_t>(errorCode))

#endif

namespace Platform
{
	namespace Windows
	{
#if defined(RUNKO_CONFIG_DEVELOPMENT)

		void invokeError(const uint32_t errorCode, const char* filename, const uint32_t line);

#else

		void invokeError(const uint32_t errorCode);

#endif
	};
}
