/**
 * @file core/platform/windows/WindowsForward.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#define CALLBACK __stdcall

struct HINSTANCE__;
using HINSTANCE = HINSTANCE__*;

struct HMONITOR__;
using HMONITOR = HMONITOR__*;

struct HWND__;
using HWND = HWND__*;

struct _SECURITY_ATTRIBUTES;
using SECURITY_ATTRIBUTES = _SECURITY_ATTRIBUTES;

using DWORD   = unsigned long;
using HANDLE  = void*;
using LPARAM  = __int64;
using LPCWSTR = const wchar_t*;
using LRESULT = __int64;
using WPARAM  = unsigned __int64;
