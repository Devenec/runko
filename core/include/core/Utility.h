/**
 * @file core/Utility.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <type_traits>

#include <core/Platform.h>
#include <core/Types.h>
#include <gsl/span>

#define RUNKO_BREAK_DEBUGGER() \
	RUNKO_INTERNAL_BREAK_DEBUGGER()

namespace Core
{
	template<typename T, Bool = std::is_enum_v<T>>
	struct UnderlyingType
	{
		using Type = std::underlying_type_t<T>;
	};

	template<typename T>
	struct UnderlyingType<T, false>
	{
		using Type = T;
	};

	template<typename FlagsA, typename FlagsB>
	constexpr Bool any(const FlagsA flagsA, const FlagsB flagsB)
	{
		using TypeA = typename UnderlyingType<FlagsA>::Type;
		using TypeB = typename UnderlyingType<FlagsB>::Type;

		return (static_cast<TypeA>(flagsA) & static_cast<TypeB>(flagsB)) != 0;
	}

	template<typename InputIterator, typename Size, typename OutputIterator>
	constexpr OutputIterator copy(InputIterator source, Size count, OutputIterator destination)
	{
		for(; count > 0; --count, ++source, ++destination)
			*destination = *source;

		return destination;
	}

	template<typename FlagsA, typename FlagsB>
	constexpr Bool in(const FlagsA flagsA, const FlagsB flagsB)
	{
		using TypeA = typename UnderlyingType<FlagsA>::Type;
		using TypeB = typename UnderlyingType<FlagsB>::Type;

		return (static_cast<TypeA>(flagsA) & static_cast<TypeB>(flagsB)) == static_cast<TypeB>(flagsB);
	}

	template<typename T>
	inline gsl::span<T> toSpan(T& object)
	{
		return gsl::span(&object, 1);
	}

	template<typename T>
	inline gsl::span<const T> toSpan(const T& object)
	{
		return gsl::span(&object, 1);
	}
}
