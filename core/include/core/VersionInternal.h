/**
 * @file core/VersionInternal.h
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#pragma once

#include <algorithm>
#include <array>
#include <string>
#include <string_view>

#include <core/StaticString.h>
#include <core/Types.h>
#include <core/Utility.h>

namespace Core
{
	namespace Version
	{
		using BuildDateTime = StaticString<24>;

		namespace Internal
		{
			constexpr BuildDateTime formatBuildDateTime(const StaticString<5>& timezone)
			{
				constexpr std::array monthNames
				{
					"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
				};

				BuildDateTime dateTime("0000-00-00T00:00:00+0000");
				copy(__DATE__ + 7, 4, dateTime.data());

				for(size_t i = 0; i < monthNames.size(); ++i)
				{
					if(std::char_traits<char>::compare(monthNames[i], __DATE__, 3) == 0)
					{
						dateTime[5] = '0' + static_cast<char>(i / 9);
						dateTime[6] = '0' + static_cast<char>((i + 1) % 10);
						break;
					}
				}

				dateTime[8] = __DATE__[4] == ' ' ? '0' : __DATE__[4];
				dateTime[9] = __DATE__[5];
				copy(__TIME__, 8, dateTime.data() + 11);
				copy(timezone.begin(), timezone.size(), dateTime.begin() + 19);

				return dateTime;
			}

			constexpr std::string_view getBuildArchitecture()
			{
				return "x64";
			}

			constexpr std::string_view getBuildConfig()
			{
#if defined(RUNKO_CONFIG_DEBUG)

				return "debug";

#elif defined(RUNKO_CONFIG_PRODUCTION)

				return "production";

#elif defined(RUNKO_CONFIG_RELEASE)

				return "release";

#endif
			}

			constexpr std::string_view getBuildSystem()
			{
#if defined(RUNKO_PLATFORM_SYSTEM_WINDOWS)

				return "Windows";

#endif
			}
		}
	}
}
