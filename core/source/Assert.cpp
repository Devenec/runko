/**
 * @file core/Assert.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <core/Assert.h>

#if defined(RUNKO_CONFIG_DEVELOPMENT)

#include <cstdlib>
#include <string>
#include <string_view>

#include <core/Logger.h>
#include <core/StaticString.h>
#include <core/Utility.h>

using namespace Core;

namespace Core
{
	void logAssertFailurePlatform(const std::string_view& message);
}

void Core::failAssert(const char* expression, const char* filename, const uint32_t line)
{
	const StaticString message = StaticString<maxAssertMessageLength>::format("Assertion failed @ {} : '{}'.",
		SourceLocation(filename, line), expression);

	logAssertFailurePlatform(message);
	logger.writeFlushAndWait(LogLevel::Error, message);
	RUNKO_BREAK_DEBUGGER();
	std::abort();
}

#endif
