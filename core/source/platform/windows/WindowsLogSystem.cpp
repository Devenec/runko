/**
 * @file core/platform/windows/WindowsLogSystem.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <core/LogSystem.h>

#include <algorithm>
#include <cstdio>
#include <cwchar>
#include <limits>

#include <core/platform/windows/Windows.h>

using namespace Core;

// Internal

static constexpr size_t maxMessageLength = static_cast<size_t>(std::numeric_limits<int32_t>::max()) - 1;


// Private

// Static

void LogSystem::initialisePlatform()
{
	SetConsoleOutputCP(CP_UTF8);
}

void LogSystem::writePlatform(const std::string_view& header, const std::string_view& message)
{
	const int32_t messageLength = static_cast<int32_t>(std::min(maxMessageLength, message.length()));
	std::wprintf(L"%S%.*S\n", header.data(), messageLength, message.data());
}

void LogSystem::flushPlatform()
{
	std::fflush(stdout);
}
