/**
 * @file core/platform/windows/Windows.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <core/platform/windows/Windows.h>

#include <algorithm>
#include <array>
#include <cstdlib>
#include <limits>
#include <string_view>

#include <core/Assert.h>
#include <core/Config.h>
#include <core/Logger.h>
#include <core/Utility.h>
#include <gsl/span>

using namespace Core;
using namespace Platform;

// Internal

#if defined(RUNKO_CONFIG_DEVELOPMENT)

static constexpr size_t maxConversionCharacterCount =
	static_cast<size_t>(std::numeric_limits<int32_t>::max()) - 1;

static void convertStringToWideCharacters(const std::string_view& string,
	const gsl::span<wchar_t>& wideCharacters);


namespace Core
{
	void logAssertFailurePlatform(const std::string_view& message)
	{
		std::array<wchar_t, maxAssertMessageLength + 1> wideMessage{};
		convertStringToWideCharacters(message, wideMessage);
		OutputDebugStringW(wideMessage.data());
		OutputDebugStringW(L"\n");
	}
}

#endif


// Windows

#if defined(RUNKO_CONFIG_DEVELOPMENT)

void Windows::invokeError(const uint32_t errorCode, const char* filename, const uint32_t line)
{
	const uint32_t windowsErrorCode = GetLastError();
	logger.writeFlushAndWait(LogLevel::Error, "Error occurred: {} (Windows error {}) @ {}.",
		ErrorCode(errorCode), windowsErrorCode, SourceLocation(filename, line));

	RUNKO_BREAK_DEBUGGER();
	std::abort();
}

#else

void Windows::invokeError(const uint32_t errorCode)
{
	const uint32_t windowsErrorCode = GetLastError();
	logger.writeFlushAndWait(LogLevel::Error, "Error occurred: {} (Windows error {}).", ErrorCode(errorCode),
		windowsErrorCode);

	std::abort();
}

#endif


// Internal

#if defined(RUNKO_CONFIG_DEVELOPMENT)

static void convertStringToWideCharacters(const std::string_view& string,
	const gsl::span<wchar_t>& wideCharacters)
{
	int32_t wideCharacterCount = static_cast<int32_t>(std::min(maxConversionCharacterCount,
		wideCharacters.size() - 1));

	const int32_t stringLength = static_cast<int32_t>(std::min(maxConversionCharacterCount, string.length()));
	wideCharacterCount = MultiByteToWideChar(CP_UTF8, 0, string.data(), stringLength,
		wideCharacters.data(), wideCharacterCount);

	wideCharacters[wideCharacterCount] = '\0';
}

#endif
