/**
 * @file core/LogSystem.cpp
 *
 * Runko
 * Copyright 2021 Eetu 'Devenec' Oinasmaa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */

#include <core/LogSystem.h>

#include <core/Logger.h>
#include <core/StaticString.h>
#include <core/Utility.h>
#include <core/Version.h>
#include <fmt/chrono.h>

using namespace Core;

static_assert(sizeof(std::time_t) >= 8, "The size of std::time_t is less than 8 bytes.");

// Internal

using Header = StaticString<12>;

static constexpr std::array logLevelCharacters{ 'D', 'I', 'W', 'E' };

static Header formatHeader(const LogLevel logLevel, const std::tm& dateTime);


// Core

Logger Core::logger;


// Public

void LogSystem::initialise()
{
	initialisePlatform();
	writeVersionInfo();
	_messageProcessingThread = std::thread(&LogSystem::messageProcessingThread, this);
}

// Private

void LogSystem::writeVersionInfo()
{
	const char* messageFormat = "Runko - version {}.{}.{} - {} {}, {} - built on {}\n\n"
		"Logging started on {:%FT%T%z}";

	const std::tm dateTime = fmt::localtime(getTimestamp());
	const std::string message = formatString(messageFormat, Version::getMajorNumber(Version::number),
		Version::getMinorNumber(Version::number), Version::getBuildNumber(Version::number),
		Version::buildSystem, Version::buildArchitecture, Version::buildConfig, Version::buildDateTime,
		dateTime);

	writePlatform("", message);
	_currentDay = static_cast<uint32_t>(dateTime.tm_mday);
}

LogSystem::Message& LogSystem::getTailMessage(std::unique_lock<std::mutex>& lock,
	const Message::Actions messageActions, const LogLevel logLevel)
{
	_messagePushCondition.wait(lock, [this]()
	{
		return !_messageQueue.isFull();
	});

	Message& message = _messageQueue.getTail();
	message.actions = messageActions;
	message.level = logLevel;
	message.timestamp = getTimestamp();
	
	return message;
}

LogSystem::Message& LogSystem::getHeadMessage(std::unique_lock<std::mutex>& lock)
{
	_messagePopCondition.wait(lock, [this]()
	{
		return !_messageQueue.isEmpty();
	});
	
	return _messageQueue.getHead();
}

void LogSystem::writeMessage(const Message& message)
{
	const std::tm dateTime = fmt::localtime(message.timestamp);

	if(_currentDay != static_cast<uint32_t>(dateTime.tm_mday))
	{
		writeDateInfo(message.level, dateTime);
		_currentDay = static_cast<uint32_t>(dateTime.tm_mday);
	}

	writePlatform(formatHeader(message.level, dateTime), message.buffer);
}

void LogSystem::messageProcessingThread()
{
	Message message;

	while(true)
	{
		{
			std::unique_lock lock(_messageProcessingMutex);
			std::swap(message, getHeadMessage(lock));
		}

		_messagePushCondition.notify_all();

		if(in(message.actions, Message::Actions::Write))
			writeMessage(message);

		if(in(message.actions, Message::Actions::Flush))
			flushPlatform();

		if(in(message.actions, Message::Actions::Signal))
		{
			{
				std::unique_lock lock(_messageProcessingMutex);
				++_waitValue;
			}

			_messageWaitCondition.notify_all();
		}

		if(in(message.actions, Message::Actions::Terminate))
			break;
	}
}

// Static

void LogSystem::writeDateInfo(const LogLevel logLevel, const std::tm& dateTime)
{
	const StaticString message = StaticString<26>::format("Date changed to {:%F}", dateTime);
	writePlatform(formatHeader(logLevel, dateTime), message);
}


// Internal

static Header formatHeader(const LogLevel logLevel, const std::tm& dateTime)
{
	const char logLevelCharacter = logLevelCharacters[static_cast<int32_t>(logLevel)];
	return Header::format("{} {:%T}  ", logLevelCharacter, dateTime);
}
