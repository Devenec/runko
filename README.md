# Runko

A C++ Vulkan framework  
Version 0.0.1


## Building

Vulkan loader and validation layer is included in Runko. Vulkan validation is enabled in debug and release
build by default, and requires path *<Runko root>\build\vulkan-layers* to be included in `VK_LAYER_PATH`
environment variable, which is a semicolon-separated list of paths the loader searches layers from. The
validation can be disabled in *core/include/core/Config.h* by removing `RUNKO_CONFIG_GRAPHICS_VALIDATE`
definition.

Python 3 (only 3.7 has been tested) is required when building Vulkan validation layer.

Open *build/runko.sln* in Visual Studio supporting C++17 (only Visual Studio 2019 has been tested), and build
the solution.


## Copyright & Licenses

Copyright 2021 Eetu 'Devenec' Oinasmaa  
Licensed under [MIT License](LICENSE.txt)

The third party source code may be licensed under different licenses, as described in
[LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt).
