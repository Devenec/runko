#!/usr/bin/python3

#
# @file external/khronos-validation-layer/scripts/generate-layer-json.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

import os
import sys
import xml.etree.ElementTree

class Header:
	apiVersionPrefix = "VK_API_VERSION_"
	version          = "VK_HEADER_VERSION"

def getArgumentFilePath(index, mustExist):
	argument = getArgument(index)
	path = os.path.abspath(argument)

	if mustExist and not os.path.isfile(path):
		raise RuntimeError("argument {} ({}) must be an existing file".format(index, argument))

	return path

def getVulkanApiVersion(vulkanRegistryPath):
	registryElement = xml.etree.ElementTree.parse(vulkanRegistryPath).getroot()
	apiVersion = "1_0"
	headerVersion = ""

	for nameElement in registryElement.findall("types/type[@category='define']/name"):
		if nameElement.text.startswith(Header.apiVersionPrefix):
			currentApiVersion = nameElement.text[len(Header.apiVersionPrefix):]
			apiVersion = max(apiVersion, currentApiVersion)
		elif nameElement.text == Header.version:
			headerVersion = nameElement.tail.strip()

	return "{}.{}".format(apiVersion.replace("_", "."), headerVersion)

def generateJson(templatePath, vulkanApiVersion):
	templatePathWithoutExtension = templatePath[:templatePath.find(".")]

	replacements = {
		"@RELATIVE_LAYER_BINARY@": os.path.basename(templatePathWithoutExtension) + ".dll",
		"@VK_VERSION@"           : vulkanApiVersion
	}

	with(open(templatePath, "r")) as file:
		contents = file.read()

		for pattern, replacement in replacements.items():
			contents = contents.replace(pattern, replacement)

		return contents

	return ""

def writeJson(inputPath, outputPath, contents):
	if not os.path.exists(outputPath):
		os.mkdir(outputPath)

	filename = os.path.splitext(os.path.basename(inputPath))[0]
	outputPath = os.path.join(outputPath, filename)

	with(open(outputPath, "w")) as file:
		file.write(contents)

def getArgument(index):
	if len(sys.argv) <= index:
		raise RuntimeError("argument {} is missing".format(index))

	return sys.argv[index]

if __name__ == "__main__":
	try:
		vulkanRegistryPath = getArgumentFilePath(1, True)
		jsonTemplatePath = getArgumentFilePath(2, True)
		outputPath = getArgumentFilePath(3, False)
	except RuntimeError as error:
		print("Error: {}.".format(error))
		sys.exit()

	vulkanApiVersion = getVulkanApiVersion(vulkanRegistryPath)
	jsonContents = generateJson(jsonTemplatePath, vulkanApiVersion)
	writeJson(jsonTemplatePath, outputPath, jsonContents)
