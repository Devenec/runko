
VK_DEBUG_REPORT_ERROR_BIT_EXT    equ 8       ; The numerical value of the enum value 'VK_DEBUG_REPORT_ERROR_BIT_EXT'
PTR_SIZE                         equ 8       ; The size of a pointer
HASH_SIZE                        equ 32      ; The size of a 'loader_dispatch_hash_entry' struct
HASH_OFFSET_INSTANCE             equ 8128    ; The offset of 'phys_dev_ext_disp_hash' within a 'loader_instance' struct
PHYS_DEV_OFFSET_INST_DISPATCH    equ 616     ; The offset of 'phys_dev_ext' within in 'loader_instance_dispatch_table' struct
PHYS_DEV_OFFSET_PHYS_DEV_TRAMP   equ 16      ; The offset of 'phys_dev' within a 'loader_physical_device_tramp' struct
ICD_TERM_OFFSET_PHYS_DEV_TERM    equ 8       ; The offset of 'this_icd_term' within a 'loader_physical_device_term' struct
PHYS_DEV_OFFSET_PHYS_DEV_TERM    equ 24      ; The offset of 'phys_dev' within a 'loader_physical_device_term' struct
INSTANCE_OFFSET_ICD_TERM         equ 8       ; The offset of 'this_instance' within a 'loader_icd_term' struct
DISPATCH_OFFSET_ICD_TERM         equ 744     ; The offset of 'phys_dev_ext' within a 'loader_icd_term' struct
FUNC_NAME_OFFSET_HASH            equ 0       ; The offset of 'func_name' within a 'loader_dispatch_hash_entry' struct
EXT_OFFSET_DEVICE_DISPATCH       equ 2688    ; The offset of 'ext_dispatch' within a 'loader_dev_dispatch_table' struct
