#
# @file tools/vulkan-header-generator/registryparser.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

from collections import OrderedDict
from functools import cmp_to_key
import os.path
import re
import xml.etree.ElementTree

from generatortypes import Enumeration
from generatortypes import Extension
from generatortypes import Flags
from generatortypes import Function
from generatortypes import FunctionParameter
from generatortypes import Handle
from generatortypes import ParameterAttribute
from generatortypes import Structure
from generatortypes import Symbol
from generatortypes import Type

class Parser:
	class ConstructorInfo:
		def __init__(self, parseDefaultConstructors = "true", isCountParameterIndependent = "false"):
			self.parseDefaultConstructors = parseDefaultConstructors == "true"
			self.isCountParameterIndependent = isCountParameterIndependent == "true"

	cppRegistryFilename    = "vk-cpp.xml"
	vulkanRegistryFilename = "vk.xml"

	instanceCommandTypes = [
		"VkInstance",
		"VkPhysicalDevice"
	]

	specialInstanceCommands = [
		"vkGetDeviceProcAddr"
	]

	suffixes = {
		"flagBits": "FlagBits",
		"flags"   : "Flags"
	}

	def __init__(self):
		self.arrayLengths = {}
		self.data = {}
		self.enabledPlatforms = []
		self.excludes = None
		self.extensionTypes = {}
		self.registryElement = None
		self.structureConstructorInfo = {}
		self.types = {}

	def parse(self, inputPath):
		self.data = {}
		self.parseRegistry(inputPath)
		self.parseCppRegistry()
		self.parseComment()
		self.parseVendorTags()
		self.parseVersions()
		self.parseBuiltInTypes()
		self.parseVulkanTypes()
		self.parseEnumerations()
		self.parseFlags()
		self.parseBaseTypes()
		self.parseHandles()
		self.parseFunctionPointers()
		self.parseConstants()
		self.parseStructures()
		self.parseCommands()
		self.parseEnumerationExtensions()
		self.parseExtensions()
		self.parseStructureTypes()

		return self.data

	# Private

	# Main parsing methods

	def parseRegistry(self, inputPath):
		inputPath = os.path.join(inputPath, self.vulkanRegistryFilename)
		self.registryElement = xml.etree.ElementTree.parse(inputPath).getroot()

	def parseCppRegistry(self):
		rootElement = xml.etree.ElementTree.parse(self.cppRegistryFilename).getroot()
		platformElements = rootElement.findall("platforms/platform")
		self.enabledPlatforms = [""] + [element.attrib["name"] for element in platformElements]
		self.excludes = rootElement.find("exclude")
		self.parseArrayLengths(rootElement)
		self.parseCustomStructureConstructors(rootElement)

	def parseComment(self):
		comment = self.registryElement.find("comment").text.lstrip()
		self.data["comment"] = comment[:comment.find("\n")].rstrip()

	def parseVendorTags(self):
		tags = self.registryElement.findall("tags/tag")
		self.data["vendorTags"] = frozenset(tag.attrib["name"] for tag in tags)

	def parseVersions(self):
		self.data["apiVersions"] = []
		apiVersionPrefix = "VK_API_VERSION_"

		for nameElement in self.registryElement.findall("types/type[@category='define']/name"):
			if nameElement.text.startswith(apiVersionPrefix):
				apiVersion = (nameElement.text[len(apiVersionPrefix):], nameElement.text)
				self.data["apiVersions"].append(apiVersion)
			elif nameElement.text == "VK_HEADER_VERSION":
				self.data["headerVersion"] = nameElement.tail.strip()

	def parseBuiltInTypes(self):
		types = []

		for element in self.registryElement.findall("types/type[@name]"):
			if not "category" in element.attrib:
				types.append(element.attrib["name"])

		for element in self.registryElement.findall("types/type[@category='basetype']"):
			name = element.find("name").text

			if not name.startswith("Vk"):
				types.append(name)

		self.data["builtInTypes"] = types

	def parseVulkanTypes(self):
		self.types = {
			"basetype"   : [],
			"bitmask"    : [],
			"enum"       : [],
			"funcpointer": [],
			"handle"     : [],
			"struct"     : [],
			"union"      : []
		}

		for element in self.registryElement.findall("types/type[@category]"):
			category = element.attrib["category"]

			if not category in self.types:
				continue

			if category != "basetype" or element.find("name").text.startswith("Vk"):
				if category != "enum" or not self.suffixes["flagBits"] in element.attrib["name"]:
					self.types[category].append(element)

	def parseEnumerations(self):
		enumerations = {}

		for element in self.types["enum"]:
			name = element.attrib["name"]
			enumeration = Enumeration(name)
			enumeration.isExtension = self.isExtensionName(name)

			if not "alias" in element.attrib:
				self.parseEnumerationMembers(enumeration, True)

			enumerations[name] = enumeration

		for element in self.registryElement.findall("feature[@api='vulkan']/require/enum[@extends]"):
			name = element.attrib["extends"]

			if any(name in element.attrib for name in ["alias", "bitpos"]):
				continue

			if not name in enumerations:
				raise RuntimeError("enumeration '{}' does not exist to be extended.".format(name))

			enumerations[name].addMember(element.attrib["name"])

		self.data["enumerations"] = self.sortDictionary(enumerations)

	def parseFlags(self):
		flagsDictionary = {}

		for element in self.types["bitmask"]:
			isAlias = "alias" in element.attrib
			flagsName = element.attrib["name"] if isAlias else element.find("name").text
			flagBitsName = self.getFlagBitsNameFromFlagsName(flagsName)
			flags = Flags(flagsName, flagBitsName)
			flags.isExtension = self.isExtensionName(flagBitsName)

			if not isAlias:
				self.parseEnumerationMembers(flags.flagBits, False)

			flagsDictionary[flagBitsName] = flags

		for element in self.registryElement.findall("feature[@api='vulkan']/require/enum[@bitpos][@extends]"):
			name = element.attrib["extends"]

			if "alias" in element.attrib:
				continue

			if not name in flagsDictionary:
				raise RuntimeError("flags enumeration '{}' does not exist to be extended.".format(name))

			flagsDictionary[name].addFlagBit(element.attrib["name"])

		self.data["flags"] = self.sortDictionary(flagsDictionary)

	def parseBaseTypes(self):
		self.data["baseTypes"] = []

		for element in self.types["basetype"]:
			name = element.find("name").text

			if not self.isExcluded("type[@category='basetype'][@name='{}']".format(name)):
				self.data["baseTypes"].append(name)

		self.data["baseTypes"].sort()

	def parseHandles(self):
		self.data["handles"] = {}
		nonDispatchableHandles = {}

		for element in (element for element in self.types["handle"] if not element.find("type") is None):
			name = element.find("name").text

			if element.find("type").text == "VK_DEFINE_HANDLE":
				self.data["handles"][name] = Handle(name)
			else:
				nonDispatchableHandles[name] = Symbol(name)
				nonDispatchableHandles[name].isExtension = self.isExtensionName(name)

		self.data["nonDispatchableHandles"] = self.sortDictionary(nonDispatchableHandles)

	def parseFunctionPointers(self):
		functions = {}

		for element in self.types["funcpointer"]:
			name = element.find("name").text

			if not element.text.startswith("typedef"):
				raise RuntimeError("function pointer '{}' does not start with 'typedef'.".format(name))

			function = Function(name, self.parseFunctionPointerReturnType(element))
			function.isExtension = self.isExtensionName(name)
			self.parseFunctionPointerParameters(element, function)
			functions[name] = function

		self.data["functionPointers"] = self.sortDictionary(functions)

	def parseConstants(self):
		constants = {}

		for element in self.registryElement.findall("enums[@name='API Constants']/enum"):
			name = element.attrib["name"]

			if "alias" in element.attrib or self.isExcluded("enum[@name='{}']".format(name)):
				continue

			constants[name] = Symbol(name, Type.none, element.attrib["value"])
			constants[name].isExtension = self.isExtensionName(name)

		self.data["constants"] = self.sortDictionary(constants)

	def parseStructures(self):
		self.data["structures"] = {}

		for category in ["struct", "union"]:
			for element in (element for element in self.types[category] if not "alias" in element.attrib):
				self.parseStructure(element, category)

		self.data["structures"] = self.sortStructures(self.data["structures"], False)

	def parseCommands(self):
		self.data["commands"] = {}

		for element in self.registryElement.findall("commands/command[proto]"):
			name = element.find("proto/name").text
			returnType = self.parseType(element.find("proto"))
			successCodes = element.get("successcodes", "").split(",")
			command = Function(name, returnType, "VK_INCOMPLETE" in successCodes)
			command.isExtension = self.isExtensionName(name)
			self.parseCommandParameters(element, command)
			self.data["commands"][name] = command

		self.parseHandleCommands()
		self.sortHandles()

	def parseEnumerationExtensions(self):
		matchString = "extensions/extension[@supported='vulkan']/require/enum[@extends]"

		for element in self.registryElement.findall(matchString):
			if "alias" in element.attrib:
				continue

			enumerationName = element.attrib["extends"]
			enumeration = None

			if enumerationName in self.data["enumerations"]:
				enumeration = self.data["enumerations"][enumerationName]
			elif enumerationName in self.data["flags"]:
				enumeration = self.data["flags"][enumerationName].flagBits

			if enumeration is None:
				raise RuntimeError("enumeration '{}' does not exist to be extended.".format(enumerationName))

			if not element.attrib["name"] in enumeration.members:
				enumeration.addMember(element.attrib["name"])

	def parseExtensions(self):
		self.data["extensions"] = OrderedDict()

		self.extensionTypes = {
			"enumerations"    : set(),
			"flags"           : set(),
			"functionPointers": set(),
			"structures"      : set()
		}

		for extensionElement in self.registryElement.findall("extensions/extension[@supported='vulkan']"):
			platform = extensionElement.get("platform", "")

			if platform in self.enabledPlatforms:
				self.parseExtension(extensionElement, platform)

	def parseStructureTypes(self):
		types = self.data["enumerations"]["VkStructureType"].members

		for structure in self.data["structures"].values():
			if not structure.typeEnumeration:
				structure.typeEnumeration = types[0]
			elif not structure.typeEnumeration in types:
				structure.typeEnumeration = ""

	# Other methods

	def parseArrayLengths(self, rootElement):
		self.arrayLengths = {}

		for itemElement in rootElement.findall("arrayLengths/item"):
			items = {}

			for memberElement in itemElement.findall("member"):
				memberName = memberElement.attrib["name"]
				items[memberName] = (memberElement.get("lenMember", ""), memberElement.attrib["len"])

			self.arrayLengths[itemElement.attrib["name"]] = items

	def parseCustomStructureConstructors(self, rootElement):
		self.data["customConstructors"] = {}
		self.structureConstructorInfo = {}

		for typeElement in rootElement.findall("constructors/type"):
			structureName = typeElement.attrib["name"]
			self.structureConstructorInfo[structureName] = self.ConstructorInfo(
				typeElement.get("defaultConstructors", "true"),
				typeElement.get("independentCountParameter", "false"))

			constructorElements = typeElement.findall("constructor")
			constructors = []

			for constructorElement in constructorElements:
				parameters = []

				for parameterElement in constructorElement.findall("param"):
					symbol = Symbol(parameterElement.find("name").text, self.parseType(parameterElement))
					initialiserElement = parameterElement.find("initialiser")
					initialiser = "" if initialiserElement is None else initialiserElement.text
					parameters.append(FunctionParameter(symbol, initialiser))

				constructors.append(parameters)

			self.data["customConstructors"][structureName] = constructors

	def isExtensionName(self, name):
		return any(name.endswith(tag) for tag in self.data["vendorTags"])

	def parseEnumerationMembers(self, enumeration, failIfDefinitionNotFound):
		definitionElement = self.registryElement.find("enums[@name='{}']".format(enumeration.name))

		if definitionElement is None:
			if failIfDefinitionNotFound:
				raise RuntimeError("definition for enumeration '{}' was not found.".format(enumeration.name))

			return

		for memberElement in definitionElement.findall("enum"):
			if not "alias" in memberElement.attrib:
				enumeration.addMember(memberElement.attrib["name"])

	def isExcluded(self, matchString):
		return not self.excludes.find(matchString) is None

	def parseStructure(self, structureElement, category):
		name = structureElement.attrib["name"]
		parseConstructors = self.shouldParseStructureConstructors(structureElement, name)
		structure = Structure(category, name, parseConstructors)
		structure.isExtension = self.isExtensionName(name)
		self.parseStructureMembers(structureElement, structure)
		self.data["structures"][name] = structure

	def parseCommandParameters(self, commandElement, command):
		parameters = commandElement.findall("param")
		commandName = command.symbol.name

		for i, parameterElement in enumerate(parameters, 1):
			type = self.parseType(parameterElement, True, False, commandName)
			symbol = Symbol(parameterElement.find("name").text, type)
			isLast = i == len(parameters)
			attributes = self.parseCommandParameterAttributes(parameterElement, type, commandName, isLast)
			countInfo = None

			if ParameterAttribute.ArrayPointer in attributes:
				countInfo = self.parseCountInfo(parameterElement, commandName, command.parameters,
					ParameterAttribute.Output in attributes)

			parameter = FunctionParameter(symbol, attributes = attributes, countInfo = countInfo)
			command.addParameter(parameter)

	def parseHandleCommands(self):
		for key, command in self.data["commands"].items():
			handleName = self.parseCommandHandleName(command)
			isInstanceCommand = handleName in self.instanceCommandTypes or key in self.specialInstanceCommands
			command.category = "instance" if isInstanceCommand else "device"
			self.data["handles"][handleName].addCommand(key)

		for handle in self.data["handles"].values():
			handle.commands.sort()

	def sortHandles(self):
		dependencies = dict((key, set()) for key in self.data["handles"])

		for key, handle in self.data["handles"].items():
			for commandName in handle.commands:
				for parameter in self.data["commands"][commandName].parameters:
					if parameter.symbol.type.name in self.data["handles"]:
						dependencies[key].add(parameter.symbol.type.name)

		def compareHandleDependencies(dependencyA, dependencyB):
			if dependencyA[0] in dependencyB[1]:
				return -1

			if dependencyB[0] in dependencyA[1]:
				return 1

			return -1 if dependencyA[0] < dependencyB[0] else 1

		sortedDependencies = sorted(dependencies.items(), key = cmp_to_key(compareHandleDependencies))
		self.data["handles"] = OrderedDict((dependency[0], self.data["handles"][dependency[0]]) for
			dependency in sortedDependencies)

	def parseExtension(self, extensionElement, platform):
		requiredElements = extensionElement.findall("require")
		requiredElements = [element for element in requiredElements if not "extension" in element.attrib]
		name = extensionElement.attrib["name"]
		extension = Extension(name, platform)
		self.parseExtensionHandles(extension, requiredElements)
		self.parseExtensionConstants(extension, requiredElements)
		self.parseExtensionStructuresAndTypes(extension, requiredElements)
		self.parseExtensionCommandsAndTypes(extension, requiredElements, extensionElement.attrib["type"])
		self.sortExtensionTypes(extension)
		self.data["extensions"][name] = extension

	def parseExtensionHandles(self, extension, requiredElements):
		format = "enum[@extends='VkObjectType']"

		for requiredElement in requiredElements:
			for enumerationElement in requiredElement.findall(format):
				if "alias" in enumerationElement.attrib:
					continue

				nameParts = enumerationElement.attrib["name"].split("_")
				vendorTag = nameParts.pop() if nameParts[-1] in self.data["vendorTags"] else ""
				name = "".join(part.title() for part in nameParts[0:1] + nameParts[3:]) + vendorTag

				if not name in self.data["nonDispatchableHandles"]:
					raise RuntimeError("undefined extension non-dispatchable handle '{}'.".format(name))

				extension.nonDispatchableHandles.append(name)

		extension.nonDispatchableHandles.sort()

	def parseExtensionConstants(self, extension, requiredElements):
		for requiredElement in requiredElements:
			for enumerationElement in requiredElement.findall("enum"):
				name = enumerationElement.attrib["name"]

				if name in self.data["constants"]:
					extension.constants.append(name)

		extension.constants.sort()

	def parseExtensionStructuresAndTypes(self, extension, requiredElements):
		for requiredElement in requiredElements:
			for typeElement in requiredElement.findall("type"):
				name = typeElement.attrib["name"]

				if name in self.data["structures"] and not name in self.extensionTypes["structures"]:
					structure = self.data["structures"][name]
					extension.structures[name] = structure
					self.extensionTypes["structures"].add(name)
					self.parseExtensionTypesFromMembers(extension, structure.members)

	def parseExtensionCommandsAndTypes(self, extension, requiredElements, extensionType):
		for requiredElement in requiredElements:
			for commandElement in requiredElement.findall("command"):
				commandName = commandElement.attrib["name"]

				if commandName in self.data["commands"]:
					command = self.data["commands"][commandName]

					if extensionType == "instance":
						command.category = extensionType

					handleName = self.parseCommandHandleName(command)

					if not handleName in extension.handles:
						extension.handles[handleName] = []

					extension.handles[handleName].append(commandName)
					self.parseExtensionTypesFromMembers(extension, command.parameters)

	def shouldParseStructureConstructors(self, structureElement, structureName):
		parseDefaultConstructors = self.getStructureConstructorInfo(structureName).parseDefaultConstructors
		return parseDefaultConstructors and structureElement.get("returnedonly", "false") == "false"

	def parseStructureMembers(self, structureElement, structure):
		for memberElement in structureElement.findall("member"):
			memberType = self.parseType(memberElement, True, True, structure.name)

			if memberType.name == "VkStructureType":
				structure.typeEnumeration = memberElement.get("values", "")

			symbol = Symbol(memberElement.find("name").text, memberType)
			attributes = self.parseStructureMemberAttributes(memberElement, memberType, structure.name)
			countInfo = None

			if ParameterAttribute.ArrayPointer in attributes:
				if not self.getStructureConstructorInfo(structure.name).isCountParameterIndependent:
					countInfo = self.parseCountInfo(memberElement, structure.name, structure.members,
						ParameterAttribute.Output in attributes)

			member = Structure.Member(symbol, attributes, countInfo)
			structure.addMember(member)

	def parseCommandParameterAttributes(self, parameterElement, parameterType, commandName, isLast):
		attributes = self.parseElementAttributes(parameterElement, commandName)

		if parameterType.isPointer():
			if isLast and not parameterType.isConstant():
				attributes |= ParameterAttribute.Output

			if not ParameterAttribute.ArrayPointer in attributes or ParameterAttribute.Output in attributes:
				if parameterElement.get("optional", "false") == "true":
					attributes |= ParameterAttribute.Optional

		return attributes

	def parseCountInfo(self, element, parentName, members, elementIsOutput):
		countInfo = self.parseElementLengthAttribute(element, parentName)
		countMember = next((member for member in members if member.symbol.name == countInfo[0]), None)

		if not countMember is None:
			countMember.attributes |= ParameterAttribute.ArraySize

			if elementIsOutput and "*" in countMember.symbol.type.suffix:
				countMember.attributes |= ParameterAttribute.InOutSize

		return countInfo

	def parseCommandHandleName(self, command):
		handleName = command.parameters[0].symbol.type.name
		return handleName if handleName in self.data["handles"] else "VkInstance"

	def parseExtensionTypesFromMembers(self, extension, members):
		for member in members:
			self.addExtensionType(extension, member.symbol.type.name)

	def getStructureConstructorInfo(self, structureName):
		infoExists = structureName in self.structureConstructorInfo
		return self.structureConstructorInfo[structureName] if infoExists else self.ConstructorInfo()

	def parseStructureMemberAttributes(self, memberElement, memberType, parentName):
		attributes = self.parseElementAttributes(memberElement, parentName)
		isConstantPointer = memberType.isConstant() and memberType.isPointer()

		if isConstantPointer and not ParameterAttribute.ArrayPointer in attributes:
			if memberElement.get("optional", "false") == "true":
				attributes |= ParameterAttribute.Optional

		return attributes

	def parseElementAttributes(self, element, parentName):
		attributes = ParameterAttribute.Nothing
		length = self.parseElementLengthAttribute(element, parentName)

		if not length is None:
			if length[0] == "null-terminated":
				attributes |= ParameterAttribute.String
			else:
				attributes |= ParameterAttribute.ArrayPointer

		return attributes

	def parseElementLengthAttribute(self, element, parentName):
		length = element.get("len", "")

		if not length:
			return None

		if parentName in self.arrayLengths:
			elementName = element.find("name").text

			if elementName in self.arrayLengths[parentName]:
				return self.arrayLengths[parentName][elementName]

		return length, "{}"

	def addExtensionType(self, extension, typeName):
		if self.suffixes["flags"] in typeName:
			typeName = self.getFlagBitsNameFromFlagsName(typeName)

		if typeName in self.data["enumerations"]:
			self.addExtensionTypeToSet(extension.enumerations, "enumerations", typeName)
		elif typeName in self.data["flags"]:
			self.addExtensionTypeToSet(extension.flags, "flags", typeName)
		elif typeName in self.data["functionPointers"]:
			self.addExtensionTypeToSet(extension.functionPointers, "functionPointers", typeName)
		elif typeName in self.data["structures"]:
			if self.shouldAddExtensionType("structures", typeName):
				structure = self.data["structures"][typeName]
				extension.structures[typeName] = structure
				self.extensionTypes["structures"].add(typeName)
				self.parseExtensionTypesFromMembers(extension, structure.members)

	def addExtensionTypeToSet(self, extensionSet, category, typeName):
		if self.shouldAddExtensionType(category, typeName):
			extensionSet.add(typeName)
			self.extensionTypes[category].add(typeName)

	def shouldAddExtensionType(self, category, typeName):
		return self.data[category][typeName].isExtension and not typeName in self.extensionTypes[category]

	# Static

	@staticmethod
	def sortDictionary(dictionary):
		return OrderedDict(sorted(dictionary.items()))

	@staticmethod
	def getFlagBitsNameFromFlagsName(flagsName):
		flagsSuffix = Parser.suffixes["flags"]
		suffixIndex = flagsName.rfind(flagsSuffix)
		flagBitsSuffix = flagsName[suffixIndex:].replace(flagsSuffix, Parser.suffixes["flagBits"], 1)

		return flagsName[:suffixIndex] + flagBitsSuffix

	@staticmethod
	def parseFunctionPointerReturnType(functionElement):
		bracketPosition = functionElement.text.find("(")
		returnSignature = functionElement.text[len("typedef"):bracketPosition].strip()
		match = re.search(r"\w+", returnSignature, re.ASCII)

		return Type(returnSignature[match.start():match.end()], "", returnSignature[match.end():])

	@staticmethod
	def parseFunctionPointerParameters(functionElement, function):
		typePrefix = functionElement.find("name").tail
		typePrefix = typePrefix[typePrefix.find("(") + 1:].lstrip()

		for typeElement in functionElement.findall("type"):
			match = re.search(r"\w+[),]", typeElement.tail, re.ASCII)
			parameterName = typeElement.tail[match.start() : match.end() - 1]
			typeSuffix = typeElement.tail[:match.start()].rstrip()
			type = Type(typeElement.text, typePrefix, typeSuffix)
			function.addParameter(Symbol(parameterName, type))
			typePrefix = typeElement.tail[match.end():].lstrip()

	@staticmethod
	def sortStructures(structures, sortExtensions):
		keys = sorted(structures.keys())

		while True:
			i = 0
			orderChanged = False

			while i < len(keys):
				for j in range(0, i):
					if not sortExtensions and structures[keys[j]].isExtension:
						continue

					if any(member.symbol.type.name == keys[i] for member in structures[keys[j]].members):
						keys.insert(j, keys.pop(i))
						orderChanged = True
						break

				i += 1

			if not orderChanged:
				break

		return OrderedDict((key, structures[key]) for key in keys)

	@staticmethod
	def parseType(parentElement, parseArraySize = False, parseBitSize = False, warningName = ""):
		typeElement = parentElement.find("type")
		prefix = "" if parentElement.text is None else parentElement.text.lstrip()
		suffix = "" if typeElement.tail is None else typeElement.tail.rstrip()
		arraySize = Parser.parseTypeArraySize(parentElement, warningName) if parseArraySize else None
		bitSize = Parser.parseTypeBitSize(parentElement) if parseBitSize else 0

		return Type(typeElement.text, prefix, suffix, arraySize, bitSize)

	@staticmethod
	def sortExtensionTypes(extension):
		extension.enumerations = sorted(extension.enumerations)
		extension.flags = sorted(extension.flags)
		extension.functionPointers = sorted(extension.functionPointers)

		for key in extension.handles:
			extension.handles[key] = sorted(extension.handles[key])

		structures = Parser.sortStructures(extension.structures, True)
		extension.structures = list(structures.keys())

	@staticmethod
	def parseTypeArraySize(parentElement, warningName):
		for commentElement in parentElement.findall("comment"):
			parentElement.remove(commentElement)

		memberText = "".join(parentElement.itertext())
		indexBegin = memberText.find("[")
		sizes = []

		while(indexBegin != -1):
			indexEnd = memberText.find("]", indexBegin + 1)
			sizes.append(memberText[indexBegin + 1 : indexEnd])
			indexBegin = memberText.find("[", indexEnd + 1)

		return tuple(sizes) if sizes else None

	@staticmethod
	def parseTypeBitSize(parentElement):
		nameTail = parentElement.find("name").tail
		return 0 if nameTail is None or not nameTail.startswith(":") else int(nameTail[1:])
