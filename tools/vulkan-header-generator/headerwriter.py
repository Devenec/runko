#
# @file tools/vulkan-header-generator/headerwriter.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

import os.path

from generatortypes import Symbol
from generatortypes import SymbolFormatter
from generatortypes import Type
from registrytranslator import Translator

class Header:
	apiVersion = "constexpr uint32_t apiVersion_{0[0]} = {0[1]};"

	assertionHeaderVersion = (
		"static_assert(VK_HEADER_VERSION >= {}, \"The Vulkan header is of incorrect version.\");")

	assertionTypeAlignment = (
		"static_assert(alignof({0}) == alignof({1}), \"The alignment of {0} is incorrect.\");")

	assertionTypeSize = "static_assert(sizeof({0}) == sizeof({1}), \"The size of {0} is incorrect.\");"

	deviceDispatchStructure = (
		"\tstruct DeviceDispatch\n"
		"\t{{\n"
		"{}\n\n"
		"\t\tvoid initialise(const Instance instance, const InstanceDispatch& instanceDispatch,"
			" const Device device)\n"
		"\t\t{{\n"
		"\t\t\tRUNKO_ASSERT(device != Device());\n\n"
		"{}\n\n"
		"{}\n"
		"\t\t}}\n"
		"\t}};\n")

	includes = [
		"<algorithm>",
		"<array>",
		"<utility>",
		"<vector>",
		"",
		"<core/Assert.h>",
		"<core/Types.h>",
		"<gsl/span>",
		"<vulkan/vulkan.h>"
	]

	instanceDispatchStructure = (
		"\tstruct InstanceDispatch\n"
		"\t{{\n"
		"{}\n\n"
		"\t\tInstanceDispatch() :\n"
		"\t\t\tgetInstanceProcAddr(vkGetInstanceProcAddr)\n"
		"\t\t{{\n"
		"\t\t\tInstance instance;\n"
		"{}\n"
		"\t\t}}\n\n"
		"\t\tvoid initialise(const Instance instance)\n"
		"\t\t{{\n"
		"\t\t\tRUNKO_ASSERT(instance != Instance());\n\n"
		"{}\n"
		"\t\t}}\n"
		"\t}};\n")

	dispatchDeclarations = (
		"\tstruct DeviceDispatch;\n"
		"\tstruct InstanceDispatch;\n")

	fileComment = (
		"/**\n"
		" * @file graphics/vulkan/{}\n"
		" *\n"
		" * Runko\n"
		" * Copyright 2021 Eetu 'Devenec' Oinasmaa\n"
		" *\n"
		" * Permission is hereby granted, free of charge, to any person obtaining a copy\n"
		" * of this software and associated documentation files (the \"Software\"), to deal\n"
		" * in the Software without restriction, including without limitation the rights\n"
		" * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n"
		" * copies of the Software, and to permit persons to whom the Software is\n"
		" * furnished to do so, subject to the following conditions:\n"
		" *\n"
		" * The above copyright notice and this permission notice shall be included in\n"
		" * all copies or substantial portions of the Software.\n"
		" *\n"
		" *\n"
		" * {}\n"
		" */\n")

	filename = "Vulkan.h"

	flagsClass = (
		"\ttemplate<typename T>\n"
		"\tclass Flags\n"
		"\t{\n"
		"\tpublic:\n\n"
		"\t\tconstexpr Flags() = default;\n\n"
		"\t\tconstexpr Flags(const T bit) :\n"
		"\t\t\t_mask(static_cast<VkFlags>(bit)) { }\n\n"
		"\t\tconstexpr Flags(const Flags& flags) = default;\n"
		"\t\tconstexpr Flags(Flags&& flags) = default;\n\n"
		"\t\t~Flags() = default;\n\n"
		"\t\tconstexpr Bool any(const Flags flags) const\n"
		"\t\t{\n"
		"\t\t\treturn (*this & flags) != Flags();\n"
		"\t\t}\n\n"
		"\t\tconstexpr Bool in(const Flags flags) const\n"
		"\t\t{\n"
		"\t\t\treturn (*this & flags) == flags;\n"
		"\t\t}\n\n"
		"\t\tconstexpr Bool in(const T bit) const\n"
		"\t\t{\n"
		"\t\t\treturn (*this & bit) == bit;\n"
		"\t\t}\n\n"
		"\t\tconstexpr VkFlags " + Translator.classGetterNames["flags"] + " const\n"
		"\t\t{\n"
		"\t\t\treturn _mask;\n"
		"\t\t}\n\n"
		"\t\tconstexpr Flags& operator =(const Flags& flags) = default;\n"
		"\t\tconstexpr Flags& operator =(Flags&& flags) = default;\n\n"
		"\t\tconstexpr Flags& operator &=(const Flags flags)\n"
		"\t\t{\n"
		"\t\t\t_mask &= flags._mask;\n"
		"\t\t\treturn *this;\n"
		"\t\t}\n\n"
		"\t\tconstexpr Flags operator ~() const\n"
		"\t\t{\n"
		"\t\t\tFlags flags;\n"
		"\t\t\tflags._mask = ~_mask;\n\n"
		"\t\t\treturn flags;\n"
		"\t\t}\n\n"
		"\t\tconstexpr Flags& operator |=(const Flags flags)\n"
		"\t\t{\n"
		"\t\t\t_mask |= flags._mask;\n"
		"\t\t\treturn *this;\n"
		"\t\t}\n\n"
		"\t\tconstexpr Flags& operator ^=(const Flags flags)\n"
		"\t\t{\n"
		"\t\t\t_mask ^= flags._mask;\n"
		"\t\t\treturn *this;\n"
		"\t\t}\n\n"
		"\tprivate:\n\n"
		"\t\tVkFlags _mask = 0;\n"
		"\t};\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator &(const Flags<T> flagsA, const Flags<T> flagsB)\n"
		"\t{\n"
		"\t\tFlags<T> result = flagsA;\n"
		"\t\treturn result &= flagsB;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator &(const Flags<T> flags, const T bit)\n"
		"\t{\n"
		"\t\tFlags<T> result = flags;\n"
		"\t\treturn result &= bit;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator &(const T bitA, const T bitB)\n"
		"\t{\n"
		"\t\tFlags<T> result = bitA;\n"
		"\t\treturn result &= bitB;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator ~(const T bit)\n"
		"\t{\n"
		"\t\treturn ~Flags<T>(bit);\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator |(const Flags<T> flagsA, const Flags<T> flagsB)\n"
		"\t{\n"
		"\t\tFlags<T> result = flagsA;\n"
		"\t\treturn result |= flagsB;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator |(const Flags<T> flags, const T bit)\n"
		"\t{\n"
		"\t\tFlags<T> result = flags;\n"
		"\t\treturn result |= bit;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator |(const T bitA, const T bitB)\n"
		"\t{\n"
		"\t\tFlags<T> result = bitA;\n"
		"\t\treturn result |= bitB;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator ^(const Flags<T> flagsA, const Flags<T> flagsB)\n"
		"\t{\n"
		"\t\tFlags<T> result = flagsA;\n"
		"\t\treturn result ^= flagsB;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator ^(const Flags<T> flags, const T bit)\n"
		"\t{\n"
		"\t\tFlags<T> result = flags;\n"
		"\t\treturn result ^= bit;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Flags<T> operator ^(const T bitA, const T bitB)\n"
		"\t{\n"
		"\t\tFlags<T> result = bitA;\n"
		"\t\treturn result ^= bitB;\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Bool operator ==(const Flags<T> flagsA, const Flags<T> flagsB)\n"
		"\t{\n"
		"\t\treturn flagsA.mask() == flagsB.mask();\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Bool operator ==(const Flags<T> flags, const T bit)\n"
		"\t{\n"
		"\t\treturn flags == Flags<T>(bit);\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Bool operator !=(const Flags<T> flagsA, const Flags<T> flagsB)\n"
		"\t{\n"
		"\t\treturn !(flagsA == flagsB);\n"
		"\t}\n\n"
		"\ttemplate<typename T>\n"
		"\tconstexpr Bool operator !=(const Flags<T> flags, const T bit)\n"
		"\t{\n"
		"\t\treturn !(flags == bit);\n"
		"\t}\n")

	handleClass = (
		"\tclass {0.name}\n"
		"\t{{\n"
		"\tpublic:\n\n"
		"\t\tstatic constexpr " + Translator.types["objectType"] + " cObjectType = {1};\n\n"
		"\t\t{0.name}() = default;\n\n"
		"\t\t{0.name}(const {0.name}& object) = default;\n"
		"\t\t{0.name}({0.name}&& object) = default;\n\n"
		"\t\t~{0.name}() = default;\n\n"
		"{2}\n\n"
		"\t\t{0.value} handle() const\n"
		"\t\t{{\n"
		"\t\t\treturn _handle;\n"
		"\t\t}}\n\n"
		"\t\t{0.name}& operator =(const {0.name}& object) = default;\n"
		"\t\t{0.name}& operator =({0.name}&& object) = default;\n\n"
		"\t\tBool operator ==(const {0.name} object) const\n"
		"\t\t{{\n"
		"\t\t\treturn " + Translator.classGetterNames["handle"] + " == object." +
			Translator.classGetterNames["handle"] + ";\n"
		"\t\t}}\n\n"
		"\t\tBool operator !=(const {0.name} object) const\n"
		"\t\t{{\n"
		"\t\t\treturn !(*this == object);\n"
		"\t\t}}\n\n"
		"\tprivate:\n\n"
		"\t\t{0.value} " + Translator.classGetterNames["handle"] + " = nullptr;\n"
		"\t}};")

	macros = (
		"#pragma once\n\n"
		"{}\n\n"
		"#if defined(RUNKO_VULKAN_PLATFORM)\n\n"
		"{}\n\n"
		"#endif\n"
	)

	namespace = "Vk"

	nonDispatchableHandleClass = (
		"\ttemplate<typename T, ObjectType Type>\n"
		"\tclass Handle\n"
		"\t{\n"
		"\tpublic:\n\n"
		"\t\tstatic constexpr " + Translator.types["objectType"] + " cObjectType = Type;\n\n"
		"\t\tHandle() = default;\n\n"
		"\t\tHandle(const Handle& handle) = default;\n"
		"\t\tHandle(Handle&& handle) = default;\n\n"
		"\t\t~Handle() = default;\n\n"
		"\t\tT " + Translator.classGetterNames["nonDispatchableHandle"] + " const\n"
		"\t\t{\n"
		"\t\t\treturn _handle;\n"
		"\t\t}\n\n"
		"\t\tHandle& operator =(const Handle& handle) = default;\n"
		"\t\tHandle& operator =(Handle&& handle) = default;\n\n"
		"\tprivate:\n\n"
		"\t\tT _handle = VK_NULL_HANDLE;\n"
		"\t};\n\n"
		"\ttemplate<typename T, ObjectType Type>\n"
		"\tBool operator ==(const Handle<T, Type> handleA, const Handle<T, Type> handleB)\n"
		"\t{\n"
		"\t\treturn handleA.handle() == handleB.handle();\n"
		"\t}\n\n"
		"\ttemplate<typename T, ObjectType Type>\n"
		"\tBool operator !=(const Handle<T, Type> handleA, const Handle<T, Type> handleB)\n"
		"\t{\n"
		"\t\treturn !(handleA == handleB);\n"
		"\t}\n")

	optionalClass = (
		"\ttemplate<typename T>\n"
		"\tclass Optional\n"
		"\t{\n"
		"\tpublic:\n\n"
		"\t\tconstexpr Optional() = default;\n\n"
		"\t\tconstexpr Optional(const T& value) :\n"
		"\t\t\t_value(&value) { }\n\n"
		"\t\tconstexpr Optional(const Optional& optional) = default;\n"
		"\t\tconstexpr Optional(Optional&& optional) = default;\n\n"
		"\t\t~Optional() = default;\n\n"
		"\t\tconstexpr const T* " + Translator.classGetterNames["optional"] + " const\n"
		"\t\t{\n"
		"\t\t\treturn _value;\n"
		"\t\t}\n\n"
		"\t\tconstexpr Optional& operator =(const Optional& optional) = default;\n"
		"\t\tconstexpr Optional& operator =(Optional&& optional) = default;\n\n"
		"\tprivate:\n\n"
		"\t\tconst T* _value = nullptr;\n"
		"\t};\n")

	platformMacros = (
		"\t#if defined({0[condition]})\n\n"
		"\t\t{1}\n\n"
		"\t\t#define {0[definition]}\n\n"
		"\t#endif"
	)

	platforms = {
		"win32": {
			"condition": "RUNKO_PLATFORM_SYSTEM_WINDOWS",
			"definition": "RUNKO_VULKAN_PLATFORM_WINDOWS",

			"includes" : [
				"<core/platform/windows/WindowsForward.h>",
				"<vulkan/vulkan_win32.h>"
			]
		}
	}

	resultPairStructure = (
		"\ttemplate<typename T>\n"
		"\tstruct ResultPair\n"
		"\t{\n"
		"\tpublic:\n\n"
		"\t\tT value;\n"
		"\t\tResult result;\n\n"
		"\t\tResultPair(const Result result, T&& value) :\n"
		"\t\t\tvalue(std::move(value)),\n"
		"\t\t\tresult(result) { }\n"
		"\t};\n")

	utilities = (
		"\t{}\n\n"
		"\tconstexpr uint32_t makeVersion(const uint32_t majorNumber, const uint32_t minorNumber,\n"
		"\t\tconst uint32_t patchNumber)\n"
		"\t{{\n"
		"\t\treturn VK_MAKE_VERSION(majorNumber, minorNumber, patchNumber);\n"
		"\t}}\n\n"
		"\tconstexpr uint32_t versionMajor(const uint32_t versionNumber)\n"
		"\t{{\n"
		"\t\treturn VK_VERSION_MAJOR(versionNumber);\n"
		"\t}}\n\n"
		"\tconstexpr uint32_t versionMinor(const uint32_t versionNumber)\n"
		"\t{{\n"
		"\t\treturn VK_VERSION_MINOR(versionNumber);\n"
		"\t}}\n\n"
		"\tconstexpr uint32_t versionPatch(const uint32_t versionNumber)\n"
		"\t{{\n"
		"\t\treturn VK_VERSION_PATCH(versionNumber);\n"
		"\t}}\n")

class Writer:
	def __init__(self):
		self.data = None
		self.file = None

	def write(self, data, outputPath):
		self.data = data
		self.writeMainFile(outputPath)

	# Private

	# Main writing methods

	def writeMainFile(self, outputPath):
		outputPath = os.path.join(outputPath, Header.filename)
		self.file = open(outputPath, "w")

		self.writeFileComment()
		self.writeMacros()
		self.file.write("\nnamespace {}\n{{\n".format(Header.namespace))
		self.writeHeaderVersionCheck()
		self.writeUtilities()
		self.writeEnumerations()
		self.writeFlags()
		self.writeBaseTypes()
		self.writeNonDispatchableHandles()
		self.writeFunctionPointers()
		self.writeConstants()
		self.writeStructureDeclarations()
		self.writeExtensions()
		self.writeHandles()
		self.writeStructureDefinitions()
		self.writeCommands()
		self.file.write("}\n")

		self.file.close()

	def writeFileComment(self):
		additionalComment = "\n * ".join(self.data["comment"])
		self.file.write(Header.fileComment.format(Header.filename, additionalComment))

	def writeMacros(self):
		includes = self.formatIncludes(Header.includes, "\n")
		self.file.write("\n" + Header.macros.format(includes, self.formatPlatformMacros()))

	def writeHeaderVersionCheck(self):
		assertion = Header.assertionHeaderVersion.format(self.data["headerVersion"])
		self.file.write("\t{}\n\n".format(assertion))

	def writeUtilities(self):
		apiVersions = []

		for apiVersion in self.data["apiVersions"]:
			apiVersions.append(Header.apiVersion.format(apiVersion))

		self.file.write(Header.utilities.format("\n\t".join(apiVersions)))

	def writeEnumerations(self):
		enumerations = (value for value in self.data["enumerations"].values() if not value.isExtension)
		formattedEnumerations = []

		for enumeration in enumerations:
			formattedEnumerations.append(self.formatEnumeration(enumeration))

		self.file.write("\n\t// Enumerations\n\n{}\n".format("\n\n".join(formattedEnumerations)))

	def writeFlags(self):
		flags = (value for value in self.data["flags"].values() if not value.isExtension)
		formattedFlags = []

		for flagsItem in flags:
			formattedFlags.append(self.formatFlags(flagsItem))

		self.file.write("\n\t// Flags\n\n{}\n{}\n".format(Header.flagsClass, "\n\n".join(formattedFlags)))

	def writeBaseTypes(self):
		baseTypes = []
		width = SymbolFormatter.calculateAlignmentWidth(self.data["baseTypes"].values())

		for baseType in self.data["baseTypes"].values():
			baseTypes.append("\tusing {};".format(SymbolFormatter.formatAssignment(baseType, width)))

		self.file.write("\n\t// Base types\n\n{}\n".format("\n".join(baseTypes)))

	def writeNonDispatchableHandles(self):
		handles = [value for value in self.data["nonDispatchableHandles"].values() if not value.isExtension]
		formattedHandles = self.formatHandles(handles)
		self.file.write("\n\t// Non-dispatchable handles\n\n{}\n{}\n".format(
			Header.nonDispatchableHandleClass, formattedHandles))

	def writeFunctionPointers(self):
		functions = [value for value in self.data["functionPointers"].values() if not value.isExtension]
		formattedFunctions = self.formatFunctionPointers(functions)
		self.file.write("\n\t// Function pointers\n\n{}\n".format(formattedFunctions))

	def writeConstants(self):
		constants = [value for value in self.data["constants"].values() if not value.isExtension]
		formattedConstants = self.formatConstants(constants)
		self.file.write("\n\t// Constants\n\n{}\n".format(formattedConstants))

	def writeStructureDeclarations(self):
		format = "\t{0.category} {0.name};"
		structures = [structure for key, structure in sorted(self.data["structures"].items())]
		coreStructures = [format.format(value) for value in structures if not value.isExtension]
		extensionStructures = []

		for extension in self.data["extensions"].values():
			for structureName in extension.structures:
				extensionStructures.append(self.data["structures"][structureName])

		extensionStructures.sort(key = lambda structure: structure.name)
		formattedStructures = coreStructures + [""] + [format.format(structure) for structure in
			extensionStructures]

		self.file.write("\n\t// Structure declarations\n\n{}\n".format("\n".join(formattedStructures)))

	def writeExtensions(self):
		extensions = []

		for extension in self.data["extensions"].values():
			extensionLists = [extension.constants, extension.enumerations, extension.flags,
				extension.functionPointers, extension.nonDispatchableHandles]

			if not any(list for list in extensionLists if list):
				continue

			items = []

			for name in extension.enumerations:
				items.append(self.formatEnumeration(self.data["enumerations"][name]))

			for name in extension.flags:
				items.append(self.formatFlags(self.data["flags"][name]))

			if extension.nonDispatchableHandles:
				handles = [self.data["nonDispatchableHandles"][name] for name in
					extension.nonDispatchableHandles]

				items.append(self.formatHandles(handles))

			if extension.functionPointers:
				functions = [self.data["functionPointers"][name] for name in extension.functionPointers]
				items.append(self.formatFunctionPointers(functions))

			if extension.constants:
				constants = [self.data["constants"][name] for name in extension.constants]
				items.append(self.formatConstants(constants))

			formattedExtension = "\t// {}\n\n{}".format(extension.name, "\n\n".join(items))

			if extension.platform:
				formattedExtension = self.wrapExtensionWithPlatformGuard(extension.platform,
					formattedExtension)

			extensions.append(formattedExtension)

		self.file.write("\n\t// Extensions\n\n{}\n".format("\n\n".join(extensions)))

	def writeHandles(self):
		handles = []

		for key, handle in self.data["handles"].items():
			commands = self.formatCommands(key, handle)
			handles.append(Header.handleClass.format(handle.symbol, handle.objectType, "\n\n".join(commands)))

		self.file.write("\n\t// Handles\n\n{}\n{}\n{}\n{}\n".format(Header.optionalClass,
			Header.resultPairStructure, Header.dispatchDeclarations, "\n\n".join(handles)))

	def writeStructureDefinitions(self):
		structures = ((name, value) for name, value in self.data["structures"].items() if
			not value.isExtension)

		formattedStructures = []

		for structureName, structure in structures:
			formattedStructures.append(self.formatStructureDefinition(structureName, structure))

		for extension in self.data["extensions"].values():
			if not extension.structures:
				continue

			extensionStructures = ["\t// " + extension.name]

			for structureName in extension.structures:
				extensionStructures.append(self.formatStructureDefinition(structureName,
					self.data["structures"][structureName]))

			formattedExtensionStructures = "\n\n".join(extensionStructures)

			if extension.platform:
				formattedExtensionStructures = self.wrapExtensionWithPlatformGuard(extension.platform,
					formattedExtensionStructures)

			formattedStructures.append(formattedExtensionStructures)

		self.file.write("\n\t// Structure definitions\n\n{}\n".format("\n\n".join(formattedStructures)))

	def writeCommands(self):
		deviceDispatchStructure, instanceDispatchStructure = self.formatDispatchStructures()
		formattedCommands = []

		for key, handle in self.data["handles"].items():
			commands = (self.data["commands"][commandName] for commandName in handle.commands if
				not self.data["commands"][commandName].isExtension)

			for command in commands:
				formattedCommands.append(self.formatCommandDefinition(handle, command))

			for extension in self.data["extensions"].values():
				if not key in extension.handles:
					continue

				extensionCommands = ["\t// " + extension.name]

				for commandName in extension.handles[key]:
					extensionCommands.append(self.formatCommandDefinition(handle,
						self.data["commands"][commandName]))

				formattedExtensionCommands = "\n\n".join(extensionCommands)

				if extension.platform:
					formattedExtensionCommands = self.wrapExtensionWithPlatformGuard(extension.platform,
						formattedExtensionCommands)

				formattedCommands.append(formattedExtensionCommands)

		self.file.write("\n\t// Commands\n\n{}\n{}\n{}\n".format(deviceDispatchStructure,
			instanceDispatchStructure, "\n\n".join(formattedCommands)))

	# Other methods

	def formatCommands(self, handleKey, handle):
		commands = (self.data["commands"][commandName] for commandName in handle.commands if
			not self.data["commands"][commandName].isExtension)

		formattedCommands = [self.formatCommandDeclaration(command) for command in commands]

		for extension in self.data["extensions"].values():
			if not handleKey in extension.handles:
				continue

			extensionCommands = ["\t\t// " + extension.name]

			for commandName in extension.handles[handleKey]:
				extensionCommands.append(self.formatCommandDeclaration(self.data["commands"][commandName]))

			formattedExtensionCommands = "\n\n".join(extensionCommands)

			if extension.platform:
				formattedExtensionCommands = self.wrapExtensionWithPlatformGuard(extension.platform,
					formattedExtensionCommands)

			formattedCommands.append(formattedExtensionCommands)

		return formattedCommands

	def formatDispatchStructures(self):
		pointerDeclarations = "{}\n\n{}".format(self.formatCommandPointers("devicePriority", True),
			self.formatCommandPointers("device", True))

		pointerPriorityInitialisations = self.formatCommandPointers("devicePriority", False)
		pointerInitialisations = self.formatCommandPointers("device", False)
		deviceDispatchStructure = Header.deviceDispatchStructure.format(pointerDeclarations,
			pointerPriorityInitialisations, pointerInitialisations)

		pointerDeclarations = "{}\n{}\n\n{}".format(self.formatCommandPointers("instanceNull", True),
			self.formatCommandPointers("none", True), self.formatCommandPointers("instance", True))

		pointerNullInitialisations = self.formatCommandPointers("instanceNull", False)
		pointerInitialisations = self.formatCommandPointers("instance", False)
		instanceDispatchStructure = Header.instanceDispatchStructure.format(pointerDeclarations,
			pointerNullInitialisations, pointerInitialisations)

		return deviceDispatchStructure, instanceDispatchStructure

	def formatCommandPointers(self, category, formatAsDeclarations):
		corePointers = []

		for handle in self.data["handles"].values():
			corePointers += self.getCommandPointers(handle.commands, category, False)

		extensionPointers = []
		platformPointers = {}

		for extension in self.data["extensions"].values():
			pointers = []

			for commands in extension.handles.values():
				pointers += self.getCommandPointers(commands, category, True)

			if not pointers:
				continue

			if extension.platform:
				if not extension.platform in platformPointers:
					platformPointers[extension.platform] = []

				platformPointers[extension.platform] += pointers
			else:
				extensionPointers += pointers

		formattedPointers = "\n".join(self.formatCommandPointerList(corePointers, category,
			formatAsDeclarations))

		if extensionPointers:
			formattedPointers += "\n\n" + "\n".join(self.formatCommandPointerList(extensionPointers,
				category, formatAsDeclarations))

		for platform, pointers in sorted(platformPointers.items()):
			formattedPlatformPointers = self.wrapExtensionWithPlatformGuard(platform,
				"\n".join(self.formatCommandPointerList(pointers, category, formatAsDeclarations)))

			formattedPointers = "{}\n\n{}".format(formattedPointers, formattedPlatformPointers)

		return formattedPointers

	def getCommandPointers(self, commandNames, category, getExtensions):
		pointers = []

		for commandName in commandNames:
			command = self.data["commands"][commandName]

			if command.functionPointer is None or command.isExtension != getExtensions:
				continue

			if command.functionPointer.category == category:
				pointers.append(command.functionPointer)

		return pointers

	# Static

	@staticmethod
	def formatIncludes(includes, delimiter):
		return delimiter.join("#include " + include if include else "" for include in includes)

	@staticmethod
	def formatPlatformMacros():
		macros = []

		for platform in Header.platforms.values():
			includes = Writer.formatIncludes(platform["includes"], "\n\t\t")
			macros.append(Header.platformMacros.format(platform, includes))

		return "\n\n".join(macros)

	@staticmethod
	def formatEnumeration(enumeration):
		declaration = "\tenum class " + enumeration.name
		body = ""

		if enumeration.members:
			width = SymbolFormatter.calculateAlignmentWidth(enumeration.members)
			members = ("\t\t{}".format(SymbolFormatter.formatAssignment(member, width)) for member in
				enumeration.members)

			body = "\n\t{{\n{}\n\t".format(",\n".join(members))
		else:
			body = " {{ ".format(enumeration.name)

		return "{}{}}};".format(declaration, body)

	@staticmethod
	def formatFlags(flags):
		enumeration = Writer.formatEnumeration(flags.flagBits)
		lineBreak = "\n" if flags.flagBits.members else ""
		symbol = Symbol(flags.name, Type.none, "Flags<{}>".format(flags.flagBits.name))
		alias = "using " + SymbolFormatter.formatAssignment(symbol)

		return "{}\n{}\t{};".format(enumeration, lineBreak, alias)

	@staticmethod
	def formatHandles(handles):
		formattedHandles = []
		width = SymbolFormatter.calculateAlignmentWidth(handles)

		for handle in handles:
			formattedHandles.append("\tusing {};".format(SymbolFormatter.formatAssignment(handle, width)))

		return "\n".join(formattedHandles)

	@staticmethod
	def formatFunctionPointers(functions):
		formattedFunctions = []
		width = max(len(function.symbol.name) for function in functions)

		for function in functions:
			parameters = ", ".join(SymbolFormatter.formatDeclaration(parameter) for parameter in
				function.parameters)

			formattedFunction = "{0.name:{1}} = {0.type} (VKAPI_PTR*)({2})".format(function.symbol, width,
				parameters)

			formattedFunctions.append("\tusing {};".format(formattedFunction))

		return "\n".join(formattedFunctions)

	@staticmethod
	def formatConstants(constants):
		formattedConstants = []
		width = SymbolFormatter.calculateAlignmentWidth(constants)

		for constant in constants:
			formattedConstants.append("\tconstexpr {};".format(SymbolFormatter.formatAssignment(constant,
				width)))

		return "\n".join(formattedConstants)

	@staticmethod
	def wrapExtensionWithPlatformGuard(platform, formattedExtension):
		definition = Header.platforms[platform]["definition"]
		return "#if defined({})\n\n{}\n\n#endif".format(definition, formattedExtension)

	@staticmethod
	def formatStructureDefinition(structureName, structure):
		format = "\t{0.category} {0.name}\n\t{{\n{1}{2}\n\t}};\n\n\t{3}"
		members = []

		for member in structure.members:
			bitfield = " : {}".format(member.symbol.type.bitSize) if member.symbol.type.bitSize > 0 else ""
			members.append("\t\t{}{};".format(SymbolFormatter.formatAssignment(member.symbol), bitfield))

		formattedMembers = "\n".join(members)
		constructors = Writer.formatStructureConstructors(structure) if structure.constructors else ""
		assertions = Writer.formatStructureAssertions(structureName, structure)

		return format.format(structure, formattedMembers, constructors, assertions)

	@staticmethod
	def formatCommandDefinition(handle, command):
		symbol = "{} {}::{}".format(SymbolFormatter.formatType(command.symbol.type), handle.symbol.name,
			command.symbol.name)

		parameters = ", ".join((SymbolFormatter.formatDeclaration(parameter.symbol) for parameter in
			command.parameters if not parameter.excludeFromParameterList))

		const = "" if command.isStatic else " const"
		body = "\n".join(("\t\t" + line) if line else "" for line in command.body)

		return "\t{}({}){}\n\t{{\n{}\n\t}}".format(symbol, parameters, const, body)

	@staticmethod
	def formatCommandDeclaration(command):
		prefix = "static inline " if command.isStatic else "inline "
		symbol = SymbolFormatter.formatDeclaration(command.symbol)
		parameters = ", ".join(SymbolFormatter.formatDeclaration(parameter.symbol) for parameter in
			command.parameters if not parameter.excludeFromParameterList)

		const = "" if command.isStatic else " const"
		return "\t\t{}{}({}){};".format(prefix, symbol, parameters, const)

	@staticmethod
	def formatCommandPointerList(commandPointers, category, formatAsDeclarations):
		formattedCommandPointers = []

		for commandPointer in sorted(commandPointers, key = lambda pointer: pointer.symbol):
			if not commandPointer.category == category:
				continue

			if formatAsDeclarations:
				formattedCommandPointers.append("\t\t{} = nullptr;".format(
					SymbolFormatter.formatDeclaration(commandPointer.symbol)))
			else:
				formattedCommandPointers.append("\t\t\t{0.name} = {0.value};".format(commandPointer.symbol))

		return formattedCommandPointers

	@staticmethod
	def formatStructureConstructors(structure):
		constructors = []

		for constructor in structure.constructors:
			name = constructor.symbol.name
			parameters = []
			listInitialisers = []
			bodyInitialisers = []

			for parameter in constructor.parameters:
				if not parameter.excludeFromParameterList:
					parameters.append(str(parameter.symbol))

				if not parameter.argument:
					continue

				if parameter.listInitialise:
					listInitialisers.append(parameter.argument)
				else:
					bodyInitialisers.append("\t\t\t" + parameter.argument)

			initialiserListFormat = " :\n\t\t\t{}" if listInitialisers else ""
			initialiserList = initialiserListFormat.format(", ".join(listInitialisers))
			bodyFormat = "\n\t\t{{\n{}\n\t\t}}" if bodyInitialisers else " {{ }}"
			body = bodyFormat.format("\n".join(bodyInitialisers))
			constructors.append("\t\t{}({}){}{}".format(name, ", ".join(parameters), initialiserList, body))

		return "\n\n" + "\n\n".join(constructors) if constructors else ""

	@staticmethod
	def formatStructureAssertions(structureName, structure):
		assertions = [
			Header.assertionTypeAlignment.format(structure.name, structureName),
			Header.assertionTypeSize.format(structure.name, structureName)
		]

		return "\n\t".join(assertions)
