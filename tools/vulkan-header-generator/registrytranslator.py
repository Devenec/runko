#
# @file tools/vulkan-header-generator/registrytranslator.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

from collections import OrderedDict
import copy
from functools import cmp_to_key
import re
import xml.etree.ElementTree

from generatortypes import Command
from generatortypes import Enumeration
from generatortypes import Flags
from generatortypes import Function
from generatortypes import FunctionParameter
from generatortypes import Handle
from generatortypes import ParameterAttribute
from generatortypes import ParameterTypeConversion
from generatortypes import Structure
from generatortypes import Symbol
from generatortypes import SymbolFormatter
from generatortypes import Type
from registryparser import Parser

class Translator:
	class TypeInfo:
		def __init__(self, name, category, defaultValue):
			self.category = category
			self.defaultValue = defaultValue
			self.name = name

	capitalsNameSubstitutions = frozenset((
		"AABB",
		"ASTC",
		"HDR",
		"ID",
		"LOD",
		"PCI"
	))

	classGetterNames = {
		"flags"                : "mask()",
		"handle"               : "_handle",
		"nonDispatchableHandle": "handle()",
		"optional"             : "value()"
	}

	enumerationMemberPrefix   = "e"
	functionPointerNameSuffix = "FunctionPointer"

	largeSizeTypes = [
		"DeviceSize",
		"size_t"
	]

	resultValues = {
		"incomplete": "Result::Incomplete",
		"success"   : "Result::Success"
	}

	specialCommands = {
		"vkCreateInstance"                      : "instanceNull",
		"vkEnumerateInstanceVersion"            : "instanceNull",
		"vkEnumerateInstanceExtensionProperties": "instanceNull",
		"vkEnumerateInstanceLayerProperties"    : "instanceNull",
		"vkGetDeviceProcAddr"                   : "devicePriority",
		"vkGetInstanceProcAddr"                 : "none"
	}

	types = {
		"array"           : "std::array<{}, {}>",
		"deviceDispatch"  : "DeviceDispatch",
		"handle"          : "Handle<{}, {}>",
		"instanceDispatch": "InstanceDispatch",
		"objectType"      : "ObjectType",
		"optional"        : "Optional<{}>",
		"result"          : "Result",
		"resultPair"      : "ResultPair<{}>",
		"span"            : "gsl::span<{}>",
		"vector"          : "std::vector<{}>"
	}

	vulkanPrefixes = [
		"PFN_vk",
		"Vk",
		"vk"
	]

	def __init__(self):
		self.cppConstants = None
		self.cppData = {}
		self.registryData = None
		self.structureTypes = {}
		self.typeInfo = {}

	def translate(self, registryData):
		self.cppData = {}
		self.registryData = registryData
		self.typeInfo = {}

		self.parseCppRegistry()
		self.initialiseData()
		self.translateComment()
		self.translateEnumerations()
		self.translateFlags()
		self.translateBaseTypes()
		self.translateHandles()
		self.translateNonDispatchableHandles()
		self.addStructureTypeInfo()
		self.translateFunctionPointers()
		self.translateConstants()
		self.translateStructures()
		self.translateCommands()
		self.translateExtensions()

		return self.cppData

	# Private

	# Main parsing methods

	def parseCppRegistry(self):
		rootElement = xml.etree.ElementTree.parse(Parser.cppRegistryFilename).getroot()
		self.cppConstants = rootElement.find("constants")

	def initialiseData(self):
		self.cppData["apiVersions"] = self.registryData["apiVersions"]
		self.cppData["headerVersion"] = self.registryData["headerVersion"]

		for typeName in self.registryData["builtInTypes"]:
			self.addTypeInfo(typeName, "builtIn")

	def translateComment(self):
		self.cppData["comment"] = [line.strip() for line in self.registryData["comment"].split("\n")]

	def translateEnumerations(self):
		self.cppData["enumerations"] = OrderedDict()
		self.structureTypes = {}

		for key, cEnumeration in self.registryData["enumerations"].items():
			name = self.translateVulkanName(cEnumeration.name)
			cppEnumeration = Enumeration(name)
			cppEnumeration.isExtension = cEnumeration.isExtension
			self.translateEnumerationMembers(cEnumeration, cppEnumeration)
			self.addTypeInfo(name, "enumeration", cppEnumeration.members)
			self.cppData["enumerations"][key] = cppEnumeration

	def translateFlags(self):
		self.cppData["flags"] = OrderedDict()

		for key, cFlags in self.registryData["flags"].items():
			flagsName = self.translateVulkanName(cFlags.name)
			flagBitsName = self.translateVulkanName(cFlags.flagBits.name)
			cppFlags = Flags(flagsName, flagBitsName)
			cppFlags.isExtension = cFlags.isExtension
			self.translateEnumerationMembers(cFlags.flagBits, cppFlags.flagBits)
			self.addTypeInfo(flagBitsName, "enumeration", cppFlags.flagBits.members)
			self.addTypeInfo(flagsName, "flags")
			self.cppData["flags"][key] = cppFlags

	def translateBaseTypes(self):
		self.cppData["baseTypes"] = OrderedDict()

		for cName in self.registryData["baseTypes"]:
			cppName = self.translateVulkanName(cName)
			self.addTypeInfo(cppName, "base")
			self.cppData["baseTypes"][cName] = Symbol(cppName, Type.none, cName)

	def translateHandles(self):
		self.cppData["handles"] = OrderedDict()

		for key, cHandle in self.registryData["handles"].items():
			name = self.translateVulkanName(cHandle.symbol.name)
			self.addTypeInfo(name, "handle")
			self.cppData["handles"][key] = Handle(name, cHandle.symbol.name, self.formatObjectType(name))

	def translateNonDispatchableHandles(self):
		self.cppData["nonDispatchableHandles"] = OrderedDict()

		for key, cHandle in self.registryData["nonDispatchableHandles"].items():
			name = self.translateVulkanName(cHandle.name)
			value = self.types["handle"].format(cHandle.name, self.formatObjectType(name))
			cppHandle = Symbol(name, Type.none, value)
			cppHandle.isExtension = cHandle.isExtension
			self.addTypeInfo(name, "nonDispatchableHandle")
			self.cppData["nonDispatchableHandles"][key] = cppHandle

	def addStructureTypeInfo(self):
		for structure in self.registryData["structures"].values():
			self.addTypeInfo(self.translateVulkanName(structure.name), "structure")

	def translateFunctionPointers(self):
		self.cppData["functionPointers"] = OrderedDict()

		for key, cFunction in self.registryData["functionPointers"].items():
			name = self.translateFunctionPointerName(cFunction.symbol.name)
			cppFunction = Function(name, self.translateType(cFunction.symbol.type))
			cppFunction.isExtension = cFunction.isExtension

			for parameter in cFunction.parameters:
				cppFunction.addParameter(self.translateSymbol(parameter))

			self.addTypeInfo(name, "functionPointer")
			self.cppData["functionPointers"][key] = cppFunction

	def translateConstants(self):
		self.cppData["constants"] = OrderedDict()

		for key, cConstant in self.registryData["constants"].items():
			name = self.decapitaliseName(self.translateCapitalsName(cConstant.name, False))
			typeElement = self.cppConstants.find("enum[@name='{}']".format(cConstant.name))

			if typeElement is None:
				format = "constant '{}' does not exist in '{}'."
				raise RuntimeError(format.format(cConstant.name, cppRegistryFilename))

			cppConstant = Symbol(name, Type(typeElement.attrib["type"]), cConstant.name)
			cppConstant.isExtension = cConstant.isExtension
			self.cppData["constants"][key] = cppConstant

	def translateStructures(self):
		self.cppData["structures"] = OrderedDict()

		for key, cStructure in self.registryData["structures"].items():
			name = self.translateVulkanName(cStructure.name)
			cppStructure = Structure(cStructure.category, name, cStructure.parseConstructors)
			cppStructure.isExtension = cStructure.isExtension
			constructorParameters = []

			for cMember in cStructure.members:
				memberSymbol, constructorSymbol = self.translateStructureMember(cMember.symbol,
					cppStructure.members)

				self.translateStructureMemberValue(cMember.symbol, memberSymbol, cStructure.typeEnumeration)
				cppStructure.addMember(Structure.Member(memberSymbol))

				if not memberSymbol.value:
					parameter = FunctionParameter(constructorSymbol, attributes = cMember.attributes,
						countInfo = cMember.countInfo)

					constructorParameters.append(parameter)

			if cStructure.parseConstructors and constructorParameters:
				self.translateStructureConstructors(cppStructure, constructorParameters)

			self.addTypeInfo(cppStructure.name, "structure")
			self.cppData["structures"][key] = cppStructure

		self.translateCustomStructureConstructors()

	def translateCommands(self):
		self.cppData["commands"] = OrderedDict()

		for key, cCommand in self.registryData["commands"].items():
			handles = self.registryData["handles"].items()
			handleName = next((name for name, handle in handles if key in handle.commands), "")
			cHandle = self.registryData["handles"][handleName]
			cppHandle = self.cppData["handles"][handleName]
			cppCommand = self.translateCommand(cCommand, cHandle, cppHandle, True)
			self.translateCommandFunctionPointer(handleName, key, cppCommand)
			outputParameter = self.findCommandParameterWithAttribute(cCommand, ParameterAttribute.OutputArray)

			if not outputParameter is None:
				self.addCommand(cppHandle, key, self.translateCommand(cCommand, cHandle, cppHandle, False))
				key += "_"

			self.addCommand(cppHandle, key, cppCommand)

		for cHandle in self.cppData["handles"].values():
			self.sortCommands(cHandle.commands)

	def translateExtensions(self):
		self.cppData["extensions"] = OrderedDict()

		for extensionKey, cExtension in self.registryData["extensions"].items():
			cppExtension = copy.copy(cExtension)
			cppExtension.handles = {}

			for handleKey, commands in cExtension.handles.items():
				cppExtension.handles[handleKey] = list(commands)

				for commandName in commands:
					if commandName + "_" in self.cppData["commands"]:
						cppExtension.handles[handleKey].append(commandName + "_")

				self.sortCommands(cppExtension.handles[handleKey])

			self.cppData["extensions"][extensionKey] = cppExtension

	# Other methods

	def addTypeInfo(self, name, category, defaultValue = ""):
		simpleCategories = [
			"base",
			"builtIn",
			"flags",
			"functionPointer",
			"handle",
			"nonDispatchableHandle",
			"structure"
		]

		if category in simpleCategories:
			defaultValue = "{}"
		elif category == "enumeration":
			defaultValue = "{}::{}".format(name, defaultValue[0].name) if defaultValue else "{}"
		else:
			raise RuntimeError("unknown type category '{}'.".format(category))

		self.typeInfo[name] = Translator.TypeInfo(name, category, defaultValue)

	def translateEnumerationMembers(self, cEnumeration, cppEnumeration):
		isStructureTypeEnumeration = cEnumeration.name == "VkStructureType"

		for member in cEnumeration.members:
			baseName, vendorTag = self.splitVulkanName(cppEnumeration.name)

			if baseName.endswith(Parser.suffixes["flagBits"]):
				baseName = baseName[:-len(Parser.suffixes["flagBits"])]

			memberName = self.translateCapitalsName(member, isStructureTypeEnumeration, baseName)

			if memberName[0].isdigit():
				memberName = self.enumerationMemberPrefix + memberName

			if vendorTag and memberName.endswith(vendorTag):
				memberName = memberName[:-len(vendorTag)]

			if isStructureTypeEnumeration:
				self.structureTypes[member] = memberName

			cppEnumeration.addMember(Symbol(memberName, Type.none, member))

	def formatObjectType(self, typename):
		objectTypes = self.cppData["enumerations"]["VkObjectType"].members

		if not typename in (objectType.name for objectType in objectTypes):
			format = "object name '{}' not found in enumeration '{}'."
			raise RuntimeError(format.format(typename, self.types["objectType"]))

		return "{}::{}".format(self.types["objectType"], typename)

	def translateFunctionPointerName(self, name):
		name, vendorTag = self.splitVulkanName(self.translateVulkanName(name))
		nameSuffix = "Function"

		if name.endswith(nameSuffix):
			name = name[:-len(nameSuffix)]

		return "{}{}{}".format(name, self.functionPointerNameSuffix, vendorTag)

	def translateType(self, cType):
		cppType = cType.copy()
		translationFunction = (self.translateFunctionPointerName if
			cType.name in self.registryData["functionPointers"] else self.translateVulkanName)

		cppType.name = translationFunction(cType.name)

		if not cppType.name in self.typeInfo:
			name, vendorTag = self.splitVulkanName(cppType.name)

			if name in self.typeInfo:
				cppType.name = name
			else:
				format = "no type info found for type '{}' ('{}')."
				raise RuntimeError(format.format(cppType.name, cType.name))

		if cType.isArray():
			constants = self.cppData["constants"]
			arraySizes = ((constants[size].name if size in constants else size) for size in cType.arraySizes)
			cppType.arraySizes = tuple(arraySizes)

		return cppType

	def translateSymbol(self, cSymbol):
		return Symbol(self.translateIdentifier(cSymbol.name), self.translateType(cSymbol.type), cSymbol.value)

	def translateCapitalsName(self, name, substituteNameParts, namePrefix = ""):
		nameParts = name.split("_")
		vendorTag = nameParts.pop() if nameParts[-1] in self.registryData["vendorTags"] else ""

		if nameParts[-1] == "BIT":
			nameParts.pop()

		for i in range(len(nameParts)):
			if not substituteNameParts or not nameParts[i] in self.capitalsNameSubstitutions:
				nameParts[i] = nameParts[i].title()

		name = self.translateVulkanName("".join(nameParts) + vendorTag)
		return name[len(namePrefix):] if name.startswith(namePrefix) else name

	def translateStructureMember(self, cSymbol, cppMembers):
		cppSymbol = self.translateSymbol(cSymbol)
		namesakeCount = sum(1 for member in cppMembers if member.symbol.name == cppSymbol.name)

		if namesakeCount > 0:
			cppSymbol.name += str(namesakeCount + 1)

		parameterSymbol = copy.deepcopy(cppSymbol)

		if cppSymbol.type.bitSize > 0:
			typeCategory = self.getTypeCategory(cppSymbol.type)

			if typeCategory == "flags":
				cppSymbol.type.name = cSymbol.type.name
				self.addTypeInfo(cppSymbol.type.name, "flags")
			elif typeCategory != "builtIn":
				format = "type '{}' ('{}') can not be declared with bitfield."
				raise RuntimeError(format.format(cppSymbol.type.name, cSymbol.type.name))

		return cppSymbol, parameterSymbol

	def translateStructureMemberValue(self, cSymbol, cppSymbol, typeEnumeration):
		if cSymbol.name == "pNext":
			cppSymbol.value = "{}"
		elif cSymbol.type.name == "VkStructureType":
			structureType = self.structureTypes[typeEnumeration] if typeEnumeration else ""
			cppSymbol.value = "{}::{}".format(cppSymbol.type.name, structureType)

	def translateStructureConstructors(self, cppStructure, cppParameters):
		if cppStructure.category == "union":
			for parameter in cppParameters:
				constructor = self.createStructureConstructor(cppStructure.name, False)
				setDefaultValue = not cppStructure.constructors
				self.translateStructureConstructorParameter(constructor, parameter, setDefaultValue)
				cppStructure.addConstructor(constructor)
		else:
			constructor = self.createStructureConstructor(cppStructure.name, len(cppParameters) == 1)

			for parameter in cppParameters:
				self.translateStructureConstructorParameter(constructor, parameter, True)

			cppStructure.addConstructor(constructor)

	def translateCustomStructureConstructors(self):
		for key, constructors in self.registryData["customConstructors"].items():
			structure = self.cppData["structures"][key]
			useDefaultValue = len(structure.constructors) + len(constructors) == 1

			for parameters in constructors:
				isExplicit = len(parameters) == 1 and structure.category != "union"
				cppConstructor = self.createStructureConstructor(structure.name, isExplicit)

				for parameter in parameters:
					symbol = self.translateSymbol(parameter.symbol)
					symbol.value = self.getTypeDefaultValue(symbol.type) if useDefaultValue else ""
					cppConstructor.addParameter(FunctionParameter(symbol, parameter.argument, True))

				structure.addConstructor(cppConstructor)

	def translateCommand(self, cCommand, cHandle, cppHandle, parseOutputs):
		name = self.translateCommandName(cCommand, cHandle, cppHandle)
		commandReturnType, callReturnType = self.translateCommandReturnTypes(cCommand, parseOutputs)
		isStatic = cCommand.parameters[0].symbol.type.name != cHandle.symbol.name
		cppCommand = Command(name, cCommand.category, commandReturnType, callReturnType, isStatic)
		cppCommand.isExtension = cCommand.isExtension
		self.translateCommandParameters(cCommand, cppCommand, cHandle, parseOutputs)
		self.translateCommandBody(cCommand, cppCommand, parseOutputs)

		return cppCommand

	def addCommand(self, cppHandle, key, cppCommand):
		self.cppData["commands"][key] = cppCommand
		cppHandle.addCommand(key)

	def sortCommands(self, commands):
		def compareCommands(commandNameA, commandNameB):
			commandA = self.cppData["commands"][commandNameA]
			commandB = self.cppData["commands"][commandNameB]

			if commandA.isStatic == commandB.isStatic:
				return -1 if commandA.symbol < commandB.symbol else 1

			return -1 if commandB.isStatic else 1

		commands.sort(key = cmp_to_key(compareCommands))

	def splitVulkanName(self, name):
		vendorTag = next((tag for tag in self.registryData["vendorTags"] if name.endswith(tag)), "")
		return (name[:-len(vendorTag)] if vendorTag else name), vendorTag

	def getTypeCategory(self, type):
		typeName = type.getName()
		return self.typeInfo[typeName].category if typeName in self.typeInfo else None

	def translateStructureConstructorParameter(self, constructor, parameter, setDefaultValue):
		type, typeConversion = self.translateFunctionParameterType(parameter.symbol.type,
			parameter.attributes, False)

		defaultValue = self.getTypeDefaultValue(type) if setDefaultValue else ""
		symbol = Symbol(parameter.symbol.name, type, defaultValue)
		initialiser, listInitialise = self.translateStructureConstructorInitialiser(symbol, typeConversion)

		if listInitialise:
			initialiser = "{}({})".format(symbol.name, initialiser)

		constructor.addParameter(FunctionParameter(symbol, initialiser, listInitialise))

		if not parameter.countInfo is None:
			self.translateStructureConstructorCountParameter(constructor, parameter.countInfo, symbol.name)

	def getTypeDefaultValue(self, type):
		if type.isPointer() or type.isTemplate():
			return "{}"

		if type.name in self.typeInfo:
			return self.typeInfo[type.name].defaultValue

		raise RuntimeError("no type info found for type '{}'.".format(type.name))

	def translateCommandReturnTypes(self, cCommand, parseOutputs):
		commandType = self.translateType(cCommand.symbol.type)
		commandReturns = cCommand.symbol.type.isPointer() or cCommand.symbol.type.name != "void"
		callType = commandType if commandReturns else None
		outputParameter = (self.findCommandParameterWithAttribute(cCommand, ParameterAttribute.Output) if
			parseOutputs else None)

		if not outputParameter is None:
			commandType = self.dereferenceType(self.translateType(outputParameter.symbol.type))

			if ParameterAttribute.ArrayPointer in outputParameter.attributes:
				self.translateTypeToTemplateArgumentType(commandType, False, False)
				commandType = self.translateTypeToTemplateType(Type.none, "vector", commandType)

			if commandReturns:
				commandType = self.translateTypeToTemplateType(Type.none, "resultPair", commandType)

		return commandType, callType

	def translateCommandParameters(self, cCommand, cppCommand, cHandle, parseOutputs):
		dispatchType = Type.noneConstReference.copy()
		dispatchTypeName = "instance" if cHandle.symbol.name in Parser.instanceCommandTypes else "device"
		dispatchType.name = self.types[dispatchTypeName + "Dispatch"]
		cppCommand.addParameter(FunctionParameter(Symbol("dispatch", dispatchType)))
		parameterIterator = iter(cCommand.parameters)

		if cCommand.parameters[0].symbol.type.name == cHandle.symbol.name:
			cppCommand.addCallArgument(self.classGetterNames["handle"])
			next(parameterIterator)

		for parameter in parameterIterator:
			self.translateCommandParameter(cppCommand, parameter, parseOutputs)

		cppCommand.callArguments += [parameter.argument for parameter in cppCommand.parameters if
			parameter.argument]

	def translateCommandBody(self, cCommand, cppCommand, parseOutputs):
		outputParameter = (self.findCommandParameterWithAttribute(cppCommand, ParameterAttribute.Output) if
			parseOutputs else None)

		countParameter = None if outputParameter is None else self.findCountParameter(cppCommand,
			outputParameter.countInfo)

		if cCommand.canReturnIncomplete and not outputParameter is None:
			self.translateCommandBodyWithLoop(cCommand, cppCommand, outputParameter, countParameter)
		else:
			self.translateCommandBodyWithoutLoop(cCommand, cppCommand, outputParameter, countParameter)

		if not outputParameter is None:
			self.translateCommandBodyReturn(cppCommand, outputParameter)

	def translateFunctionParameterType(self, type, attributes, allowNonConstReference):
		if type.isArray():
			self.translateTypeToTemplateArgumentType(type, False, False)
			type = self.translateTypeToTemplateType(Type.noneConstReference, "array", type,
				type.arraySizes[0])

			return type, ParameterTypeConversion.Array

		if ParameterAttribute.ArrayPointer in attributes:
			self.translateTypeToTemplateArgumentType(type, True, True)
			type = self.translateTypeToTemplateType(Type.noneConstReference, "span", type)

			return type, ParameterTypeConversion.Span

		if ParameterAttribute.String in attributes:
			return type, ParameterTypeConversion.Nothing

		if ParameterAttribute.Optional in attributes:
			self.translateTypeToTemplateArgumentType(type, False, True)
			type = self.translateTypeToTemplateType(Type.noneConstReference, "optional", type)

			return type, ParameterTypeConversion.Optional

		isDereferencablePointer = type.isPointer() and type.name != "void"

		if (isDereferencablePointer and (allowNonConstReference or type.isConstant()) or
			(not isDereferencablePointer and self.getTypeCategory(type) == "structure")):

			return self.translateFunctionParameterTypeToReference(type, attributes, allowNonConstReference)

		return type, ParameterTypeConversion.Nothing

	def translateStructureConstructorInitialiser(self, cppSymbol, typeConversion):
		initialiser = self.translateFunctionParameterArgument(cppSymbol.name, typeConversion)

		if typeConversion == ParameterTypeConversion.Array:
			format = "std::copy_n({0.name}.data(), {0.name}.size(), this->{0.name});"
			initialiser = format.format(cppSymbol)
		elif typeConversion == ParameterTypeConversion.Nothing:
			if cppSymbol.type.bitSize > 0 and self.getTypeCategory(cppSymbol.type) == "flags":
				initialiser += ".{}".format(self.classGetterNames["flags"])

		return initialiser, typeConversion != ParameterTypeConversion.Array

	def translateCommandParameter(self, cppCommand, cParameter, parseOutputs):
		name = self.translateIdentifier(cParameter.symbol.name)
		type, typeConversion = self.translateCommandParameterType(cParameter, parseOutputs)
		symbol = Symbol(name, type, cParameter.symbol.value)
		argument = self.translateCommandParameterArgument(cParameter, type, name, typeConversion)
		countInfo = self.translateCountInfo(cParameter.countInfo)
		cppParameter = FunctionParameter(symbol, argument, attributes = cParameter.attributes,
			countInfo = countInfo)

		cppCommand.addParameter(cppParameter)

		if not countInfo is None and not ParameterAttribute.Output in cParameter.attributes:
			self.translateCountParameter(cppCommand, countInfo, symbol.name)

	def translateCommandBodyWithLoop(self, cCommand, cppCommand, outputParameter, countParameter):
		countName = outputParameter.countInfo[1].format(outputParameter.countInfo[0])

		cppCommand.body += [
			"{} result = {};".format(self.types["result"], self.resultValues["success"]),
			self.translateCommandBodyCountDeclaration(cppCommand, countParameter),
			self.translateCommandBodyOutputDeclaration(cCommand, outputParameter, countParameter),
			"",
			"do",
			"{",
			"\tresult = " + self.translateCommandBodyCall(cCommand, cppCommand, outputParameter, True),
			"",
			"\tif(result == {} && {} > 0)".format(self.resultValues["success"], countName),
			"\t{"
		]

		cppCommand.parameters.remove(outputParameter)
		resize = "{}.resize({});".format(outputParameter.symbol.name, countName)

		cppCommand.body += [
			"\t\t" + resize,
			"\t\tresult = " + self.translateCommandBodyCall(cCommand, cppCommand, outputParameter, False),
			"\t}",
			"}",
			"while(result == {});".format(self.resultValues["incomplete"]),
			"",
			"if(result == {})".format(self.resultValues["success"]),
			"\t" + resize
		]

	def translateCommandBodyWithoutLoop(self, cCommand, cppCommand, outputParameter, countParameter):
		isFirstCall = True

		if not outputParameter is None:
			if (ParameterAttribute.ArrayPointer | ParameterAttribute.Optional) in outputParameter.attributes:
				cppCommand.body += [
					self.translateCommandBodyCountDeclaration(cppCommand, countParameter),
					self.translateCommandBodyCall(cCommand, cppCommand, outputParameter, True, True)
				]

				isFirstCall = False

				if not cppCommand.callReturnType is None:
					cppCommand.body += self.translateCommandBodyResultCheck(outputParameter, countParameter)

			cppCommand.parameters.remove(outputParameter)
			cppCommand.addBodyLine(self.translateCommandBodyOutputDeclaration(cCommand, outputParameter,
				countParameter))

		call = self.translateCommandBodyCall(cCommand, cppCommand, outputParameter, isFirstCall, True)
		cppCommand.addBodyLine(call)

	def translateFunctionParameterTypeToReference(self, type, attributes, allowNonConst):
		isConstant = not type.isPointer() or type.isConstant()
		referenceType = (Type.noneConstReference if isConstant else Type.noneReference).copy()
		referenceType.name = type.name
		typeConversion = (ParameterTypeConversion.Reference if type.isPointer() else
			ParameterTypeConversion.Nothing)

		return referenceType, typeConversion

	def translateCommandParameterType(self, cParameter, parseOutputs):
		attributes = cParameter.attributes
		arraySizeOrOutput = ParameterAttribute.ArraySize | ParameterAttribute.Output
		type = self.translateType(cParameter.symbol.type)
		typeConversion = ParameterTypeConversion.Nothing

		if parseOutputs and (attributes & arraySizeOrOutput) != ParameterAttribute.Nothing:
			if type.isPointer():
				self.dereferenceType(type)
				typeConversion = ParameterTypeConversion.Reference

			if ParameterAttribute.OutputArray in attributes:
				self.translateTypeToTemplateArgumentType(type, False, False)
				type = self.translateTypeToTemplateType(Type.none, "vector", type)
				typeConversion = ParameterTypeConversion.Vector
		else:
			type, typeConversion = self.translateFunctionParameterType(type, attributes, True)

		return type, typeConversion

	def translateCommandParameterArgument(self, cParameter, cppType, cppName, typeConversion):
		argument = self.translateFunctionParameterArgument(cppName, typeConversion)

		if typeConversion in [ParameterTypeConversion.Array, ParameterTypeConversion.Vector]:
			argument += ".data()"
		elif typeConversion == ParameterTypeConversion.Nothing and not cParameter.symbol.type.isPointer():
			argument = self.translateCommandParameterArgumentByTypeCategory(cParameter, cppType, argument)

		cType = cParameter.symbol.type

		if (cType.isArray() or cType.isPointer()) and self.typeRequiresCast(cppType):
			asterisk = "" if cType.isPointer() else "*"
			argument = "reinterpret_cast<{}{}>({})".format(SymbolFormatter.formatType(cType), asterisk,
				argument)

		return argument

	def translateCommandBodyCall(self, cCommand, cppCommand, outputParameter, isFirstCall,
		parseStatement = False):

		statement = ""
		outputInParameters = outputParameter in cppCommand.parameters
		callArguments = (cppCommand.callArguments[:-1] + ["nullptr"] if outputInParameters else
			cppCommand.callArguments)

		commandName = self.decapitaliseName(self.translateVulkanName(cCommand.symbol.name))
		call = "dispatch.{}({})".format(commandName, ", ".join(callArguments))

		if not cppCommand.callReturnType is None:
			if parseStatement:
				if outputParameter is None:
					statement = "return "
				else:
					const = "const " if isFirstCall and not outputInParameters else ""
					type = SymbolFormatter.formatType(cppCommand.callReturnType) + " " if isFirstCall else ""
					statement = "{}{}result = ".format(const, type)

			if self.typeRequiresCast(cppCommand.callReturnType):
				type = SymbolFormatter.formatType(cppCommand.callReturnType)
				call = "static_cast<{0}>({1})".format(type, call)

		return "{}{};".format(statement, call)

	def translateCommandParameterArgumentByTypeCategory(self, cParameter, cppType, argument):
		category = self.getTypeCategory(cppType)

		if category in ["enumeration", "flags"]:
			getter = ".{}".format(self.classGetterNames["flags"]) if category == "flags" else ""
			type = SymbolFormatter.formatType(cParameter.symbol.type)
			argument = "static_cast<{}>({}{})".format(type, argument, getter)
		elif category == "nonDispatchableHandle":
			argument = "{}.{}".format(argument, self.classGetterNames["nonDispatchableHandle"])
		elif category in ["handle", "structure"]:
			format = "handle or structure type parameter '{}' must be of a pointer type."
			raise RuntimeError(format.format(cParameter.symbol))

		return argument

	def typeRequiresCast(self, type):
		typeCategory = self.getTypeCategory(type)
		return typeCategory in ["enumeration", "flags", "handle", "nonDispatchableHandle", "structure"]

	# Static

	@staticmethod
	def translateVulkanName(name):
		prefix = next((prefix for prefix in Translator.vulkanPrefixes if name.startswith(prefix)), "")
		return name[len(prefix):]

	@staticmethod
	def decapitaliseName(name):
		return name[0].lower() + name[1:]

	@staticmethod
	def translateCommandFunctionPointer(cHandleName, cCommandName, cppCommand):
		symbolName = Translator.decapitaliseName(Translator.translateVulkanName(cCommandName))
		symbolType = Type("PFN_" + cCommandName)
		category = ""

		if cCommandName in Translator.specialCommands:
			category = Translator.specialCommands[cCommandName]
		else:
			category = "instance" if cHandleName in Parser.instanceCommandTypes else "device"

		dispatchArgumentName = ("*this" if cppCommand.category in category else
			(cppCommand.category + "Dispatch"))

		symbolValue = "reinterpret_cast<{}>({}.getProcAddr({}, \"{}\"))".format(symbolType.name,
			cppCommand.category, dispatchArgumentName, cCommandName)

		cppCommand.functionPointer = Command.FunctionPointer(symbolType, symbolName, symbolValue, category)

	@staticmethod
	def findCommandParameterWithAttribute(command, parameterAttribute):
		return next((parameter for parameter in command.parameters if parameterAttribute in
			parameter.attributes), None)

	@staticmethod
	def translateIdentifier(identifier):
		match = re.search(r"^(p|pfn|pp)([A-Z])", identifier, re.ASCII)

		if match is None:
			return identifier

		return identifier[match.start(2)].lower() + identifier[match.end(2):]

	@staticmethod
	def createStructureConstructor(structureName, isExplicit):
		name = ("explicit " if isExplicit else "") + structureName
		return Function(name)

	@staticmethod
	def translateCommandName(cCommand, cHandle, cppHandle):
		cppName = Translator.translateVulkanName(cCommand.symbol.name)
		cppName = cppName.replace(cppHandle.symbol.name, "")
		namePrefix = "Cmd"

		if cHandle.symbol.name == "VkCommandBuffer" and cppName.startswith(namePrefix):
			cppName = cppName[len(namePrefix):]

		return Translator.decapitaliseName(cppName)

	@staticmethod
	def translateStructureConstructorCountParameter(constructor, cCountInfo, parameterName):
		cppCountInfo = Translator.translateCountInfo(cCountInfo)
		countParameter = Translator.translateCountParameter(constructor, cppCountInfo, parameterName)

		if not countParameter is None:
			countParameter.argument = "{}({})".format(countParameter.symbol.name, countParameter.argument)

	@staticmethod
	def dereferenceType(type):
		endPosition = type.suffix.rfind("*")
		type.suffix = type.suffix[:endPosition]

		return type

	@staticmethod
	def translateTypeToTemplateArgumentType(type, keepConstness, dereference):
		if not keepConstness:
			type.prefix = ""

		if dereference:
			Translator.dereferenceType(type)

		if not type.isPointer() and type.name == "void":
			type.name = "std::byte"

	@staticmethod
	def translateTypeToTemplateType(baseType, templateName, argumentType, *additionalArguments):
		templateType = baseType.copy()
		templateType.name = Translator.types[templateName].format(SymbolFormatter.formatType(argumentType),
			*additionalArguments)

		templateType.templateArgumentTypename = argumentType.name
		return templateType

	@staticmethod
	def findCountParameter(cppFunction, countInfo):
		if countInfo is None:
			return None

		for parameter in (parameter for parameter in cppFunction.parameters if not parameter.symbol is None):
			if parameter.symbol.name == countInfo[0]:
				return parameter

		return None

	@staticmethod
	def translateCommandBodyReturn(cppCommand, outputParameter):
		callReturns = not cppCommand.callReturnType is None
		format = "return {1}(result, std::move({0}));" if callReturns else "return {0};"
		resultPair = Translator.types["resultPair"][:Translator.types["resultPair"].find("<")]
		cppCommand.addBodyLine("")
		cppCommand.addBodyLine(format.format(outputParameter.symbol.name, resultPair))

	@staticmethod
	def translateFunctionParameterArgument(argument, typeConversion):
		if typeConversion == ParameterTypeConversion.Optional:
			argument += ".{}".format(Translator.classGetterNames["optional"])
		elif typeConversion == ParameterTypeConversion.Reference:
			argument = "&" + argument
		elif typeConversion == ParameterTypeConversion.Span:
			argument = "{0}.empty() ? nullptr : {0}.data()".format(argument)

		return argument

	@staticmethod
	def translateCountInfo(countInfo):
		if countInfo is None:
			return None

		countExpression = countInfo[0].split(",")[0].replace("->",  ".")
		return Translator.translateIdentifier(countExpression), countInfo[1]

	@staticmethod
	def translateCommandBodyCountDeclaration(cppCommand, countParameter):
		countParameter.argument = ""
		cppCommand.parameters.remove(countParameter)

		return "{}{{}};".format(SymbolFormatter.formatDeclaration(countParameter.symbol))

	@staticmethod
	def translateCommandBodyOutputDeclaration(cCommand, outputParameter, countParameter):
		isArrayPointer = ParameterAttribute.ArrayPointer in outputParameter.attributes
		declaration = SymbolFormatter.formatDeclaration(outputParameter.symbol)

		if isArrayPointer:
			format = "{};" if cCommand.canReturnIncomplete else "{}({});"
			useCountParameter = not countParameter is None and countParameter.argument
			initialiser = (countParameter.argument if useCountParameter else
				outputParameter.countInfo[1].format(outputParameter.countInfo[0]))

			return format.format(declaration, initialiser)

		return declaration + "{};"

	@staticmethod
	def translateCommandBodyResultCheck(outputParameter, countParameter):
		ifFormat = "if(result != {} || {} == 0)"
		countName = outputParameter.countName[1].format(outputParameter.countName[0])
		returnFormat = "\treturn {}(result, {}());"
		resultPair = Translator.types["resultPair"][:Translator.types["resultPair"].find("<")]

		return [
			"",
			ifFormat.format(Translator.resultValues["success"], countParameter.symbol.name, countName),
			returnFormat.format(resultPair, SymbolFormatter.formatType(outputParameter.symbol.type)),
			""
		]

	@staticmethod
	def translateCountParameter(function, countInfo, parameterName):
		countParameter = Translator.findCountParameter(function, countInfo)

		if (countParameter is None or countParameter.excludeFromParameterList or
			ParameterAttribute.InOutSize in countParameter.attributes):

			return None

		typename = countParameter.symbol.type.name
		cast = "{}" if typename in Translator.largeSizeTypes else "static_cast<uint32_t>({})"
		countParameter.argument = cast.format(countInfo[1].format(parameterName + ".size()"))
		countParameter.excludeFromParameterList = True

		return countParameter
