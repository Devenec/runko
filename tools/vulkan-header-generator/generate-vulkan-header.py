#
# @file tools/vulkan-header-generator/generate-vulkan-header.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

import os
import sys

import headerwriter
import registryparser
import registrytranslator

defaultHeaderPath   = "../../graphics/include/graphics/vulkan"
defaultRegistryPath = "../../external/vulkan/registry"

def parseArgumentDirectoryPath(index):
	absolutePath = os.path.abspath(sys.argv[index])

	if not os.path.isdir(absolutePath):
		raise RuntimeError("argument {} '{}' is not a directory.".format(index, sys.argv[index]))

	return absolutePath

def parseArguments():
	paths = [
		os.path.join(os.getcwd(), defaultRegistryPath),
		os.path.join(os.getcwd(), defaultHeaderPath)
	]

	for i in range(len(paths)):
		if len(sys.argv) > i + 1:
			paths[i] = parseArgumentDirectoryPath(i + 1)

		paths[i] = os.path.normpath(paths[i])

	return paths

if __name__ == "__main__":
	try:
		inputPath, headerOutputPath = parseArguments()
		registryData = registryparser.Parser().parse(inputPath)
		cppRegistryData = registrytranslator.Translator().translate(registryData)
	except RuntimeError as exception:
		print("Error: {}".format(exception))
	finally:
		headerwriter.Writer().write(cppRegistryData, headerOutputPath)
