#
# @file tools/vulkan-header-generator/types.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

import copy
from enum import Enum
from enum import Flag

class ParameterAttribute(Flag):
	Nothing      = 0
	ArrayPointer = 1
	ArraySize    = 2
	InOutSize    = 4
	Optional     = 8
	Output       = 16
	String       = 32

	OutputArray  = ArrayPointer | Output

class ParameterTypeConversion(Enum):
	Nothing   = 0
	Array     = 1
	Optional  = 2
	Reference = 3
	Span      = 4
	Vector    = 5

class Enumeration:
	def __init__(self, name):
		self.members = []
		self.name = name

	def addMember(self, member):
		self.members.append(member)

class Extension:
	def __init__(self, name, platform):
		self.constants = []
		self.enumerations = set()
		self.flags = set()
		self.functionPointers = set()
		self.handles = {}
		self.name = name
		self.nonDispatchableHandles = []
		self.platform = platform
		self.structures = {}

class Flags:
	def __init__(self, name, flagBitsName):
		self.flagBits = Enumeration(flagBitsName)
		self.name = name

	def addFlagBit(self, flagBit):
		self.flagBits.addMember(flagBit)

class FunctionParameter:
	def __init__(self, symbol, argument = "", listInitialise = False, attributes = ParameterAttribute.Nothing,
		countInfo = None):

		self.argument = argument
		self.attributes = attributes
		self.countInfo = countInfo
		self.excludeFromParameterList = False
		self.listInitialise = listInitialise
		self.symbol = symbol

class Structure:
	class Member:
		def __init__(self, symbol, attributes = ParameterAttribute.Nothing, countInfo = None):
			self.attributes = attributes
			self.countInfo = countInfo
			self.symbol = symbol

	def __init__(self, category, name, parseConstructors):
		self.category = category
		self.constructors = []
		self.members = []
		self.name = name
		self.parseConstructors = parseConstructors
		self.typeEnumeration = ""

	def addConstructor(self, function):
		self.constructors.append(function)

	def addMember(self, member):
		self.members.append(member)

class Type:
	def __init__(self, name, prefix = "", suffix = "", arraySizes = None, bitSize = 0):
		self.arraySizes = arraySizes
		self.bitSize = bitSize
		self.name = name
		self.prefix = prefix
		self.suffix = suffix
		self.templateArgumentTypename = ""

	def copy(self):
		return copy.copy(self)

	def formatArraySpecifier(self):
		if not self.isArray():
			return ""

		sizes = "][".join(str(size) for size in self.arraySizes)
		return "[{}]".format(sizes)

	def getName(self):
		return self.templateArgumentTypename if self.templateArgumentTypename else self.name

	def isArray(self):
		return not self.arraySizes is None

	def isConstant(self):
		return "const" in self.prefix

	def isPointer(self):
		return "*" in self.suffix

	def isTemplate(self):
		return bool(self.templateArgumentTypename)

	def __repr__(self):
		return "{0.prefix}{0.name}{0.suffix}{1}".format(self, self.formatArraySpecifier())

Type.none               = Type("")
Type.noneConstReference = Type("", "const ", "&")
Type.noneReference      = Type("", "", "&")

class Symbol:
	def __init__(self, name, type = Type.none, value = ""):
		self.name = name
		self.type = type
		self.value = value

	def __lt__(self, symbol):
		return self.name < symbol.name

	def __repr__(self):
		type = str(self.type) if self.type.name else "(NO TYPE)"
		value = " = " + self.value if self.value else ""

		return "{} {}{}".format(type, self.name, value)

class Function:
	def __init__(self, name, returnType = Type.none, canReturnIncomplete = False):
		self.canReturnIncomplete = canReturnIncomplete
		self.parameters = []
		self.symbol = Symbol(name, returnType)
		self.category = ""

	def addParameter(self, parameter):
		self.parameters.append(parameter)

class Command(Function):
	class FunctionPointer:
		def __init__(self, type, name, value, category):
			self.category = category
			self.symbol = Symbol(name, type, value)

	def __init__(self, name, category, functionReturnType, callReturnType, isStatic):
		super().__init__(name, functionReturnType)
		self.body = []
		self.callArguments = []
		self.callReturnType = callReturnType
		self.category = category
		self.functionPointer = None
		self.isStatic = isStatic

	def addBodyLine(self, line):
		self.body.append(line)

	def addCallArgument(self, argument):
		self.callArguments.append(argument)

class Handle:
	def __init__(self, name, value = "", objectType = ""):
		self.commands = []
		self.objectType = objectType
		self.symbol = Symbol(name, Type.none, value)

	def addCommand(self, command):
		self.commands.append(command)

class SymbolFormatter:
	@staticmethod
	def calculateAlignmentWidth(symbols):
		return max(len(SymbolFormatter.formatDeclaration(symbol)) for symbol in symbols)

	@staticmethod
	def formatAssignment(symbol, alignmentWidth = 1):
		format = "{0:{1}} = {2}" if symbol.value else "{0:{1}}"
		return format.format(SymbolFormatter.formatDeclaration(symbol), alignmentWidth, symbol.value)

	@staticmethod
	def formatDeclaration(symbol):
		type = SymbolFormatter.formatType(symbol.type)
		type = type + " " if type else ""

		return "{}{}{}".format(type, symbol.name, symbol.type.formatArraySpecifier())

	@staticmethod
	def formatType(type):
		return "{0.prefix}{0.name}{0.suffix}".format(type)
