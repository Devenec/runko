#
# @file tools/generate-gui-shaders.py
#
# Runko
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#

import binascii
from collections import OrderedDict
import os
import subprocess
import sys

defaultRootPath = "../graphics"
headerPath      = "include/graphics/GUIShaders.h"

shaders = {
	"fragment": "shaders/GUIFragment.glsl",
	"vertex"  : "shaders/GUIVertex.glsl"
}

compilerPath = "../tools/glslc.exe"
tempFilename = "temp.spirv"

byteValuesPerLine = 6

headerTemplate = (
	"/**\n"
	" * @file graphics/GUIShaders.h\n"
	" *\n"
	" * Runko\n"
	" * Copyright 2021 Eetu 'Devenec' Oinasmaa\n"
	" *\n"
	" * Permission is hereby granted, free of charge, to any person obtaining a copy\n"
	" * of this software and associated documentation files (the \"Software\"), to deal\n"
	" * in the Software without restriction, including without limitation the rights\n"
	" * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n"
	" * copies of the Software, and to permit persons to whom the Software is\n"
	" * furnished to do so, subject to the following conditions:\n"
	" *\n"
	" * The above copyright notice and this permission notice shall be included in\n"
	" * all copies or substantial portions of the Software.\n"
	" */\n\n"
	"#pragma once\n\n"
	"#include <array>\n"
	"#include <core/Types.h>\n\n"
	"namespace Graphics\n"
	"{{\n"
	"{}\n"
	"}}\n"
)

headerArrayTemplate = (
	"\tstatic constexpr std::array<std::byte, {}> guiShaderCode{} =\n"
	"\t{{\n"
	"\t\t{}\n"
	"\t}};"
)

def compileShader(rootPath, stage, sourcePath):
	processPath = os.path.join(rootPath, compilerPath)
	argumentStage = "-fshader-stage={}".format(stage)
	sourcePath = os.path.join(rootPath, sourcePath)
	arguments = [processPath, argumentStage, "-o", tempFilename, "--target-env=vulkan1.2", sourcePath]
	result = subprocess.run(arguments, encoding = "utf8", stderr = subprocess.STDOUT,
		stdout = subprocess.PIPE)

	if result.returncode != 0:
		raise RuntimeError(result.stdout)

def generateHeader(rootPath, shaderCode):
	arrays = []

	for stage, code in shaderCode.items():
		byteList = ["std::byte(0x{:02X})".format(byte) for byte in code]
		byteLines = [byteList[i : i + byteValuesPerLine] for i in range(0, len(byteList), byteValuesPerLine)]
		initialiser = (", ".join(line) for line in byteLines)
		arrays.append(headerArrayTemplate.format(len(code), stage.capitalize(), ",\n\t\t".join(initialiser)))

	header = headerTemplate.format("\n\n".join(arrays))
	path = os.path.join(rootPath, headerPath)

	with open(path, "w") as file:
		file.write(header)

def getRootPath():
	if len(sys.argv) == 1:
		return defaultRootPath

	return sys.argv[1]

def readShaderCode():
	code = None

	with open(tempFilename, "rb") as file:
		code = file.read()

	os.remove(tempFilename)
	return code

if __name__ == "__main__":
	rootPath = getRootPath()
	shaderCode = OrderedDict()

	try:
		for stage, path in shaders.items():
			compileShader(rootPath, stage, path)
			shaderCode[stage] = readShaderCode()

		generateHeader(rootPath, shaderCode)
	except RuntimeError as exception:
		print(str(exception))
